<?php 

/**
* 
*/
class Customers
{
	private $host = 'localhost';
	private $dbname = 'stock';
	private $user = 'root';
	private $pass = '';
	public $dbh;

	function __construct()
	{
		$this->connection();
	}
	private function connection()
	{
		try{
			$this->dbh = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname, $this->user, $this->pass);	
		}catch (PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}

	public function FetchAll($type=null){
		try {
			$stmt = 'SELECT * FROM `spl_customers`  ORDER BY customer_id DESC';
			$prepare = $this->dbh->prepare($stmt);
			$prepare->execute();
			if(strtolower($type) == 'assoc'){
				$result = $prepare->fetchAll(PDO::FETCH_ASSOC);
			}else if(strtolower($type) == 'both'){
				$result = $prepare->fetchAll(PDO::FETCH_BOTH);
			}else if(strtolower($type) == 'array'){
				$result = $prepare->fetchAll(PDO::FETCH_NUM);
			}else{
				$result = $prepare->fetchAll(PDO::FETCH_OBJ);
			}
			return $result;
		} catch (PDOException $e) {
		    die("Error!: " . $e->getMessage() . "<br/>");
		}
	}
	public function nextRecord($type=null){
		$new_cumtomer_prefix = 'IMSC00';
		try {
			$stmt = 'SELECT MAX(customer_id)as id FROM `spl_customers`';
			$prepare = $this->dbh->prepare($stmt);
			$prepare->execute();
			$result = $prepare->fetchAll(PDO::FETCH_OBJ);
			$result = $new_cumtomer_prefix.($result[0]->id + 1);
			return $result;
		} catch (PDOException $e) {
		    die("Error!: " . $e->getMessage() . "<br/>");
		}
	}
	public function NewCustomer($array = array())
	{
		$customer_number = $array['customer_number'];
		$city = $array['city'];
		$contact_person = $array['contact_person'];
		$phone_number = $array['phone_number'];
		$mobile_number = $array['mobile_number'];
		$email = $array['email'];
		$notes = $array['notes'];
		$address = $array['address'];
		$added_by = $array['added_by'];
		$updated_by = $array['updated_by'];
		 
		try {
			$stmt = "INSERT INTO spl_customers (customer_number, customer_name, address, city, contact_person, phone_number, email, mobile_number, notes, added_by,updated_by) VALUES (:customer_number,:customer_name,:address,:city,:contact_person,:phone_number,:email,:mobile_number,:notes,:added_by,:updated_by)";

			$prepare = $this->dbh->prepare($stmt);
			$prepare->bindParam(':customer_number', $customer_number, PDO::PARAM_STR);
			$prepare->bindParam(':customer_name', $contact_person, PDO::PARAM_STR);
			$prepare->bindParam(':address', $address, PDO::PARAM_STR);
			$prepare->bindParam(':city', $city, PDO::PARAM_STR);
			$prepare->bindParam(':contact_person', $contact_person, PDO::PARAM_STR);
			$prepare->bindParam(':phone_number', $phone_number, PDO::PARAM_INT);
			$prepare->bindParam(':email', $email, PDO::PARAM_STR);
			$prepare->bindParam(':mobile_number', $mobile_number, PDO::PARAM_INT);
			$prepare->bindParam(':notes', $notes, PDO::PARAM_STR);
			$prepare->bindParam(':added_by', $added_by, PDO::PARAM_STR);
			$prepare->bindParam(':updated_by', $updated_by, PDO::PARAM_STR);
			$prepare->execute();
			$result = array();
			$result['lastId'] = $this->dbh->lastInsertId();
			$result['rowCount'] = $prepare->rowCount();
			return $result;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
	public function UpdateCustomer($array = array())
	{
		$customer_id = $array['customer_id'];
		$customer_number = $array['customer_number'];
		$city = $array['city'];
		$contact_person = $array['contact_person'];
		$phone_number = $array['phone_number'];
		$mobile_number = $array['mobile_number'];
		$email = $array['email'];
		$notes = $array['notes'];
		$address = $array['address'];
		$added_by = $array['added_by'];
		$updated_by = $array['updated_by'];

		try {
			$stmt = "UPDATE spl_customers SET customer_name=:customer_name,address=:address,city=:city,contact_person=:contact_person,phone_number=:phone_number,email=:email,mobile_number=:mobile_number,notes=:notes,added_by=:added_by,updated_by=:updated_by WHERE customer_id=:customer_id AND added_by=:added_by";
			$prepare = $this->dbh->prepare($stmt);
			$prepare->bindParam(':customer_id', $customer_id, PDO::PARAM_INT);
			$prepare->bindParam(':customer_number', $customer_number, PDO::PARAM_STR);
			$prepare->bindParam(':customer_name', $contact_person, PDO::PARAM_STR);
			$prepare->bindParam(':address', $address, PDO::PARAM_STR);
			$prepare->bindParam(':city', $city, PDO::PARAM_STR);
			$prepare->bindParam(':contact_person', $contact_person, PDO::PARAM_STR);
			$prepare->bindParam(':phone_number', $phone_number, PDO::PARAM_STR);
			$prepare->bindParam(':email', $email, PDO::PARAM_STR);
			$prepare->bindParam(':mobile_number', $mobile_number, PDO::PARAM_STR);
			$prepare->bindParam(':notes', $notes, PDO::PARAM_STR);
			$prepare->bindParam(':added_by', $added_by, PDO::PARAM_STR);
			$prepare->bindParam(':updated_by', $updated_by, PDO::PARAM_STR);
			$prepare->execute();
			$result = $prepare->rowCount();
			return $result;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
	public function DelCustomer($array = array()) {
		$customer_id = $array['customer_id'];
		$added_by = $array['added_by'];
		try {
			$stmt = "DELETE FROM spl_customers WHERE customer_id=:customer_id AND added_by=:added_by";
			$prepare = $this->dbh->prepare($stmt);
			$prepare->bindParam(':customer_id', $customer_id,PDO::PARAM_INT);
			$prepare->bindParam(':added_by', $added_by,PDO::PARAM_STR);
			$prepare->execute();
			$result = $prepare->rowCount();
			return $result;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
	public function getRecord($array = array()) {
		$customer_id = $array['customer_id'];
		$added_by = $array['added_by'];
		try {
			$stmt = "SELECT * FROM spl_customers WHERE customer_id=:customer_id AND added_by=:added_by";
			$prepare = $this->dbh->prepare($stmt);
			$prepare->bindParam(':customer_id', $customer_id,PDO::PARAM_INT);
			$prepare->bindParam(':added_by', $added_by,PDO::PARAM_STR);
			$prepare->execute();
			$result = array();
			$result['rowCount'] = $prepare->rowCount();
			$result['data'] = $prepare->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
}