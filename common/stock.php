<?php 

/**
* 
*/
class Stock
{
	private $host = 'localhost';
	private $dbname = 'stock';
	private $user = 'root';
	private $pass = '';
	public $dbh;

	function __construct()
	{
		$this->connection();
	}
	private function connection()
	{
		try{
			$this->dbh = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname, $this->user, $this->pass);	
		}catch (PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	public function FetchAll($type=null){
		try {
			$stmt = 'SELECT * FROM `spl_stock_items`';
			$prepare = $this->dbh->prepare($stmt);
			$prepare->execute();
			if(strtolower($type) == 'assoc'){
				$result = $prepare->fetchAll(PDO::FETCH_ASSOC);
			}else if(strtolower($type) == 'both'){
				$result = $prepare->fetchAll(PDO::FETCH_BOTH);
			}else if(strtolower($type) == 'array'){
				$result = $prepare->fetchAll(PDO::FETCH_NUM);
			}else{
				$result = $prepare->fetchAll(PDO::FETCH_OBJ);
			}
			return $result;
		} catch (PDOException $e) {
		    die("Error!: " . $e->getMessage() . "<br/>");
		}
	}
	public function SearchQuery($value = ''){
		try {
			$stmt = "SELECT * FROM spl_stock_items WHERE stock_name LIKE :value ";
			$prepare = $this->dbh->prepare($stmt);
			$prepare->execute(array(':value' => $value.'%'));
			// $prepare->bindParam(':value', '%'.$value.'%', PDO::PARAM_STR);
			// $prepare->bindParam(':value1', '%'.$value.'%', PDO::PARAM_STR);
			//$prepare->execute();
			//$rec = $prepare->fetchAll(PDO::FETCH_BOTH);
			$a_json = array();
			$row = $prepare->fetchAll(PDO::FETCH_ASSOC);
			foreach ($row as $k => $v) {
				
				$stock_id = htmlentities(stripslashes($v['stock_id']));
				$stock_name = htmlentities(stripslashes($v['stock_name']));
				$purchasing_price = htmlentities(stripslashes($v['purchasing_price']));
				$selling_price = htmlentities(stripslashes($v['selling_price']));
				$quantity = htmlentities(stripslashes($v['quantity']));
				
				
				$a_json_row["id"] = $stock_id;
				$a_json_row["value"] = $stock_name;
				$a_json_row["label"] = $stock_name;
				
				$a_json_row["purchasing_price"] = $purchasing_price;
				$a_json_row["selling_price"] = $selling_price;
				$a_json_row["quantity"] = $quantity;
				
				array_push($a_json, $a_json_row);
			}
			//$count = $prepare->rowCount();
			//$result = array('total_records'=> $count, 'records' => $rec);

			return $a_json;
		} catch (PDOException $e) {
			echo $e->getMessage();
			return $e->getMessage();
		}
		
	}
	public function nextRecord($type=null){
		$new_cumtomer_prefix = 'IMSSTK00';
		try {
			$stmt = 'SELECT MAX(customer_id)as id FROM `spl_customers`';
			$prepare = $this->dbh->prepare($stmt);
			$prepare->execute();
			$result = $prepare->fetchAll(PDO::FETCH_OBJ);
			$result = $new_cumtomer_prefix.($result[0]->id + 1);
			return $result;
		} catch (PDOException $e) {
		    die("Error!: " . $e->getMessage() . "<br/>");
		}
	}
	public function NewStock($array = array())
	{
		$stock_name = $array['ns_stock_name'];
		$purchasing_price = $array['ns_purchasing_price'];
		$selling_price = $array['ns_selling_price'];
		$quantity = $array['ns_quantity'];
		$added_by = $array['added_by'];
		$updated_by = $array['updated_by'];
		 
		try {
			$stmt = "INSERT INTO spl_stock_items (stock_name, purchasing_price, selling_price, quantity,added_by, updated_by) VALUES (:stock_name, :purchasing_price, :selling_price, :quantity, :added_by, :updated_by)";

			$prepare = $this->dbh->prepare($stmt);
			$prepare->bindParam(':stock_name', $stock_name,PDO::PARAM_STR);
			$prepare->bindParam(':purchasing_price', $purchasing_price,PDO::PARAM_STR);
			$prepare->bindParam(':selling_price', $selling_price,PDO::PARAM_STR);
			$prepare->bindParam(':quantity', $quantity,PDO::PARAM_STR);
			$prepare->bindParam(':added_by', $added_by,PDO::PARAM_STR);
			$prepare->bindParam(':updated_by', $updated_by,PDO::PARAM_STR);
			$prepare->execute();
			$result = array();
			$result['lastId'] = $this->dbh->lastInsertId();
			$result['rowCount'] = $prepare->rowCount();
			return $result;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function UpdateStock($array = array())
	{
		$stock_id = $array['stock_name'];
		$stock_name = $array['stock_name'];
		$purchasing_price = $array['purchasing_price'];
		$selling_price = $array['selling_price'];
		$quantity = $array['quantity'];
		$added_by = $array['added_by'];
		$updated_by = $array['updated_by'];

		try {
			$stmt = "UPDATE spl_stock_items SET stock_name=:stock_id, purchasing_price=:purchasing_price, selling_price=:selling_price, quantity=:quantity WHERE stock_id = :stock_id AND added_by = :added_by";
			$prepare = $this->dbh->prepare($stmt);
			$prepare->bindParam(':stock_id', $stock_id,PDO::PARAM_INT);
			$prepare->bindParam(':stock_name', $stock_name,PDO::PARAM_STR);
			$prepare->bindParam(':purchasing_price', $purchasing_price,PDO::PARAM_STR);
			$prepare->bindParam(':selling_price', $selling_price,PDO::PARAM_STR);
			$prepare->bindParam(':quantity', $quantity,PDO::PARAM_STR);
			$prepare->bindParam(':added_by', $added_by,PDO::PARAM_STR);
			$prepare->execute();
			$result = array();
			$result['rowCount'] = $prepare->rowCount();
			return $result;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

	public function DelStockItem($array = array()) {
		$stock_id = $array['stock_id'];
		$added_by = $array['added_by'];
		try {
			$stmt = "DELETE FROM spl_stock_items WHERE stock_id=:stock_id AND added_by=:added_by";
			$prepare = $this->dbh->prepare($stmt);
			$prepare->bindParam(':stock_id', $stock_id,PDO::PARAM_INT);
			$prepare->bindParam(':added_by', $added_by,PDO::PARAM_STR);
			$prepare->execute();
			$result = $prepare->rowCount();
			return $result;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}

}


?>