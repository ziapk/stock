<?php 

/**
* 
*/
class Sales
{
	private $host = 'localhost';
	private $dbname = 'stock';
	private $user = 'root';
	private $pass = '';
	public $dbh;

	function __construct()
	{
		$this->connection();
	}
	private function connection()
	{
		try{
			$this->dbh = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname, $this->user, $this->pass);	
		}catch (PDOException $e){
			print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	public function nextRecord($type=null){
		$new_cumtomer_prefix = 'IMSSLC00';
		try {
			$stmt = 'SELECT MAX(customer_id)as id FROM `spl_customers`';
			$prepare = $this->dbh->prepare($stmt);
			$prepare->execute();
			$result = $prepare->fetchAll(PDO::FETCH_OBJ);
			$result = $new_cumtomer_prefix.($result[0]->id + 1);
			return $result;
		} catch (PDOException $e) {
		    die("Error!: " . $e->getMessage() . "<br/>");
		}
	}
}