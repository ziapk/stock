<?php
class Dbase {
	/*var $db_format;
	var $dbLink;					// database link
	var $USER_NAME = "";
	var $USER_PWD = "";
	var $DASQL_HOST = "";  
	var $dbTABASE;
	var $MY_selected;
	 var $num;*/

	var $db_prefix="spl_";

	function Dbase($db_nsme=""){
		$this->USER_NAME = "root";
		$this->USER_PWD = "";
		
		if ($db_nsme=="") {
			$this->DATABASE = "stock";
		} else {
			$this->DATABASE = $db_nsme;
		}
		$this->MYSQL_HOST = "localhost";

		if ($this->connectHost()) {
			if (!($this->connectDB())) {
				exit("There is some problem with Database Connections, please contact Hybrid Retail administrator! <br>".$this->DATABASE."<br>".mysql_error());
			}
		} else {
			exit("There is some problem with Database Connections, please contact Hybrid Retail administrator! <br>".$this->MYSQL_HOST."<br>".mysql_error());
		}
	} //  end of Admin

	function connectHost() {
		//echo $this->MYSQL_HOST."<br>";
		//echo $this->USER_NAME."<br>".$this->DATABASE."<br>".$this->dbLink."<br>";
		$this->dbLink = mysql_connect($this->MYSQL_HOST, $this->USER_NAME, $this->USER_PWD, TRUE);
		if (!$this->dbLink) {
			//exit("false");
			return false;
		}
		//echo $this->dbLink."<br>"; 
		return true;
	}

	function get_db_name() {
		return $this->DATABASE;
	}

	function set_db_name($db_name) {
		$this->DATABASE = $db_name;
	}
	
	function connectDB() {
		$this->db_selected = mysql_select_db($this->DATABASE,$this->dbLink);
		if (!$this->db_selected) {
			return false;
		}
		//echo $this->dbLink."<br>".$this->DATABASE; 
		return true;
	}

	function escape($str="")
	{
		return(mysql_escape_string($str)); 
	}
	//function insert(array $data, string table)....used by farooq
	function insert($data, $table) {
		if(!is_array($data))
			return(0);
		
		foreach($data as $key => $name	) {
				$attribs[]	=	$key;
				$values[]	=	"'" . $this->escape(stripslashes($name)) . "'";
		}	
		$attribs=implode(",", $attribs);
		$values = implode(",", $values);
		$query = "insert into $table ($attribs) values ($values)";
		$this->sql = $query;
		 //echo $this->sql; exit;
		
		if (mysql_query($query, $this->dbLink)) {
			return mysql_insert_id();
		} else {
			////$this->error_log();
			return false;
		}
	}
	
	

	function insert_ignoreDuplicates($data, $table) {
		if(!is_array($data))
			return(0);
		
		foreach($data as $key => $name) {
			$attribs[]	=	$key;
			$values[]	=	"'" . $this->escape(stripslashes($name)) . "'";
		}	
		$attribs=implode(",", $attribs);
		$values = implode(",", $values);
		$query = "insert into $table ($attribs) values ($values)";
		$this->sql = $query;
		//$this->log();
		@mysql_query($query, $this->dbLink);
	}
	
	//Date and time function created by farooq
	function date_time($formate) {
		 
		//return only date 
		if($formate == 'D'){
			return date('Y-m-d');
		}
		//return date and time 
		if($formate == 'DT'){
			date_default_timezone_set("Asia/Karachi");
			return date('Y-m-d h:m A');
		}
	}
	
	//Create customer function created by farooq
	function autoincrement($tablename, $filedname) {
			$q = "select MAX(".$filedname.")as RET  from ".$tablename;
			$r = mysql_query($q, $this->dbLink);
			$this->sql = $q;
			if (!($r)) {
				$this->error_log();
			}
			$row=mysql_fetch_object($r);
			if (mysql_num_rows($r)>0) {
			$inc = ++ $row->RET;
				if($inc == 1){
					if($tablename == PREFIX.'customers')
						$prefix='Customer-00';
					if($tablename == PREFIX.'suppliers')
						$prefix='Supplier-00';
					if($tablename == PREFIX.'sales')
						$prefix='Sales-00';
					if($tablename == PREFIX.'laptops_repairing')
						$prefix='Job-00';
					if($tablename == PREFIX.'purchases')
						$prefix='Purchases-00';
					if($tablename == PREFIX.'stock_items')
						$prefix='Stock-00';
					return $prefix.$inc;
				}
				else
				return $inc;
			}  
		
		
		 // echo"<pre>"; print_r($res); exit;
//		  echo $customer_number = $data_array['customer_number']; exit;
//		  echo $only_number = substr($customer_number, 9); 
	 
	}
	
	//Send test SMS created by farooq
	function Send_SMS($data, $table) {
		if(!is_array($data))
			return(0);
		
		foreach($data as $key => $name	) {
				$attribs[]	=	$key;
				$values[]	=	"'" . $this->escape(stripslashes($name)) . "'";
		}	
		$attribs=implode(",", $attribs);
		$values = implode(",", $values);
		
		//Pick mask name form DB
		$maske_name = $this->select('maske_name',PREFIX.'sms_settings');
		
		//Executing SMS Sending query here and if success, then return success msg otherwise return error msg based on return codes
		
		//execute this query only if msg success
		$query = "insert into $table ($attribs) values ($values)";
		$this->sql = $query;
		
		if (mysql_query($query, $this->dbLink)) {
			return mysql_insert_id();
		} else {
			////$this->error_log();
			return false;
		}
	}
	function exec_query($query) {
		$this->sql = $query;
		if (mysql_query($query, $this->dbLink)) {
			return true;
		} else {
			return false;
		}
		
	
	}
	function execute_query($query) {
		$this->sql = $query;
		if ($r = mysql_query($query, $this->dbLink)) {
			return $r;
		} else {
			return false;
		}
		
	
	}
	function selectIndex($retField, $table, $index, $value) {
		$q = "select $retField as RET from $table Where $index=$value";
		$r = mysql_query($q, $this->dbLink);
		$this->sql = $q;
		if (!($r)) {
			//$this->error_log();
		}
		$row=mysql_fetch_object($r);
		if (mysql_num_rows($r)>0) {
			return $row->RET;
		} else {
			return false;
		}
	}
	/////////////////////////////////////////////////////////////////////////////
	function activitylog($id,$activity)
	{
	
		$userid=$id;
		
		$newactivity=$activity;
		
		$data_activity=array("activityID","uid","act_one","act_two","act_three");
		
		$table_activity="tb_userActivity";
		
		$row_activity=$this->selectSRow($data_activity,$table_activity,"uid=$userid");
		
		$actID=$row_activity["activityID"];
		
		$activity2=$row_activity["act_one"];
		
		$activity3=$row_activity["act_two"];
		
		$update_activity=array('act_one'=>$newactivity,'act_two'=>$activity2,'act_three'=>$activity3);

		$update=$this->updateCondition("tb_useractivity","activityID='$actID'",$update_activity);
		
		
		return $update;
		
		
	
	}
	
	
	//Select query for multiple rows ...........Used by Farooq
	function select($retField, $table, $where="", $groupby="", $orderby="", $limit="") {
		$fields = implode(",", $retField);
		if ($where!="") {
			$q = "select $fields from $table WHERE $where";
		} else {
			$q = "select $fields from $table";
		}
		if ($groupby!="") {
			$q .= " GROUP BY $groupby";
		}
		if ($orderby!="") {
			$q .= " ORDER BY $orderby";
		}
		if ($limit!="") {
			$q .= " LIMIT $limit";
		}
		
		$this->sql = $q;
	 
		//$this->log();
		$r = mysql_query($q, $this->dbLink);
		$num=mysql_num_rows($r);
		if (!($r)) {
			//$this->error_log();
		}
		$this->num=mysql_num_rows($r);
		$i=1;
		while ($row=mysql_fetch_object($r)) {
			$cont[$i] = $row;
			$i++;
		}
		if (mysql_num_rows($r)>0) {
			return $cont;
		}
		
	}
	
	//Function used by Farooq 
	function countfields($retField, $table, $where="") {
		if(is_array($retField)){
			$fields = implode(",", $retField);
		}else{
			$fields=$retField;
		}
		if ($where!="") {
			$q = "select $fields from $table WHERE $where";
		
			$this->sql = $q;
			//echo $q; exit;
			//$this->log();
			$r = mysql_query($q, $this->dbLink);
			return mysql_num_rows($r);
		}
		if ($where=="") {
			$q = "select $fields from $table ";
			$this->sql = $q;
			//$this->log();
			$r = mysql_query($q, $this->dbLink);
			return mysql_num_rows($r);
		}
	}

	//select some fields used by Farooq
	function selectfeilds($retField, $table, $where="") {
		if(is_array($retField)){
			$fields = implode(",", $retField);
		}else{
			$fields=$retField;
		}
		if ($where!="") {
			$q = "select $fields from $table WHERE $where";

		$this->sql = $q;
		//echo  $q; exit;
		//$this->log();
		$r = mysql_query($q, $this->dbLink);
		if (!($r)) {
			//echo"Error : ".$this->error_log(); exit;
		}
		$row=mysql_fetch_array($r);
		//print_r($row); exit;
		return $row;
		}
	}
	
	//** function select (array $retField, string $table, string $where)
	function select_orderby($table, $where="" , $orderby="") {
		$q="SHOW COLUMNS FROM $table";
		$r = mysql_query($q, $this->dbLink);
		while ($res=mysql_fetch_array($r)) { 
			//echo $res[1]."<br>"; 
			if (($res[1]=="timestamp14") || ($res[1]=="datetime")) {
				$retField[]="DATE_FORMAT($res[0], '%d %b %Y at %H:%i:%s') AS $res[0]";
			} else {
				$retField[]=$res[0];
			}
		}
		
		$fields = implode(",", $retField);
		$q = "select $fields from $table $where ORDER BY $orderby";
		$this->sql = $q;
		//echo $q; exit;
		////$this->log();
		$r = mysql_query($q, $this->dbLink);
		$num=mysql_num_rows($r);
		$i=1;
		while ($row=mysql_fetch_object($r)) {
			$cont[$i] = $row;
			$i++;
		}
		if (mysql_num_rows($r)>0) {
			return $cont;
		}
	}
	
	//** function select (array $retField, string $table, string $where)
	function selectAll($table, $where="") {
		$q="SHOW COLUMNS FROM $table";
		$r = mysql_query($q, $this->dbLink);
		while ($res=mysql_fetch_array($r)) { 
			//echo $res[1]."<br>"; 
			if (($res[1]=="timestamp14") || ($res[1]=="datetime")) {
				$retField[]="DATE_FORMAT($res[0], '%d %b %Y at %H:%i:%s') AS $res[0]";
			} else {
				$retField[]=$res[0];
			}
		}
		
		$fields = implode(",", $retField);
		$q = "select $fields from $table $where";
		$this->sql = $q;
		//echo $q; exit;
		////$this->log();
		$r = mysql_query($q, $this->dbLink);
		$num=mysql_num_rows($r);
		$i=1;
		while ($row=mysql_fetch_object($r)) {
			$cont[$i] = $row;
			$i++;
		}
		if (mysql_num_rows($r)>0) {
			return $cont;
		}
	}
	
	//This function is Used by _can view function below ........Farooq
	function selectSRow($retField, $table, $where="", $groupby="", $orderby="", $limit="") {
		$fields = implode(",", $retField);
		if ($where!="") {
			$q = "select $fields from $table WHERE $where";
		} else {
			$q = "select $fields from $table";
		}
		if ($groupby!="") {
			$q .= " GROUP BY $groupby";
		}
		if ($orderby!="") {
			$q .= " ORDER BY $orderby";
		}
		if ($limit!="") {
			$q .= " LIMIT $limit";
		}
		 $this->sql = $q;
		 //echo $q;    
		//$this->log();
		//echo $this->DATABASE; exit;
		$r = mysql_query($q, $this->dbLink);
		$num=mysql_num_rows($r);
		
		if (!($r)) {
			//$this->error_log();
		}
		$num=mysql_num_rows($r);
		$i=1;
		$cont=array();
		$row=mysql_fetch_array($r); 
		$cont = $row;
		$i++;
		return $cont;
	}

	function lastID() {
		return mysql_insert_id();
	}

	function selectIfExist($retField, $table, $where, $groupby="", $orderby="", $limit="") {
		$fields = implode(",", $retField);
		if ($where!="") {
			$q = "select $fields from $table WHERE $where";
		} else {
			$q = "select $fields from $table";
		}
		if ($groupby!="") {
			$q .= " GROUP BY $groupby";
		}
		if ($orderby!="") {
			$q .= " ORDER BY $orderby";
		}
		if ($limit!="") {
			$q .= " LIMIT $limit";
		}
		//$q = "select $fields from $table $where";
		$this->sql = $q;
		//$this->log();
		$r = mysql_query($q, $this->dbLink);
		if (!($r)) {
			//$this->error_log();
		}
		$num=mysql_num_rows($r);
		//echo "query $q result = ".$num."<br><br><br><br>";
		if ($num!=0) {
			return true;
		}
		return false;
	}

	function is_url( $url )	{
		 if ( !( $parts = @parse_url( $url ) ) )
			  return false;
		 else {
		 if ( $parts[scheme] != "http" && $parts[scheme] != "https" && $parts[scheme] != "ftp" && $parts[scheme] != "gopher" )
			  return false;
		 else if ( !eregi( "^[0-9a-z]([-.]?[0-9a-z])*\.[a-z]{2,3}$", $parts[host], $regs ) )
			  return false;
		 else if ( !eregi( "^([0-9a-z-]|[\_])*$", $parts[user], $regs ) )
			  return false;
		 else if ( !eregi( "^([0-9a-z-]|[\_])*$", $parts[pass], $regs ) )
			  return false;
		 else if ( !eregi( "^[0-9a-z/_\.@~\-]*$", $parts[path], $regs ) )
			  return false;
		 else if ( !eregi( "^[0-9a-z?&=#\,]*$", $parts[query], $regs ) )
			  return false;
		 }
		 return true;
	}
	
	function lib_getmicrotime() { 
		 list($usec, $sec) = explode(" ",microtime()); 
		 return ((float)$usec + (float)$sec); 
  	}

	function log() {
		$fp = fopen("sql.log", "a");
		if(flock($fp, LOCK_EX))
		{
			$sql = str_replace("\n", " ", $this->sql);
			fputs($fp, date("d-m-Y h:i:s")." --> $sql\n");
			flock($fp, LOCK_UN);
		}
		fclose($fp);
	}
	
	
	function error_log() {
		$fp = fopen("sql_error.log", "a");
		if(flock($fp, LOCK_EX))
		{
			$sql = str_replace("\n", " ", $this->sql);
			fputs($fp, date("d-m-Y h:i:s")." --> $sql\n");
			flock($fp, LOCK_UN);
		}
		fclose($fp);
		
		$strHTML = "<HTML><HEAD><TITLE>MYSQL DEBUG CONSOLE</TITLE></HEAD><BODY>";
		$strHTML .= "<div id='mysql_error_div'><table width='70%' align='center' border='0' cellspacing='0' cellpadding='0'>";
		$strHTML .="<tr><td width='1%' align='center' bordercolor='#000000' bgcolor='#FF0000'>&nbsp;</td>";
		$strHTML .="<td width='98%' align='center' bordercolor='#000000' bgcolor='#FF0000'><font color=#FFFFFF face='verdana' size='+1'>MySQL DEBUG CONSOLE</font> </td>";
		$strHTML .="<td width='1%' align='center' bordercolor='#000000' bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td bgcolor='#FF0000'>&nbsp;</td><td>&nbsp;</td><td bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td bgcolor='#FF0000'>&nbsp;</td><td style='padding-left:10px'><strong>Query:</strong></td><td bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td bgcolor='#FF0000'>&nbsp;</td><td style='padding-left:20px'>$sql</td><td bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td bgcolor='#FF0000'>&nbsp;</td><td>&nbsp;</td><td bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td bgcolor='#FF0000'>&nbsp;</td><td style='padding-left:10px'><strong>Mysql Response:</strong></td><td bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td bgcolor='#FF0000'>&nbsp;</td><td style='padding-left:20px'>".mysql_error()."</td><td bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td bgcolor='#FF0000'>&nbsp;</td><td>&nbsp;</td><td bgcolor='#FF0000'>&nbsp;</td></tr>";
		$strHTML .="<tr><td colspan='3' bgcolor='#FF0000' height='2'></td></tr></table>";
		$strHTML .= "</div></BODY></HTML>";

		echo $strHTML;
	 }
	
	
	//Update all records ..Used by Farooq
	 function update($table="", $data_array="") {
		if(!is_array($data_array))
			return(0);
		
		$sql = array();
		while(list($k,$v) = each($data_array))
		{
			$sql[] = "$k='" . $this->escape(stripslashes($v)) . "'";
		}
		
		$query = "UPDATE $table SET " . implode(", ", $sql);
		$this->sql = $query;
		//echo $query; exit;
		return mysql_query($query, $this->dbLink);
	 }


	//Used by farooq for updaation
	 function updateCondition($arr=array(), $table="",$cond="" ) {

		if(!is_array($arr))
			return(0);
		
		$sql = array();
		while(list($k,$v) = each($arr))
		{
			$sql[] = "$k='" . $this->escape(stripslashes($v)) . "'";
		}
		
		  $query = "UPDATE $table SET " . implode(", ", $sql) . " WHERE $cond";
		//$this->tz = $this->lib_getmicrotime();
		 $this->sql = $query;
		 //echo $query; exit;
		//$this->log();
		return mysql_query($query, $this->dbLink);
	 }
	 
	 
	function delete( $condition="" , $table="") {
		$query = "DELETE FROM $table WHERE $condition";
		$this->sql = $query;
		//echo $this->sql; exit;
		if (!(mysql_query($query, $this->dbLink))) {
			//$this->error_log();
			return false;
		} else {
			return true;
		}
	 }

	function deleteAll($table="") {
		$query = "TRUNCATE $table";
		//$this->tz = $this->lib_getmicrotime();
		$this->sql = $query;
		//$this->log();
		if (!(mysql_query($query, $this->dbLink))) {
			//$this->error_log();
			return false;
		} else {
			return true;
		}
	 }

	function selectRows($table, $where="") {
		$q="SHOW COLUMNS FROM $table";
		$r = mysql_query($q, $this->dbLink);
		while ($res=mysql_fetch_array($r)) { 
			//echo $res[1]."<br>"; 
			if (($res[1]=="timestamp14") || ($res[1]=="datetime")) {
				$retField[]="DATE_FORMAT($res[0], '%d %b %Y at %H:%i:%s') AS $res[0]";
			} else {
				$retField[]=$res[0];
			}
		}
		
		$fields = implode(",", $retField);
		$q = "select $fields from $table $where";
		$this->sql = $q;
		//$this->log();
		$r = mysql_query($q, $this->dbLink);
		$num=mysql_num_rows($r);
		$i=1;
		while ($row=mysql_fetch_array($r)) {
			$cont[$i] = $row;
			$i++;
		}
		if (mysql_num_rows($r)>0) {
			return $cont;
		}
	}
	
	function select_array($retField, $table, $where="", $groupby="", $orderby="", $limit="") {
		$fields = implode(",", $retField);
		if ($where!="") {
			$q = "select $fields from $table WHERE $where";
		} else {
			$q = "select $fields from $table";
		}
		if ($groupby!="") {
			$q .= " GROUP BY $groupby";
		}
		if ($orderby!="") {
			$q .= " ORDER BY $orderby";
		}
		if ($limit!="") {
			$q .= " LIMIT $limit";
		}
		//echo "$q";exit;
		$this->sql = $q;
		//$this->log();
		$r = mysql_query($q, $this->dbLink);
		if (!($r)) {
			//$this->error_log();
		}
		$num=mysql_num_rows($r);
		$i=1;
		while ($row=mysql_fetch_array($r)) {
			$cont[$i] = $row;
			$i++;
		}
		if (mysql_num_rows($r)>0) {
			
		//	echo print_r($cont);
		//	exit;
			
			return $cont;
		}
	}
	
	function hide_password($input) {
    		return str_repeat("*", strlen($input)); 
	}
	function _can($action="view",$moduleAction = "")
	{
		 
		$admin_login = $_SESSION["admin_id"];
		if($admin_login == 1)
		{

			return true;

		}
 		else

		{

			$actionPerform ="" ;

			if(is_string($action))

			{
 				if($action=='add')
					$actionPerform = '1';

				if($action=='edit')

				   $actionPerform = '2';

				if($action=='delete')

					$actionPerform = '3';

				if($action=='view')

					$actionPerform = '4';

				if($action=='send')

					$actionPerform = '5';

				if($action=='block')
					$actionPerform = '6';

				if($actionPerform)

				{

					if($moduleAction == "")

					{

					$moduleAction = mysql_real_escape_string($_GET["action"]);

					}

				

					

					$getRole = $_SESSION["user_role"];

					

					

					$getModuleId = $this->selectSRow(array("id,associated_id"),PREFIX."modules","action_page = '$moduleAction'");

					

					$module = $getModuleId["id"];

					$associated_id = $getModuleId["associated_id"];

					//echo "!!!";

					if($associated_id!=0)

					$module = $associated_id;

					$rolePerMitions = $this->selectSRow(array("id"),PREFIX."user_control_module","roll_id = '$getRole' AND modul_id ='$module' AND control_id LIKE '%$actionPerform%'");

					//echo $this->sql;

					if($rolePerMitions )//|| $associated_id==-1

					{

					return true;

					}

					else

					{

					return false;

					}

				}

			

			}

		}

	}
	
	
}
 



?>