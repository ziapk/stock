<?php          
function image_resize( $file , $width_new, $height_new)
{					   
  list($width, $height, $type) = getimagesize($file) ;                                                          
  $modwidth = $width_new;  
  
   $diff = $height / $width;
  $modheight = $height_new; 
                                                       
  $image_type = image_type_to_mime_type($type);  
  
  $tn = imagecreatetruecolor($modwidth, $modheight) ;  
  
  // create according to image type
  if ($image_type =='image/png' || $image_type =='image/x-png') 
  {
  $image = imagecreatefrompng($file) ; 
  }
  
  if ($image_type =='image/jpeg' || $image_type =='image/pjpeg') 
  {
  $image = imagecreatefromjpeg($file) ; 
  }
  
  if ($image_type =='image/gif') 
  {
  $image = imagecreatefromgif($file) ; 
  }
  // end of create according to image type
  imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ; 
		
  if ($image_type =='image/png' || $image_type =='image/x-png') 
  {
	imagepng($tn, $file);
  }
  
  if ($image_type =='image/jpeg' || $image_type =='image/pjpeg') 
  {
    imagejpeg($tn, $file, 100) ; 
  }
  
  if ($image_type =='image/gif') 
  {
  	imagegif($tn, $file);
  }								
        
}

function create_thumb( $file, $target , $width_new, $height_new)
{					
  
  list($width, $height,$type) = getimagesize($file) ;                                                          
  $modwidth = $width_new; 
   $diff = $height / $width;
  $modheight = $height_new; 
  $image_type = image_type_to_mime_type($type);
  
  $tn = imagecreatetruecolor($modwidth, $modheight) ;
  
  // create according to image type
  if ($image_type =='image/png' || $image_type =='image/x-png') 
  {
  $image = imagecreatefrompng($file) ; 
  }
  if ($image_type =='image/jpeg' || $image_type =='image/pjpeg') 
  {
  $image = imagecreatefromjpeg($file) ; 
  }
  if ($image_type =='image/gif') 
  {
  $image = imagecreatefromgif($file) ; 
  }
  // end of create according to image type
  imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ; 
	
	 if ($image_type =='image/png' || $image_type =='image/x-png') 
  {
	imagepng($tn, $target, 100);
  }
  if ($image_type =='image/jpeg' || $image_type =='image/pjpeg') 
  {
    imagejpeg($tn, $target, 100) ; 
  }
  if ($image_type =='image/gif') 
  {
  	imagegif($tn, $target, 100);
  }		
}


//This function is used to get the extention of the file.
	function getExtension($Filename){ 

		$Extension 	 = explode (".", $Filename);
		$Extension_i = (count($Extension) - 1);
		return $Extension[$Extension_i];
	}

	//This function is to upload file to the specfic folder and return true on success. 
	//Arguments (Filename, File Tempory Name, Path, UnLink/Delete old file)
	function uploadFile(&$file_name, $file_temp_name, $path_to_insert, $unlink=false){

		//if file exist then alter its name and upload else upload it to the specific folder.
 	$fullpath = $path_to_insert . $file_name;

		if(file_exists($fullpath)){
			if($unlink){
				unlink($fullpath);
				//echo $path_to_insert.$file_name;
				if(move_uploaded_file($file_temp_name,$path_to_insert.$file_name))
					return  true;
			}
			else{
				//change file name here and upload.

				$isExists=true;
				while($isExists){
					$randNum = rand(1,100);
					$fileextention = getExtension($file_name);
					//echo "ext = " .$fileextention;

					$without_ext_name = substr($file_name,0,(strlen($file_name)-strlen($fileextention)-1));
					//echo "name = " .$without_ext_name;

					$_newfilename = $without_ext_name . $randNum . "." .$fileextention;
					$file_name = $_newfilename;
					if(file_exists($path_to_insert.$_newfilename))
						$isExists=true;
					else{
						$isExists = false;
					}
				}

				if(move_uploaded_file($file_temp_name,$path_to_insert.$file_name))
					return  true;
				//echo $path_to_insert.$file_name;
			}
		}
		else{
			if(move_uploaded_file($file_temp_name, $path_to_insert.$file_name))
				return  true;
		}
		return false;
	}

?>