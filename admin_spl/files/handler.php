<?php
	if(isset($_GET["action"]))
	{
		$act = $_GET["action"];
		echo @$option	=	mysql_real_escape_string($_GET["option"]);
	}
	else
	{
		$act = "dashboard";
	}
	switch($act)
	{
//--------------------------------------------------- Dashbord --------------------------------------------------------------//
		case "dashboard":
			{
			require_once("pages/dashboard/dashboard.php");
			break;
			}

//--------------------------------------------------- SMS Settings --------------------------------------------------------------//
		case "sms_settings":
			{
			require_once("pages/sms/sms_settings.php");
			break;
			}
		case "edit_sms_settings":
			{
			require_once("pages/sms/edit_sms_settings.php");
			break;
			}
		case "send_test_sms":
			{
			require_once("pages/sms/send_test_sms.php");
			break;
			}
		case "view_test_sms":
			{
			require_once("pages/sms/view_test_sms.php");
			break;
			}
		case "sys_password_change":
			{
			require_once("pages/sms/sys_password_change.php");
			break;
			}

//---------------------------------------------------Manage Roles -------------------------------------------------------------//
		case "manage_roles":
		{
			require_once("pages/users/manage_roles.php");
			break;
		}
		case "manage_controls":
		{
			require_once("pages/users/manage_controls.php");
			break;
		}
		case "manage_user":
		{
			require_once("pages/users/manage_user.php");
			break;
		}
		case "add_user":
		{
			require_once("pages/users/add_user.php");
			break;
		}
		case "edit_user":
		{  
			require_once("pages/users/edit_user.php");
			break;
		}
//---------------------------------------------------Sales-----------------------------------------------------------------//
		
		case "manage_sales":
			{
			require_once("pages/sales/manage_sales.php");
			break;
			}
		 
		case "payment_salesadd":
			{
			require_once("pages/sales/payment_salesadd.php");
			break;
			}
		case "view_sale":
			{
			require_once("pages/sales/view_sale.php");
			break;
			}
		
	//------------------Purchases ---------------------------------------------------------------------------//
		  
		case "manage_purchases":
			{
			
			require_once("pages/purchases/manage_purchases.php");
			break;
			}
		
		case "payment_purchasesadd":
			{
			require_once("pages/purchases/payment_purchasesadd.php");
			break;
			}
			
		case "del_purchase":
			{
			require_once("pages/purchases/del_purchase.php");
			break;
			}
		case "view_purchase":
			{
			require_once("pages/purchases/view_purchase.php");
			break;
			}
	 
	 //------------------Customers ---------------------------------------------------------------------------//
		  
		case "manage_customers":
			{
			require_once("pages/customers/manage_customers.php");
			break;
			}
		case "del_customer":
			{
			require_once("pages/customers/del_customer.php");
			break;
			}
		case "edit_customer":
			{
			require_once("pages/customers/edit_customer.php");
			break;
			}
		case "view_customer":
			{
			require_once("pages/customers/view_customer.php");
			break;
			}
	 
	//------------------Suppliers  ---------------------------------------------------------------------------//
		  
		case "manage_suppliers":
			{
			
			require_once("pages/suppliers/manage_suppliers.php");
			break;
			}
		
		case "add_supplier":
			{
			require_once("pages/suppliers/add_supplier.php");
			break;
			}
			
		case "del_supplier":
			{
			require_once("pages/suppliers/del_supplier.php");
			break;
			}
			
		case "view_supplier":
			{
			require_once("pages/suppliers/view_supplier.php");
			break;
			}
	 
	 //------------------Stock/Product ----------------------------------------------------------------------------//
		 
		case "manage_stock":
			{
			require_once("pages/stock/manage_stock.php");
			break;
			}
		case "manage_stock_categories":
			{
			require_once("pages/stock/manage_stock_categories.php");
			break;
			}
		case "del_stockitem":
			{
			require_once("pages/stock/del_stockitem.php");
			break;
			}
		
			
	//------------------Outstandings ---------------------------------------------------------------------------//
		 
		case "purchases_outstandings":
			{
			require_once("pages/outstandings/purchases_outstandings.php");
			break;
			}
			
		case "sales_outstandings":
			{
			require_once("pages/outstandings/sales_outstandings.php");
			break;
			}
			
		case "payment_transactions":
			{
			require_once("pages/outstandings/payment_transactions.php");
			break;
			}
			
	//------------------Reports ----------------------------------------------------------------------------//
		 
		case "sales_report":
			{
			require_once("pages/reports/sales_report.php");
			break;
			}
			
		case "purchases_report":
			{
			require_once("pages/reports/purchases_report.php");
			break;
			}
			
	//------------------Laptop Repairing ----------------------------------------------------------------------------//
		 
		case "manage_laptops":
			{
			require_once("pages/laptops_repairing/manage_laptops.php");
			break;
			}
	    case "view_laptop":
			{
			require_once("pages/laptops_repairing/view_laptop.php");
			break;
			}
		case "edit_laptop":
			{
			require_once("pages/laptops_repairing/edit_laptop.php");
			break;
			}
			
	
	//------------------Help (Categories) -----------------------------------------------------------------------------//
		 
		case "help":
			{
			require_once("pages/others/help.php");
			break;
			}
			
		case "about_us":
			{
			require_once("pages/others/about_us.php");
			break;
			}
			
		case "terms_conditions":
			{
			require_once("pages/others/terms_conditions.php");
			break;
			}	 	 

			
}	
		
	
?>