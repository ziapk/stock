<div id="sidebar-wrapper">
  <br />
  <br />
  <!-- Sidebar Profile links -->
	<div id="profile-links"> 
 		<a href="#"><img id="logo" src="resources/images/logo.png" alt="Simpla Admin logo" style="  margin: 11px 43px -14px 41px; text-align: center; width: 137px; "/></a>
 	</div>
 	<span style="font-size:12px; font-weight:800; margin-left: 65px;">
 	<a href="index.php?action=sms_settings" id="st_user"> Settings</a> | <a href="logout.php" title="Sign Out">Sign Out</a></span>
  <ul id="main-nav" style="margin: 22px 0 0 15px;">
    <!-- Accordion Menu -->
    <!-- Dashboard -->
    <li> <a href="index.php?action=dashboard" id="dashboard" class="nav-top-item no-submenu">  Dashboard </a> </li>
<!-- Settings -->
<?php if($db->_can("view","sms_settings")  || $db->_can("view","send_sms") ) { ?>
	<li> <a href="#" id="settings" class="nav-top-item"> Settings </a>
		<ul id="ul_settings">
		   <?php if($db->_can("edit","sms_settings")){ ?>
		        <li><a id="sms_settings" href="index.php?action=sms_settings"> SMS Settings </a></li>
		   <?php } ?>
		    <?php if($db->_can("edit","send_test_sms")){ ?>
		        <li><a id="send_test_sms" href="index.php?action=send_test_sms">Send Test SMS</a></li>
		   <?php } ?>
		    <?php if($db->_can("edit","sys_password_change")){ ?>
		        <li><a id="sys_password_change" href="index.php?action=sys_password_change">Change System Password</a></li>
		   <?php } ?>
		</ul>
   	</li>
    <?php } ?> 
    
    
<!-- Manage Roles -->
<?php if($db->_can("view","manage_roles") || $db->_can("view","manage_controls") || $db->_can("view","manage_user") || $db->_can("view","add_user") || $db->_can("view","edit_user")) {?>    
 <li> 
	<a href="#"  id="users" class="nav-top-item"> <!-- Add the class "current" to current menu item -->
	Admin Users
	</a>
	<ul id="ul_users">
		<?php if($db->_can("view","manage_roles")){ ?>
		<li><a href="index.php?action=manage_roles" id="manage_roles">User Roles</a></li>
		<?php } ?> 
	    <?php if($db->_can("view","add_user")){ ?>
		<li><a href="index.php?action=add_user" id="add_user">Add User</a></li>
		<?php } ?>
		<?php if($db->_can("view","manage_user")){ ?>
	   	<li><a href="index.php?action=manage_user" id="manage_user">Manage Users</a></li>
		<?php } ?>
	</ul>
</li> 
<?php } ?> 
<!-- Sales -->
<?php if($db->_can("view","manage_sales") || $db->_can("view","add_sale") || $db->_can("view","del_sale") || $db->_can("view","view_sale"))
{?>    
<li style=""> 
	<a href="index.php?action=manage_sales"  id="sales" class="nav-top-item"> Sales </a>
	<ul id="ul_sales">
		<?php if($db->_can("view","manage_sales")){ ?>
		<li><a href="index.php?action=manage_sales" id="manage_sales">Manage Sales</a></li>
		<?php } ?> 
	</ul>
</li>
<?php } ?>

              
<!-- Purchases -->
<?php if($db->_can("view","manage_purchases") || $db->_can("view","add_purchase"))
{?>    
<li>
	<a href="#"  id="purchases" class="nav-top-item">Purchases</a>
	<ul id="ul_purchases">
		<?php if($db->_can("view","manage_purchases")){ ?>
    	<li><a href="index.php?action=manage_purchases" id="manage_purchases">Manage Purchases</a></li>
    	<?php } ?> 
	</ul>
</li>
<?php } ?>
<!-- Customers -->
<?php if($db->_can("view","manage_customers"))
{?>    
<li style=""><a href="index.php?action=manage_customers"  id="customers" class="nav-top-item">Customers</a>
	<ul id="ul_customers">
		<?php if($db->_can("view","manage_customers")){ ?>
    	<li><a href="index.php?action=manage_customers" id="manage_customers">Manage Customers</a></li>
    	<?php } ?> 
	</ul>
</li>
<?php } ?>
     
<!-- Suppliers -->
<?php if($db->_can("view","manage_suppliers"))
{?>    
<li style="">
	<a href="#"  id="suppliers" class="nav-top-item">Suppliers</a>
	<ul id="ul_suppliers">
		<?php if($db->_can("view","manage_suppliers")){ ?>
    	<li><a href="index.php?action=manage_suppliers" id="manage_suppliers">Manage Suppliers</a></li>
    	<?php } ?> 
	</ul>
</li>
<?php } ?>
 
<!--  Stock/Products  -->
<?php if($db->_can("view","manage_stock") || $db->_can("view","manage_stock_categories"))
{?>    
<li style=""> 
	<a href="index.php?action=manage_stock"  id="stock" class="nav-top-item">Stock/Products</a>
	<ul id="ul_stock">
		<?php if($db->_can("view","manage_stock")){ ?>
		<li><a href="index.php?action=manage_stock" id="manage_stock">Manage Stock</a></li>
		<?php } ?>
     </ul>
</li>
<?php } ?>

<!--  Outstandings -->
<?php if($db->_can("view","purchases_outstandings") || $db->_can("view","sales_outstandings") || $db->_can("view","payment_outstandings"))
{?>    
<li style="">
	<a href="#"  id="outstandings" class="nav-top-item">Outstandings</a>
	<ul id="ul_outstandings">
		<?php if($db->_can("view","purchases_outstandings")){ ?>
		<li><a href="index.php?action=purchases_outstandings" id="purchases_outstandings">Purchases Outstandings</a></li>
		<?php } ?>
		<?php if($db->_can("view","sales_outstandings")){ ?>
		<li><a href="index.php?action=sales_outstandings" id="sales_outstandings">Sales Outstandings</a></li>
		<?php } ?>
	 	<?php if($db->_can("view","payment_transactions")){ ?>
        <li><a href="index.php?action=payment_transactions" id="payment_transactions">Payment Transactions</a></li>
		<?php } ?> 
     </ul>
</li>
<?php } ?>
 
 <!-- Reports -->
<?php if($db->_can("view","sales_report") || $db->_can("view","purchases_report"))
{?>    
<li style=""> 
	<a href="#"  id="reports" class="nav-top-item">Reports</a>
	<ul id="ul_reports">
		<?php if($db->_can("view","sales_report")){ ?>
		<li><a href="index.php?action=sales_report" id="sales_report">Sales Report</a></li>
		<?php } ?>
		<?php if($db->_can("view","purchases_report")){ ?>
		<li><a href="index.php?action=purchases_report" id="purchases_report">Purchases Report</a></li>
		<?php } ?>
	</ul>
</li>
<?php } ?>
 <!-- Laptop Repairing -->
<?php if($db->_can("view","manage_laptops"))
{?>    
<li style="">
	<a href="#"  id="laptops_repairing" class="nav-top-item">Laptops Repairing</a>
	<ul id="ul_laptops_repairing">
		<?php if($db->_can("view","manage_laptops")){ ?>
			<li><a href="index.php?action=manage_laptops" id="manage_laptops">Manage Laptops </a></li>
		<?php } ?>
	</ul>
</li>
<?php } ?>
<!-- Help (Categories) -->
<?php if($db->_can("view","help") || $db->_can("view","about_us") || $db->_can("view","terms_conditions"))
{?>    
<li style="">
	<a href="#"  id="help_categories" class="nav-top-item">Help (Categories)</a>
	<ul id="ul_help_categories">
		<?php if($db->_can("view","help")){ ?>
		<li><a href="index.php?action=help" id="help">Help</a></li>
		<?php } ?>
		<?php if($db->_can("view","about_us")){ ?>
		<li><a href="index.php?action=about_us" id="about_us">About Us</a></li>
		<?php } ?>
		<?php if($db->_can("view","terms_conditions")){ ?>
		<li><a href="index.php?action=terms_conditions" id="terms_conditions">Terms Conditions</a></li>
		<?php } ?>
	</ul>
</li>
<?php } ?>
      </ul>
    </li>
  </ul>
  <!-- End #main-nav --> 
</div>

