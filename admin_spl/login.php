<?php
	require_once("../common/config.php");
	$all_rec = array("*");
	if(isset($_SESSION["admin_login"]))
	{
		header("location:index.php?action=dashboard");
	}
	if(isset($_POST['submit']))
	{
		extract($_POST);
		$uname = addslashes($uname);
		$pwd = addslashes($pwd);
		$res_admin = $db->selectSRow($all_rec,PREFIX."admin","user_name='$uname' AND password='$pwd'");
		 
		if($res_admin)
		{
			$_SESSION["admin_login"] = 1;
			$_SESSION["admin_id"] = $res_admin["admin_id"];
			$_SESSION["user_role"] = $res_admin["user_role"];
			$_SESSION["user_name"] = $res_admin["user_name"];
			?>
			<script type="text/javascript">
				location.href = "index.php";
			</script>
			<?php
		}
		else
  
		{
			$inv = 1;
		}
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<title>Sarwar Laptops | Admin Area</title>
		
		<!--                       CSS                       -->
	  
		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
		
		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />	
		
		
		<!-- jQuery -->
		<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
		
		<!-- jQuery Configuration -->
		<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>
		
		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="resources/scripts/facebox.js"></script>
		
		<!-- jQuery WYSIWYG Plugin -->
		<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>
		
		
		
	</head>
  
	<body id="login">
		
		<div id="login-wrapper" class="png_bg">
			
		<div style="width:400px;  padding:10px ; opacity:40%; margin: 207px auto; height:220px; border-radius:10px; background:#BFE6FD; border:#999999 1px solid">
			
			
					<div style=" padding-top:10px; font-size:24px; color:#333333; text-align:center;">Admin Panel</div>
				<!-- Logo (221px width) -->
<br/>	

			
			<div id="login-content">
				
				<form action="" method="post">
				<?php if(isset($inv)) { ?>
					<div class="notification information png_bg">
						<div style="color:#FF0000">
							Invalid Username or Password.
						</div>
					</div>
					<?php } ?>
					<p>
						<label>Username</label>
						<input class="text-input" type="text" name="uname" />
					</p>
					<div class="clear"></div>
					<p>
						<label>Password</label>
						<input class="text-input" type="password" name="pwd" />
					</p>
					<div class="clear"></div>
					<p id="remember-password">
						<input type="checkbox" />Remember me
					</p>
					<div class="clear"></div>
					<p>
						<input class="button" type="submit" name="submit"  value="Sign In" />
					</p>
					
				</form>
			</div> <!-- End #login-content -->
			
            
   </div>
            
		</div> <!-- End #login-wrapper -->
		
  </body>
  
</html>
