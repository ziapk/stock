<?php //--------------------------------- DELETE AND DELETE ALL PROCESS --------------------------------------// ?>


<?php
if($_GET['del'] && $_GET['del']==1 && $_GET['admin_id'] && $_GET['admin_id']!='')
{
	$admin_id = $_GET["admin_id"];
	$db->delete("admin_id=$admin_id",PREFIX."admin");

	$_SESSION["green"] = "User has been deleted successfully.";
?>
<script type="text/javascript">
location.href = "index.php?action=manage_user";
</script>	
	
<?php
exit;
}
?>


<?php
if($_GET['del_all'] && $_GET['del_all']==1 && $_GET['admin_id'] && $_GET['admin_id']!='')
{
	$admin_id = $_GET["admin_id"];
	$db->delete("admin_id IN ($admin_id)",PREFIX."admin");

	$_SESSION["green"] = "Selected User(s) have been deleted successfully.";
?>
<script type="text/javascript">
location.href = "index.php?action=manage_user";
</script>	
	
<?php
exit;
}
?>


<?php //---------------------------------END OF DELETE AND DELETE ALL PROCESS --------------------------------------// ?>






<script type="text/javascript">

			 $("#location").addClass("current");
			 $("#manage_location_details").addClass("current");
			 $("#ul_location").show("slow");

	function valid()
	{
		var cat_tit = document.getElementById("cat_tit");
	
		
		if(cat_tit.value == "")
		{
			document.getElementById("tit_error").style.visibility = "visible";
			cat_tit.focus();
			return false;
		}
		
		return true;
	}
	
	function checkBoxActions(){
var action	=	$("#sel_opr option:selected").val();	
var ids =[];	
$('.customer:checkbox:checked ').each(function(index){
ids[index] = $(this).val();
})
if(action==''){
alert('Please select an action');
return false;	
}

if(action == 'Delete'){
if(ids!=''){
	window.location="index.php?action=manage_user&del_all=1&admin_id="+ids;	
	
}else
{ alert("Please select atleast one record."); }

}
}	
</script>
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
<h2 class="align-left">
<img src="resources/images/icons/business_users.png" width="40" alt="icon" /> Manage Users</h2>

 <div class="add_btn">
<a class="button" href="index.php?action=add_user">Add User</a>
</div>

	<div class="clear"></div>
					


<div class="content-box"><!-- Start Content Box -->
				
			  <div class="content-box-header">
					
					<h3>Manage Users</h3>
                    
                
					
					
				</div> <!-- End .content-box-header -->
		<div class="content-box-content">
<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
						
						
						<table border="0" id="example">
							
							<thead>
							<?php if(isset($_SESSION["green"])) { ?>
							<tr>
							<th colspan="5"><div class="notification success png_bg">
				<a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
				<div>
					<?php echo $_SESSION["green"]; ?>
				</div>
			</div></th>
							</tr>
							<?php 
							unset($_SESSION["green"]);
							} ?>
							
								<tr>
								   <th><input class="check-all" type="checkbox" name="chk_all" id="chk_all" /></th>
								   <th>Name</th>
								   <th>Role</th>
								   <th>Email</th>
								   <th>Actions</th>
								  
								</tr>
								
							</thead>
						 
							<tfoot>
								<tr>
									<td colspan="5">
															<div class="bulk-actions align-left">
											<select name="sel_opr" id="sel_opr">
												<option value="">Choose an action...</option>
												<option value="Delete">Delete</option>
											</select>
										<a class="button" onclick="checkBoxActions()">Apply to selected</a>	 
                                            								</div>
										
										
										<?php 
										if(isset($_GET["page"]))
										{
											$page = $_GET["page"];
										}
										else
										{
										$page=1;
										}
										$table = PREFIX."admin";
										$limit = 10;
										$target_page = "index.php?action=manage_user";
										$where = "";
										
											echo $db->pagination($table,$where,$target_page,$limit,$page);
										?>
										
										 <!-- End .pagination -->
										<div class="clear"></div>
									</td>
								</tr>
							</tfoot>
						 	<?php
							$start = ($page - 1) * $limit;
							$res_all_cat = $db->select($all_rec,PREFIX."admin","","","admin_id desc","$start,$limit");
							?>
							<tbody>
							<?php
							if($res_all_cat)
							{
								$i = 1;
								foreach($res_all_cat as $rac)
								{
							?>
								<tr >
									<td><input type="checkbox" class="customer" name="cat_<?php echo $i;?>" id="cat_<?php echo $i;?>" value="<?php echo $rac->admin_id;?>" /></td>
									
									<td><?php echo $rac->Name;?></td>
									<td><?php echo $rac->user_role_text; ?></td>
									<td><?php echo $rac->email; ?></td> 
									<td>
										<!-- Icons -->
										 <a href="index.php?action=edit_user&re_id=<?php echo $rac->admin_id;?>" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
										 <a href="index.php?action=manage_user&admin_id=<?php echo $rac->admin_id;?>&del=1" onclick="return confirm('Are you sure you want to delete?');" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a> 
										
									</td>
								</tr>
								<?php
								$i++;
									}
								}
								else
								{
								?>
								<tr>
									<td colspan="5" align="center">No records found.</td>
									
									
								</tr>
								<?php
								}
								?>
								<input type="hidden" name="tot_cat" id="tot_cat" value="<?php echo $i;?>" />
							</tbody>
							
						</table>
						
		  </div> <!-- End #tab1 -->
					
					</div>
		</div>