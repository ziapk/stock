<?php

//For Sales
$total_amount_sales=$total_payment_sales=$total_balance_sales=$discount_amount_sales=$tax_amount_sales=$final_total_amount_sales=0;
$res = $db->select_array(array('*'),PREFIX."sales");
	for($p=1; $p<=count($res); $p++){
		$total_amount_sales 		= $total_amount_sales + $res[$p]['total_amount'];
		$total_payment_sales 		= $total_payment_sales + $res[$p]['total_payment'];
		$total_balance_sales		= $total_balance_sales + $res[$p]['total_balance'];
		$discount_amount_sales 		= $discount_amount_sales + $res[$p]['discount_amount'];
		$tax_amount_sales 			= $tax_amount_sales + $res[$p]['tax_amount'];
		$final_total_amount_sales 	= $final_total_amount_sales + $res[$p]['final_total_amount'];
		 
}

 

//For Purchases
$total_amount_purchases=$total_payment_purchases=$total_balance_purchases=0;
$res = $db->select_array(array('*'),PREFIX."purchases");
	for($p=1; $p<=count($res); $p++){
		$total_amount_purchases 		= $total_amount_purchases + $res[$p]['Total_Amount'];
		$total_payment_purchases 		= $total_payment_purchases + $res[$p]['Total_Payment'];
		$total_balance_purchases		= $total_balance_purchases + $res[$p]['Total_Balance'];
		
		//$final_total_amount_purchases 	= $final_total_amount_purchases + $res[$p]['final_total_amount'];
		 
}



?>

<h2><img src="resources/images/icons/dashboard.png" alt="icon" /> Dashboard</h2>

 
 

<div class="clear"></div>
<div class="content-box"><!-- Start Content Box -->
<div class="content-box-header"><h3> Sales and Purchases Overall Overview</h3></div> <!-- End .content-box-header -->
 



<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->



						




						
<div class="content-box-content">
<div style="display: block;" id="tab1" class="tab-content default-tab"> <!-- This is the target div. id must match the href of this div's tab -->
<table style="width:100%; border-collapse:separate !important;">
				<tbody>
                
                <tr style="background:none;" class="alt-row">
                
                <td width="34%" style="vertical-align:top !important; ">
				 
                  <div class="content-box-header" style="height: 26px;"><h4 style="color:white; text-align: left; padding: 5px;   "> Purchases</h4></div>
				<table style=" border: 19px solid #ccc; ">
                  <tbody>
                  
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Total Amount</td>
                      <td style="background:#E4F3FD;">
                      <?php echo  $total_amount_purchases;  ?>                    
                      
                      </td>
                    </tr>
                    					 <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Total Payment  </td>
                      <td style="background:#E4F3FD;"><?php  echo $total_payment_purchases ; ?> </td>
                    </tr>
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Total Balance  </td>
                      <td style="background:#E4F3FD;"><?php  echo $total_balance_purchases; ?> </td>
                    </tr>
                    
                    
                                      </tbody>
                </table>
				
				</td>
				
                <td width="34%" style="vertical-align:top !important; ">
				<div class="content-box-header" style="height: 26px;"><h4 style="color:white; text-align: left; padding: 5px; "> Sales</h4></div>
				<table style=" border: 19px solid #ccc; ">
                  <tbody>
                  
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Total Amount </td>
                      <td style="background:#E4F3FD;">
                     <?php echo $total_amount_sales; ?> 
                      </td>
                    </tr>
                    					 <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Discount Payment  </td>
                      <td style="background:#E4F3FD;"> <?php echo $discount_amount_sales; ?></td>
                    </tr>
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Tax Amount  </td>
                      <td style="background:#E4F3FD;"><?php echo $tax_amount_sales; ?></td>
                    </tr>
                    
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Total Payment  </td>
                      <td style="background:#E4F3FD;"><?php echo $total_payment_sales; ?></td>
                    </tr>
                    
                   
                    
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"> Total  Balance  </td>
                      <td style="background:#E4F3FD;"><?php echo $total_balance_sales; ?></td>
                    </tr>
                                      </tbody>
                </table>
				
				</td>
                		
				</tr>
				  
                  
                  <tr style="background:none;" class="alt-row">
                
                <td width="34%" style="vertical-align:top !important;">
				<div class="content-box-header" style="height: 26px;"><h4 style="color:white; text-align: left; padding: 5px;  "> Purchases Outstandings</h4></div>
				<table style=" border: 19px solid #ccc; ">
                  <tbody>
                     
                     <?php 
					 
					$result_purchases = $db->select_array(array('MAX(Supplier_ID),SUM(Total_Balance)'),PREFIX."purchases","","Supplier_ID");
					//echo"<pre>"; print_r($result_purchases);
					for($p=1; $p<=count($result_purchases); $p++){
						
					?>
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"><?php echo $result_purchases[$p]['MAX(Supplier_ID)'];  ?></td>
                      <td style="background:#E4F3FD;">
                      <?php echo $result_purchases[$p]['SUM(Total_Balance)'];  ?>                
                      
                      </td>
                    </tr>
					<?php } ?>    
                    
                   </tbody>
                </table>
				
				</td>
				
                <td width="34%" style="vertical-align:top !important;">
				<div class="content-box-header" style="height: 26px;"><h4 style="color:white; text-align: left; padding: 5px;  "> Sales Outstandings</h4></div>
				<table style=" border: 19px solid #ccc; ">
                  <tbody>
                    <?php 
					 
					$result = $db->select_array(array('MAX(customer_id),SUM(total_balance)'),PREFIX."sales","","customer_id");
					//echo"<pre>"; print_r($result);
					for($p=1; $p<=count($result); $p++){
						
					?>
                    <tr style="border-bottom:1px solid #CCC;" class="alt-row">
                      <td style="width:165px; background:#8FC1E9;"><?php echo $result[$p]['MAX(customer_id)'];  ?></td>
                      <td style="background:#E4F3FD;">
                      <?php echo $result[$p]['SUM(total_balance)'];  ?>                
                      
                      </td>
                    </tr>
					<?php } ?>                   					  
                   </tbody>
                </table>
				
				</td>
                		
				</tr>
                		
	  </tbody></table>
	  
	    				
				
		  </div>
<!-- End #tab1 -->
					
  </div>


						 



						



		  </div> <!-- End #tab1 -->



					



  </div>
 