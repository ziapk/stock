<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		
	  	extract($_POST);
		
		$new_password		= isset($_POST['new_password']) ? $_POST['new_password'] : "";
		$confirm_password	= isset($_POST['confirm_password']) ? $_POST['confirm_password'] : "";
		if($new_password != $confirm_password)
		$_SESSION["error_message"] = "Passwords does not match.";
		else
		{
			$id = $_SESSION['admin_id'];
			$upd_array = array("password"=>$new_password);
			$upd = $db->updateCondition($upd_array,PREFIX."admin","admin_id='$id'");
        	$_SESSION["edit_message"] = "System Password have been updated successfully."; 
		}
			
	}



?>

<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#change_password").validate({
			rules: {
				new_password: "required",
				confirm_password:{
				equalTo: "#new_password"
				  }  
				 
			},
			messages: {
				new_password: "Please enter valid password",
				confirm_password: "Please enter valid password",
				 
			}
		});

	});
	</script>
<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>




<h2><img width="24" alt="icon" src="resources/images/icons/setting.png">Settings</h2>    

<?php if(isset($_SESSION["edit_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["edit_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["edit_message"]); } ?> 


<?php if(isset($_SESSION["error_message"])) { ?>
                                         <div class="notification error png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["error_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["error_message"]); } ?>     

                 
 
<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3 style="cursor: s-resize;">Change System Password</h3>
					
			 
		 
        
					
					
					
					
				</div> <!-- End .content-box-header -->
		<div style="min-height:480px;" class="content-box-content">
		 <form   enctype="multipart/form-data" method="post" action="" id="change_password" name="change_password">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								
								 
                                <p>
							    <label>New Password</label>
									<input type="password"  name="new_password" id="new_password" class="text-input medium-input">								</p>
                                <p>
							    <label>Confirm Password</label>
									<input type="password"  name="confirm_password" id="confirm_password" class="text-input medium-input">								</p>
								
                                
								
								<p>
									<input type="submit" value="Update" class="button" name="submit">
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
         <!-- End #tab1 -->
					
  </div>
</div>