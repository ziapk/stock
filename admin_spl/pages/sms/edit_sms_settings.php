<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		
	  	extract($_POST);
		
		$status = isset($_POST['status']) ? $_POST['status'] : 1 ;
		$upd_array = array("username"=>$username,"password"=>$password,"mask_name"=>$mask_name,"status"=>$status);
		//echo"<pre>"; print_r($upd_array); exit;
		$upd = $db->update(PREFIX."sms_settings",$upd_array);
        $_SESSION["edit_message"] = "SMS settings have been updated successfully."; 
			 
		
		?>
		<script type="text/javascript">
            location.href = "index.php?action=sms_settings";
        </script>	
                
		<?php 
		exit();	
	}

//Fetching all records from db
$all_rec=array("*");
$rs = $db->selectAll(PREFIX."sms_settings");
foreach ($rs as $key => $value) {
    $result[] = $value->username;
	$result[] = $value->mask_name;
	$result[] = $value->status;
}
//echo"<pre>"; print_r($result); exit;
$username = $result[0];
$mask_name = $result[1];
$status = $result[2];


?>

<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#setting_form").validate({
			rules: {
				username: "required",
				password: "required",
				mask_name: "required"
			},
			messages: {
				username: "Please enter username",
				password: "Please enter password",
				mask_name: "Please enter mask name"
			}
		});

	});
	</script>
<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>




<h2><img width="24" alt="icon" src="resources/images/icons/setting.png">Settings</h2>                         
 
<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3 style="cursor: s-resize;">Update SMS Setting</h3>
					
			 
		 
        
					
					
					
					
				</div> <!-- End .content-box-header -->
		<div style="min-height:480px;" class="content-box-content">
		 <form   enctype="multipart/form-data" method="post" action="" id="setting_form" name="setting_form">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								
								<p>
							    <label>Username</label>
									<input type="text" value="<?php echo $username; ?>" name="username" id="username" class="text-input medium-input ">								</p>
								
                                <p>
                                <p>
							    <label>Password</label>
									<input type="text"  name="password" id="password" class="text-input medium-input">								</p>
                                <p>
							    <label>Mask Name</label>
									<input type="text"  name="mask_name" id="mask_name" value="<?php echo $mask_name; ?>" class="text-input medium-input">								</p>
								
                                <p>
								<label>Disable SMS</label>
								  <input type="checkbox" value="0" id="status" name="status" <?php if($status == 0) echo"checked"; ?>>
								</p>
								
								<p>
									<input type="submit" value="Update" class="button" name="submit">
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
         <!-- End #tab1 -->
					
  </div>
</div>