<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		
	     extract($_POST);
		 $data_array = array("category_name"=>$category_name,"Purchasing_Price"=>$purchasing_price,"Selling_Price"=>$selling_price,);
		 $db->insert($data_array, PREFIX."stock_categories");
		 $_SESSION["add_message"] = "Stock category have been sent successfully."; 
		 ?>
		   	<script type="text/javascript">
				location.href = "index.php?action=manage_stock_categories";
		 	</script> 
         <?php 
		 exit();       
	}

// how many records should be displayed on a page?
$res 				= $db->selectSRow(array('rows'),PREFIX."pagination_rows");
$records_per_page 	= $res['rows'];
 
 
 

?>

<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#category_form").validate({
			rules: {
				category_name: "required",
				selling_price: {
					    required: true,
     			 		number: true
				},
				purchasing_price: {
					    required: true,
     			 		number: true
				}
			},
			messages: {
				
			}
		});

	});
	</script>
<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>




<h2><img width="24" alt="icon" src="resources/images/icons/category.png" style="margin-top: 1px;">Stock Categories</h2>                         
 
 
 <?php if(isset($_SESSION["add_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["add_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["add_message"]); } ?>

 <?php if(isset($_SESSION["dell_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["dell_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["dell_message"]);  } ?>


                         
<div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form  enctype="multipart/form-data" method="post" action="" id="category_form" name="category_form">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
							   
                              <p>
							    <label>Category Name <span style="color:#FF0000">*</span> </label>
									<input type="text"  name="category_name" id="category_name" class="text-input medium-input" placeholder="Laptop">
                                   
                            </p>
                               <p>
                            <label>Purchasing Price  <span class="style1" style="color:#F00"> *</span></label> 
                            <input class="text-input medium-input" type="text" id="purchasing_price" name="purchasing_price"      />
                            </p> 
                            
                            <p>
                            <label>Selling Price  <span class="style1" style="color:#F00"> *</span></label> 
                            <input class="text-input medium-input" type="text" id="selling_price" name="selling_price"  />
                            </p> 
        
                            
								<p>
									<input type="submit" value="Submit" class="button" name="submit">
							  </p>
								
						  </fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div>
            <div class="content-box"><!-- Start Content Box -->
                            
                            <div class="content-box-header">
                                
                                <h3 style="cursor: s-resize;">Stock Categories List</h3>
                                
                           </div> <!-- End .content-box-header -->
                    <div style="min-height:480px;" class="content-box-content">
            <div id="tab1" class="tab-content default-tab" style="display: block;"> <!-- This is the target div. id must match the href of this div's tab -->
              <label>Show 
                    <select name="rows"  id="rows" size="1" aria-controls="example" onchange="select_rows(this.value)" >						
                    				<option value="10" <?PHP if($records_per_page == 10) echo"selected";?>>10</option>
                                    <option value="25" <?PHP if($records_per_page == 25) echo"selected";?>>25</option>
                                    <option value="50" <?PHP if($records_per_page == 50) echo"selected";?>>50</option>
                                    <option value="100" <?PHP if($records_per_page == 100) echo"selected";?>>100</option>
                    </select> entries</label>
                     
                     <div class="dataTables_filter" id="example_filter" style="float: right;"><label>Search: <input type="text" aria-controls="example" value="" name="searchBox" id="search"></label></div>
                     
         
                  <div class="searchBtn">
					&nbsp;
       			</div>   

    
        
	
<br clear="all" />
<div id="content">
	<div id="sub_cont" class="search-record">	</div>
</div>               

              <div role="grid" class="dataTables_wrapper" id="example_wrapper"><table id="example" class="dataTable" aria-describedby="example_info">
                <thead>
                              <tr role="row"><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Category ID</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Category Name</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Purchasing Price</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Selling Price</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Action</th></tr>
                </thead>
                
 <!--Pagination Code put everywhere u need it-->
<?php
//Fetching all records from db
$results_objects = $db->select_orderby(PREFIX."stock_categories","","Category_ID DESC");
$Category_ID_array = array();
foreach ($results_objects as $key => $value) {
 	$Category_ID_array[] = $value->Category_ID;
}
 


// the number of total records is the number of records in the array
$pagination->records(count($Category_ID_array));
// records per page
$pagination->records_per_page($records_per_page);
// here's the magick: we need to display *only* the records for the current page
$Category_ID_array = array_slice(
    $Category_ID_array,
    (($pagination->get_page() - 1) * $records_per_page),
    $records_per_page
);

?>  
               <tfoot>
                      </tfoot>  
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach ($Category_ID_array as $index => $Category_ID):  ?>
                          
                          <tr class="odd">
                            <td>
                            <?php 
                            $Category	= $db->selectSRow(array("*"),PREFIX."stock_categories","Category_ID=$Category_ID");
                            echo $Category['Category_ID'];
                            ?>
                            </td>
                             <td>
                            <?php 
                           	  echo $Category['Category_Name'];
                            ?>
                            </td>
                             <td>
                            <?php 
                           	  echo $Category['Purchasing_Price'];
                            ?>
                            </td>
                             <td>
                            <?php 
                           	  echo $Category['Selling_Price'];
                            ?>
                            </td>
                         
                            <td class=" sorting_1">
                            <a title="View Detail" href="index.php?action=del_stock_category&Category_ID=<?php echo $Category['Category_ID'];  ?>"><img width="20" alt="Delete" src="resources/images/icons/cross.png" class="Employee" id="" onclick="return confirm('Are you sure to delete it ?');"></a>
                            </td>
                         </tr>
                    <?php endforeach?>
                            
                             
                            
                            </tbody></table>
                            
<?php
// render the pagination links
$pagination->render();

?>
                            </div>
                                    
                      </div> <!-- End #tab1 -->
                                
              </div>
            </div>