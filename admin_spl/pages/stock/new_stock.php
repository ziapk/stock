<?php 
	require_once("../../../common/stock.php");
	$stock = new Stock;
	$response = array();
	$response_errors = array();
	$everything_is_ok = true;
	if(isset($_REQUEST['ns_stock_name']) && !empty(trim($_REQUEST['ns_stock_name']))){
		$response['data']['added_by'] = $_REQUEST['added_by'];
		$response['data']['updated_by'] = $_REQUEST['updated_by'];
		$response['data']['ns_stock_name'] = $_REQUEST['ns_stock_name'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['ns_stock_name'] = 'Please check your item name';
	}

	if(isset($_REQUEST['ns_purchasing_price']) && !empty(trim($_REQUEST['ns_purchasing_price'])) && is_numeric($_REQUEST['ns_purchasing_price']) ) {
		$response['data']['ns_purchasing_price'] = $_REQUEST['ns_purchasing_price'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['ns_purchasing_price'] = 'Please check your purchasing price';
	}


	if(isset($_REQUEST['ns_selling_price']) && !empty(trim($_REQUEST['ns_selling_price'])) && is_numeric($_REQUEST['ns_selling_price'])) {
		$response['data']['ns_selling_price'] = $_REQUEST['ns_selling_price'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['ns_selling_price'] = 'Please select selling price';
	}

	if(isset($_REQUEST['ns_quantity']) && !empty(trim($_REQUEST['ns_quantity'])) && is_numeric($_REQUEST['ns_quantity'])) {
		$response['data']['ns_quantity'] = $_REQUEST['ns_quantity'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['ns_quantity'] = 'Please check your stock quantity';
	}

	if ($everything_is_ok)
	    {
	    	$res = $stock->NewStock($response['data']);
	    	if(!is_string($res['rowCount']) && $res['rowCount'] > 0) {
		        header('Content-Type: application/json');
		        $response['data']['stock_id'] = $res['lastId'];
				$response['code'] = 201;
				$response['message'] = 'New record inserted!';
				$response['status'] = 'success';
	        	print json_encode($response);
	    	}else{
	    		$response_errors['code'] = '406';
				$response_errors['status'] = 'error';
				$response_errors['message'] = $res;//'Not Acceptable';
		        header('HTTP/1.1 '.$response_errors['code'].' '.$response_errors['message']);
		        header('Content-Type: application/json; charset=UTF-8');

		        die(json_encode($response_errors));
	    	}

	    }
	else
    {
        $response_errors['code'] = '400';
		$response_errors['status'] = 'error';
		$response_errors['message'] = 'Bad Request';
        header('HTTP/1.1 '.$response_errors['code'].' '.$response_errors['message']);
        header('Content-Type: application/json; charset=UTF-8');

        die(json_encode($response_errors));
	}
?>