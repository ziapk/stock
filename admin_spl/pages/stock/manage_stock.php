<?php 
$added_by			= 	$_SESSION['user_name'];
//---------------------------------------------------- Updation ---------------------------------------------------------------//
?>

<h2 class="page-header"><img src="resources/images/icons/products.png" alt="icon"  width="40" style="margin-top: -5px; height:34px; "/>Add Stock</h2>
  

 

  <div class="alert alert-success hide" id="rec-updated">
  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  	<h4>Success!</h4>
  	<p class="alert-content"></p>
  </div>
  <div class="alert alert-success hide" id="rec-deleted">
  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  	<h4>Success!</h4>
  	<p class="alert-content"></p>
  </div>

	 <form action="" method="post" id="update_stock">
			<div class="form-group">
				<input type="text" placeholder="Name" id="customerAutocomplte" name="stock_name" class="ui-autocomplete-input form-control" autocomplete="off" />
				<input type="hidden" id="stock_id" name="stock_id" />
			</div>
			
			 <div class="form-group">
				<input type="text" placeholder="Purchasing Price" id="purchasing_price" name="purchasing_price" name="email" class="form-control">
			</div>
			
			<div class="form-group">
				<input type="text" placeholder="Selling Price" id="selling_price" name="selling_price" name="email" class="form-control">
			</div>
			
			<div class="form-group">
				<input type="text" placeholder="Quantity" id="quantity" name="quantity" class="form-control">
			</div>
			<div class="form-group">
				<input type="text" class="hide" name="added_by" value="<?php echo $added_by ?>">
				<input type="text" class="hide" name="updated_by" value="<?php echo $added_by ?>">
				<input type="submit" value="Update Stock" class="btn btn-primary">				
			</div>
		</form>
		<!-- <a class="btn btn-primary" data-toggle="modal" href='#nc_modal'>Trigger modal</a> -->
		<div class="modal fade" id="nc_modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">New Stock Item</h4>
					</div>
					<form id="new_stock" action="pages/stock/new_stock.php">
						<div class="modal-body">
							<div class="form-group">
								<label>Stock Name</label>
								<input type="text" class="form-control" id="ns_stock_name" placeholder="HP Bettry" name="ns_stock_name" required>
							</div>
							<div class="form-group">
								<label>Purchasing Price</label>
								<input type="text" class="form-control" id="ns_purchasing_price" placeholder="3000" name="ns_purchasing_price" required>
							</div>
							<div class="form-group">
								<label>Selling Price</label>
								<input type="text" class="form-control" id="ns_selling_price" placeholder="6000" name="ns_selling_price" required>
							</div>
							<div class="form-group">
								<label>Quantity</label>
								<input type="text" class="form-control" id="ns_quantity" placeholder="100" name="ns_quantity" required>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<input type="submit" class="btn btn-primary" value="Add">
						</div>
						<input class="hidden" type="reset" id="new_stock_reset">
						<input type="text" class="hide" name="added_by" id="added_by" value="<?php echo $added_by ?>">
						<input type="text" class="hide" name="updated_by" id="updated_by" value="<?php echo $added_by ?>">
					</form>
				</div>
			</div>
		</div>
		<div class="content-box"><!-- Start Content Box -->
			<div class="content-box-header">
				<h3>Sales </h3>
			</div> <!-- End .content-box-header -->
				<div class="content-box-content">
					<table id="example" class="display" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		                <th></th>
		                <th>Stock Name</th>
		                <th>Purchacing Price</th>
		                <th>Selling Price</th>
		                <th>Qty</th>
		                <th>Delete</th>
		                <th>Created At</th>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th></th>
		                <th>Stock Name</th>
		                <th>Purchacing Price</th>
		                <th>Selling Price</th>
		                <th>Qty</th>
		                <th>Delete</th>
		                <th>Created At</th>
		            </tr>
		        </tfoot>
		    </table>
			</div>
	  </div>
	<script type="text/javascript">
		
		function OnDelSubmit (element) {
			var del=confirm("Are you sure you want to delete this record?");
				if (del==true){
					var url = "pages/stock/api_del_stock.php";
					var raw = $(element).serialize();
					var params = raw+"&added_by="+"<?php echo $added_by; ?>";
				  	$.ajax ({
							type: "POST",
							url: url,
							data: params,
							processData: false,
							async: false,
							success: function(result) {
								window.location.href=window.location.href;
							},
							error:function (error) {
								alert(error.statusText);
								return false;
							}
						});
					return false;
				}
				return del;
	    } 
		function format ( d ) {
		    return  '<table><thead><tr><th>Stock ID</th><th>Stock Name</th><th>Purchacing Price</th><th>Selling Price</th><th>Stock Qty</th><th> Added By</th></tr></thead>'+
		    		'<tbody><tr><td>'+d.stock_id +'</td><td>'+d.stock_name +'</td><td>'+d.purchasing_price +'</td><td>'+d.selling_price +'</td><td>'+d.quantity +'</td><td>'+d.added_by +'</td></tr></tbody></table>';
		}
	 
		$(document).ready(function() {
		    var dt = $('#example').DataTable( {
		        "processing": false,
		        "serverSide": false,
		        "ajax": "pages/stock/api_get_stock.php",
		        "columns": [
		            {
		                "class":          "details-control",
		                "orderable":      true,
		                "data":           null,
		                "defaultContent": ""
		            },
		            { "data": "stock_name" },
		            { "data": "purchasing_price" },
		            { "data": "selling_price" },
		            { "data": "quantity" },
		            { 
		            	"data": "customer_id",
		            	render : function(data, type, row) {
				              return '<form id="d_form" onsubmit="return OnDelSubmit(this);"><input type="hidden" name="stock_id" id="d_stock_id" value="'+data+'" /><button type="submit" class="btn btn-danger delete-row">Delete</button></form>'
				          } 
		            },
		            { "data": "date_added" },
		        ],
		        "columnDefs": [
		            {
		                "targets": [ -1 ],
		                "visible": false,
		                "searchable": false
		            },
		        ],
		        "order": [[6, 'desc']]
		    } );
		 
		    // Array to track the ids of the details displayed rows
		    var detailRows = [];

		    $('#example tbody').on( 'click', 'tr td.details-control', function () {
		        var tr = $(this).closest('tr');
		        var row = dt.row( tr );
		        var idx = $.inArray( tr.attr('id'), detailRows );
		 
		        if ( row.child.isShown() ) {
		            tr.removeClass( 'details' );
		            row.child.hide();
		 
		            // Remove from the 'open' array
		            detailRows.splice( idx, 1 );
		        }
		        else {
		            tr.addClass( 'details' );
		            row.child( format( row.data() ) ).show();
		 
		            // Add to the 'open' array
		            if ( idx === -1 ) {
		                detailRows.push( tr.attr('id') );
		            }
		        }
		    } );
		    
	    	
		 
		    // On each draw, loop over the `detailRows` array and show any child rows
		    dt.on( 'draw', function () {
		        $.each( detailRows, function ( i, id ) {
		            $('#'+id+' td.details-control').trigger( 'click' );
		        } );
		    } );
		    
		   

			$('#customerAutocomplte').autocomplete({
				source:'pages/stock/stock_purchasing_price.php', 
				minLength:1,
				autoFocus: true,
				response: function (event, ui) {
					if(ui.content.length == 0){
						var inputVal = $('#customerAutocomplte').val();
						$('#purchasing_price').val('');
						$('#selling_price').val('');
						$('#quantity').val('');
						$('#nc_modal').modal('show');
						$('#nc_modal').on('shown.bs.modal', function () {
							$(this).find('#ns_stock_name').focus().val(inputVal);
						});
					}
				},
				select: function( event, ui ) {
					if(ui.item.id){
						$('#stock_id').val(ui.item.id);
					}
					if(ui.item.purchasing_price){
						$('#purchasing_price').val(ui.item.purchasing_price);
					}
					if(ui.item.selling_price){
						$('#selling_price').val(ui.item.selling_price);
					}
					if(ui.item.quantity){
						$('#quantity').val(ui.item.quantity);
					}
				}
			});

			// custom validate email

			/*$.validator.addMethod("customemail", 
				function(value, element) {
				   return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
				}, 
				"Please enter a valid email address."
			);*/

			// form validate 
			$('#new_stock').validate({
				debug: false,
				errorClass: "text-danger",
				errorElement: "span",
				rules: {
					nc_stock_name: {
						required: true
					},
					ns_purchasing_price: {
						required: true,
						number: true
					},
					ns_selling_price: {
						required: true,
						number: true
					},
					ns_quantity: {
						required: true,
						number: true
					}
				},
				// default error placement
				errorPlacement: function (error, element) {
					$(element).parent().append(error);
				},
				// form on submit 
				submitHandler: function(form) {
					url = 'pages/stock/new_stock.php';
					var params = $(form).serialize();
					$.ajax ({
						type: "POST",
						url: url,
						data: params,
						processData: false,
						async: false,
						success: function(result) {
							$('#stock_id').val(result.data.stock_id);
							$('#customerAutocomplte').val(result.data.ns_stock_name);
							$('#purchasing_price').val(result.data.ns_purchasing_price);
							$('#selling_price').val(result.data.ns_selling_price);
							$('#quantity').val(result.data.ns_quantity);
							$('#nc_modal').modal('hide');
							$('#new_stock_reset').click();
							dt.ajax.reload();
						},
						error:function (error) {
							return false;
						}
					});
				}
			});




			// form validate 
			$('#update_stock').validate({
				debug: false,
				errorClass: "text-danger",
				errorElement: "span",
				rules: {
					stock_name: {
						required: true
					},
					purchasing_price: {
						required: true,
						number: true
					},
					selling_price: {
						required: true,
						number: true
					},
					quantity: {
						required: true,
						number: true
					}
				},
				// default error placement
				errorPlacement: function (error, element) {
					$(element).parent().append(error);
				},
				// form on submit 
				submitHandler: function(form) {
					url = 'pages/stock/update_stock.php';
					var params = $(form).serialize();
					$.ajax ({
						type: "POST",
						url: url,
						data: params,
						processData: false,
						async: false,
						success: function(result) {
							$('#rec-updated').removeClass('hide').find('.alert-content').html(result.message);
							dt.ajax.reload();
						},
						error:function (error) {
							return false;
						}
					});
				}
			});
			



		});
	</script>