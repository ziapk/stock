<?php 
	require_once("../../../common/stock.php");
	$stock = new Stock;
	$response = array();
	$response_errors = array();
	$everything_is_ok = true;
	if(isset($_REQUEST['stock_name']) && !empty(trim($_REQUEST['stock_name']))){
		$response['data']['added_by'] 	= $_REQUEST['added_by'];
		$response['data']['updated_by'] = $_REQUEST['updated_by'];
		$response['data']['stock_id'] 	= $_REQUEST['stock_id'];
		$response['data']['stock_name'] = $_REQUEST['stock_name'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['stock_name'] = 'Please check your item name';
	}

	if(isset($_REQUEST['purchasing_price']) && !empty(trim($_REQUEST['purchasing_price'])) && is_numeric($_REQUEST['purchasing_price']) ) {
		$response['data']['purchasing_price'] = $_REQUEST['purchasing_price'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['purchasing_price'] = 'Please check your purchasing price';
	}


	if(isset($_REQUEST['selling_price']) && !empty(trim($_REQUEST['selling_price'])) && is_numeric($_REQUEST['selling_price'])) {
		$response['data']['selling_price'] = $_REQUEST['selling_price'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['selling_price'] = 'Please select selling price';
	}

	if(isset($_REQUEST['quantity']) && !empty(trim($_REQUEST['quantity'])) && is_numeric($_REQUEST['quantity'])) {
		$response['data']['quantity'] = $_REQUEST['quantity'];
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['quantity'] = 'Please check your stock quantity';
	}

	if ($everything_is_ok)
	    {
	    	$res = $stock->UpdateStock($response['data']);
	    	if(!is_string($res) && $res > 0) {
		        header('Content-Type: application/json');
				$response['code'] = 201;
				$response['message'] = '<strong>Stock ID '.$response['data']['stock_id'].'</strong> has been updated!';
				$response['status'] = 'success';
	        	print json_encode($response);
	    	}else{
	    		$response_errors['code'] = '406';
				$response_errors['status'] = 'error';
				$response_errors['message'] = $res;//'Not Acceptable';
		        header('HTTP/1.1 '.$response_errors['code'].' '.$response_errors['message']);
		        header('Content-Type: application/json; charset=UTF-8');
		        die(json_encode($response_errors));
	    	}

	    }
	else
    {
        $response_errors['code'] = '400';
		$response_errors['status'] = 'error';
		$response_errors['message'] = 'Bad Request';
        header('HTTP/1.1 '.$response_errors['code'].' '.$response_errors['message']);
        header('Content-Type: application/json; charset=UTF-8');

        die(json_encode($response_errors));
	}
?>