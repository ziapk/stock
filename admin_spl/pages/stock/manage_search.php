<?php
require_once("../../../common/config.php");
function checkValues($value)
{
	 // Use this function on all those values where you want to check for both sql injection and cross site scripting
	 //Trim the value
	 $value = trim($value);
	 
	// Stripslashes
	if (get_magic_quotes_gpc()) {
		$value = stripslashes($value);
	}
	
	 // Convert all &lt;, &gt; etc. to normal html and then strip these
	 $value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
	
	 // Strip HTML Tags
	 $value = strip_tags($value);
	
	// Quote the value
	$value = mysql_real_escape_string($value);
	return $value;
	
}		
 
$rec = checkValues($_REQUEST['val']);

if(!$rec){
	exit;
}
if($rec)
{

	$res = $db->select_array(array('*'), PREFIX."stock_items","Stock_Name like '%$rec%' OR Purchasing_Price like '%$rec%' OR Selling_Price like '%$rec%' OR Quantity like '%$rec%'");
}

//echo"<pre>"; print_r($res); exit;
$total =  $db->countfields(array('*'), PREFIX."stock_items","Stock_Name like '%$rec%' OR Purchasing_Price like '%$rec%' OR Selling_Price like '%$rec%' OR Quantity like '%$rec%'");

?>



<table id="example" class="dataTable" aria-describedby="example_info" style="width: 95.4%;">
       <tbody role="alert" aria-live="polite" aria-relevant="all">
  
  
     <tr role="row"  >  <th  style="width: 14%;" width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Group Name: activate to sort column ascending"></th><th style="width: 16%;" width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending"></th> <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending" style="width: 14%;">Stock Name</th> <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending"> Purchasing Price </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Selling Price </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending"> Quantity </th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
 
<?php
for($row=1; $row<=count($res); $row++)

{
	?>
	<div class="each_rec">
   			 
             	<tr class="odd" style="background:none; ">
                         
                           <td  >
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $res[$row]['Stock_ID'];  ?>">
                            
                                <img src="resources/images/icons/add.png" style="width: 10px;">
                           </a>
                              <a href="#" class="button" style="background: orange   none repeat scroll 0 0 !important;">Sales Detail 
                              
                             <!-- <i class="badge">
							  <?php 
									 $count_stock = $db->countfields(array("*"),PREFIX."sales_detail","Stock_ID='$Stock_ID'");
							   ?>
                               </i>-->
                              </a> 
                            
                            </td>
                            
                             <td>
                             <a href="#" class="button"  style="background: green   none repeat scroll 0 0 !important;">Purchases Detail 
                            <!-- <i class="badge">
                               <?php 
									 $count_stock = $db->countfields(array("*"),PREFIX."purchases_detail","Stock_ID='$Stock_ID'");
							   ?>
                              </i>-->
                              </a> 
                            
                            </td>
                          
                           <td>
                             <?php 
                            		echo $res[$row]['Stock_Name'];
                            ?>
                            </td>
                          <td>
                             <?php 
                           echo $res[$row]['Purchasing_Price'];
                            ?>
                            </td>
                            
                              <td>
                             <?php 
                          echo $res[$row]['Selling_Price'];
                            ?>
                            </td>
                            
                            
                              <td>
                             <?php 
                          echo $res[$row]['Quantity'];
                            ?>
                            </td>
                            
                              
                            
                             
                         
                          
                         
						   <td class=" sorting_1">
                           <a title="Delete it ?" href="index.php?action=del_stockitem&stock_id=<?php    echo $res[$row]['Stock_ID']; ?>"><img width="20" alt="Delete it ?" src="resources/images/icons/cross.png" class="Employee" id=""  onclick="return confirm('Are you sure you want to delete?');"></a>
                              
                            </td>
                         
                           
    </tr>
    	 
    </div>
<?php
}

if($total==0){
	?> 
    <tr class="odd" colspan="7">
     <td>
      <div class="no-rec">No Record Found !</div>
      </td>
    <?php } ?>
</tbody>
</table>