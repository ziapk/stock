<?php 
 

// how many records should be displayed on a page?
$res 				= $db->selectSRow(array('rows'),PREFIX."pagination_rows");
$records_per_page 	= $res['rows'];
?>
 
<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<!--------------For ajax base searching------------------->
<script language="javascript">

$(document).ready(function(){
$(".collapse").collapse();
$('#accordion').collapse({hide: true})
	//show loading bar

	function showLoader(){

		$('.search-background').fadeIn(200);

	}

	//hide loading bar

	function hideLoader(){

		$('#sub_cont').fadeIn(1500);

		$('.search-background').fadeOut(200);

	};

	$('#search').keyup(function(e) {
		 if(e.keyCode == 13) {

 		showLoader();

		$('#sub_cont').fadeIn(1500);

		$("#content #sub_cont").load("pages/outstandings/manage_search_payments.php?val=" + $("#search").val(), hideLoader());



      }

  

      });

	  

	$(".searchBtn").click(function(){

	

		//show the loading bar

		showLoader();

		$('#sub_cont').fadeIn(1500);

		  

		$("#content #sub_cont").load("pages/outstandings/manage_search_payments.php?val=" + $("#search").val(), hideLoader());



	});

});

 
</script>     	
<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>



<h2><img src="resources/images/icons/reports.png" alt="icon"  width="40"/>Payment Transactions </h2>

 <?php if(isset($_SESSION["add_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["add_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["add_message"]); } ?>

 <?php if(isset($_SESSION["edit_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["edit_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["edit_message"]);  } ?>

 <?php if(isset($_SESSION["dell_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["dell_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["dell_message"]);  } ?>
 

 



        <div class="content-box"><!-- Start Content Box -->
                            
                           <div class="content-box-header">
                                
                                <h3 style="cursor: s-resize;">Payment Transactions Reports </h3>
                                
                           </div> <!-- End .content-box-header -->
                    <div style="min-height:480px;" class="content-box-content">
            <div id="tab1" class="tab-content default-tab" style="display: block;"> <!-- This is the target div. id must match the href of this div's tab -->
               
                    <label>Show 
                    <select name="rows"  id="rows" size="1" aria-controls="example" onchange="select_rows(this.value)" >						
                    				<option value="10" <?PHP if($records_per_page == 10) echo"selected";?>>10</option>
                                    <option value="25" <?PHP if($records_per_page == 25) echo"selected";?>>25</option>
                                    <option value="50" <?PHP if($records_per_page == 50) echo"selected";?>>50</option>
                                    <option value="100" <?PHP if($records_per_page == 100) echo"selected";?>>100</option>
                    </select> entries</label>
                     
                     <div class="dataTables_filter" id="example_filter" style="float: right;"><label>Search: <input type="text" aria-controls="example" value="" name="searchBox" id="search"></label></div>
                     
         
                  <div class="searchBtn">
					&nbsp;
       			</div>   

    
        
	
<br clear="all" />
<div id="content">
	<div id="sub_cont" class="search-record">	</div>
</div>               
<div role="grid" class="dataTables_wrapper" id="example_wrapper">
<table id="example" class="dataTable" aria-describedby="example_info">
<thead>
     <tr role="row"  > <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending">Ref ID</th><th  class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending" width="170"  >Type </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Customer </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending">Supplier </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Sub Total </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending"> Payment  </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Balance </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Due Date </th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Transaction Date</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Added By</th></tr>
</thead>

                
<!--Pagination Code put everywhere u need it-->
<?php
//Fetching all records from db
$results_objects = $db->select_orderby(PREFIX."payment_transactions","","Payment_ID DESC");
$Payment_ID_array = array();
foreach ($results_objects as $key => $value) {
 	$Payment_ID_array[] = $value->Payment_ID;
}
 


// the number of total records is the number of records in the array
$pagination->records(count($Payment_ID_array));
// records per page
$pagination->records_per_page($records_per_page);
// here's the magick: we need to display *only* the records for the current page
$Payment_ID_array = array_slice(
    $Payment_ID_array,
    (($pagination->get_page() - 1) * $records_per_page),
    $records_per_page
);

?>  
               <tfoot>
                      </tfoot>  
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php  foreach ($Payment_ID_array as $index => $Payment_ID):  ?>
                          
                          <tr class="odd">
                            <td>
                            <?php 
                            $Ref_ID 	= $db->selectSRow(array("Ref_ID"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Ref_ID['Ref_ID'];
                            ?>
                            </td>
                         
                         	  <td>
                            <?php 
                            $Type 	= $db->selectSRow(array("Type"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Type['Type'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $Customer 	= $db->selectSRow(array("Customer"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Customer['Customer'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $Supplier 	= $db->selectSRow(array("Supplier"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Supplier['Supplier'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $Sub_Total 	= $db->selectSRow(array("Sub_Total"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Sub_Total['Sub_Total'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $Payment 	= $db->selectSRow(array("Payment"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Payment['Payment'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $Balance 	= $db->selectSRow(array("Balance"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Balance['Balance'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $Due_Date 	= $db->selectSRow(array("Due_Date"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Due_Date['Due_Date'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $Date_Transaction 	= $db->selectSRow(array("Date_Transaction"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Date_Transaction['Date_Transaction'];
                            ?>
                            </td>
                            
                              
                            <td> 
                           <?php 
                            $Added_By 	= $db->selectSRow(array("Added_By"),PREFIX."payment_transactions","Payment_ID='$Payment_ID'");
                            echo $Added_By['Added_By'];
                            ?>
                            </td>
                           
                    </tr>
                     	
                     
                   
                   
                    <?php endforeach?>
                            
                             
                            
                            </tbody></table>
                            
<?php
// render the pagination links
$pagination->render();

?>
                            </div>
</div> <!-- End #tab1 -->
                                
              </div>
            </div>     
            
              