<?php
require_once("../../../common/config.php");
function checkValues($value)
{
	 // Use this function on all those values where you want to check for both sql injection and cross site scripting
	 //Trim the value
	 $value = trim($value);
	 
	// Stripslashes
	if (get_magic_quotes_gpc()) {
		$value = stripslashes($value);
	}
	
	 // Convert all &lt;, &gt; etc. to normal html and then strip these
	 $value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
	
	 // Strip HTML Tags
	 $value = strip_tags($value);
	
	// Quote the value
	$value = mysql_real_escape_string($value);
	return $value;
	
}		
 
$rec = checkValues($_REQUEST['val']);

if(!$rec){
	exit;
}
if($rec)
{

	$res = $db->select_array(array('*'), PREFIX."payment_transactions","Ref_ID like '%$rec%' OR Customer like '%$rec%' OR Sub_Total like '%$rec%' OR Payment like '%$rec%' OR Balance like '%$rec%' OR Date_Transaction like '%$rec%'");
}

//echo"<pre>"; print_r($res); exit;
$total =  $db->countfields(array('*'), PREFIX."payment_transactions","Ref_ID like '%$rec%' OR Customer like '%$rec%' OR Sub_Total like '%$rec%' OR Payment like '%$rec%' OR Balance like '%$rec%' OR Date_Transaction like '%$rec%'");

?>



<table id="example" class="dataTable" aria-describedby="example_info" style="width: 95.4%;">
       <tbody role="alert" aria-live="polite" aria-relevant="all">
        <tr role="row"  > <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending">Ref ID</th><th  class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending" width="170"  >Type </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Customer </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending">Supplier </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Sub Total </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending"> Payment  </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Balance </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Due Date </th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Transaction Date</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Added By</th></tr>

<?php
for($row=1; $row<=count($res); $row++)

{?>
	<div class="each_rec">
   			 
             	<tr class="odd" style="background-color:none;">
                            <td>
                            <?php 
                            echo $res[$row]['Ref_ID'];
                            ?>
                            </td>
                         
                         	  <td>
                            <?php 
                            echo $res[$row]['Type'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            echo $res[$row]['Customer'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            echo $res[$row]['Supplier'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            echo $res[$row]['Sub_Total'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            echo $res[$row]['Payment'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            echo $res[$row]['Balance'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            echo $res[$row]['Due_Date'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            echo $res[$row]['Date_Transaction'];
                            ?>
                            </td>
                            
                              
                            <td> 
                           <?php 
                            echo $res[$row]['Added_By'];
                            ?>
                            </td>
                           
                    </tr>
    </div>
<?php
}

if($total==0){
	?> 
    <tr class="odd">
     <td>
      <div class="no-rec">No Record Found !</div>
      </td>
    <?php } ?>
</tbody>
</table>