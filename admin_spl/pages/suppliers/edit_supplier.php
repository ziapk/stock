<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		 extract($_POST);
		 
		 $date_updated		= $db->date_time('DT');
		 $updated_by		= $_SESSION['user_name'];
		 $upd_array 		= array("customer_name"=>$customer_name,"address"=>$address,"city"=>$city,"contact_person"=>$contact_person,"phone_number"=>$phone_number,"email"=>$email,"mobile_number"=>$mobile_number,"notes"=>$notes,"date_updated"=>$date_updated,"updated_by"=>$updated_by);
		 //echo"<pre>"; print_r( $upd_array ); exit;
		 
		 $upd = $db->updateCondition($upd_array,PREFIX."customers","customer_number='$customer_number'");
         $_SESSION["edit_message"] = "Customer detail have been updated successfully."; 
		 
		 ?>
		<script type="text/javascript">
		location.href = "index.php?action=manage_customers";
		</script>
		<?php 
}


//Fetching  customer detail from db
	$customer_id = isset($_GET['customer_id'])?$_GET['customer_id']:"";
	$all_rec=array("*");
	$res_sel		    = $db->selectSRow($all_rec,PREFIX."customers","customer_id=$customer_id");
	$customer_number	= $res_sel['customer_number'];
	$address		 	= $res_sel['address'];
	$customer_name 		= $res_sel['customer_name'];
	$address			= $res_sel['address'];
	$city 				= $res_sel['city'];
	$contact_person		= $res_sel['contact_person'];
	$phone_number		= $res_sel['phone_number'];
	$email				= $res_sel['email'];
	$mobile_number		= $res_sel['mobile_number'];
	$notes 				= $res_sel['notes'];
?>
 
<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#form_customer").validate({
			rules: {
				customer_name: "required",
				address: "required",
				city: "required",
				contact_person: "required",
				phone_number: "required",
				mobile_number: "required"
			},
			messages: {
				customer_name: "Please enter customer name",
				address: "Please enter customer address",
				city: "Please enter customer city",
				contact_person: "Please enter contact person",
				phone_number: "Please enter customer phone",
				mobile_number: "Please enter customer mobile",
 
			}
		});

	});
	</script>
 

<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>



<h2><img src="resources/images/icons/client.png" alt="icon"  width="40"/>Edit Customer</h2>
 

                         
<div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form action=""  name="form_customer" id="form_customer" method="post" enctype="multipart/form-data">
		<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
		
        
        <p>
		<label>Customer Name  <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="customer_name" name="customer_name" value="<?php echo  $customer_name; ?>"  />
      
		</p>
        
         <p>
		<label>Address  <span class="style1" style="color:#F00"> *</span>  </label> 
       <textarea class="text-input medium-input"  id="address" name="address"   ><?php echo  $address; ?></textarea> 
        </p>    
            	 <p>
		<label>City   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="city" name="city"  value="<?php echo  $city; ?>"  />
		</p>
        
              
               <p>
		<label>Contact Person   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="contact_person" name="contact_person" value="<?php echo  $contact_person; ?>"   />
		</p>
        
            <p>
		<label>Phone Number   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="phone_number" name="phone_number"  value="<?php echo  $phone_number; ?>"  />
		</p>
          <p>
		<label>Email</label> 
		<input class="text-input medium-input" type="text" id="email" name="email"  value="<?php echo  $email; ?>"  />
       
		</p>
            <p>
		<label>Mobile Number   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="mobile_number" name="mobile_number"  value="<?php echo  $mobile_number; ?>"  />
		</p>	
        
         <p>
		<label>Notes </label> 
		<input class="text-input medium-input" type="text" id="notes" name="notes" value="<?php echo  $notes; ?>"  />
      
		</p>
          
       <p>  
        <input   type="hidden" value="<?php echo $customer_number; ?>" name="customer_number" />
        <input class="button" type="submit" value="Submit" name="submit" />
        </p>
        </fieldset>
							
		 <div class="clear"></div><!-- End .clear -->
							
		 </form>
						
					</div>
             