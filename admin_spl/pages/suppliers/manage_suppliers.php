<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		 extract($_POST);
		 $supplier_number	= $db->autoincrement(PREFIX."suppliers", "Supplier_Number");
		 $date_added		= $db->date_time('DT');
		 $added_by			= $_SESSION['user_name'];
		 $data_array = array("Supplier_Number"=>$supplier_number,"Supplier_Name"=>$supplier_name,"Address"=>$address,"City"=>$city,"Contact_Person"=>$contact_person,"Phone_Number"=>$phone_number,"Email"=>$email,"Mobile_number"=>$mobile_number,"Notes"=>$notes,"Date_Added"=>$date_added,"Added_By"=>$added_by);
		 //echo"<pre>"; print_r($data_array); exit;
		 $db->insert($data_array,PREFIX."suppliers");
		 
		 $_SESSION["add_message"] = "Supplier detail have been added successfully.";
		 ?>
		<script type="text/javascript">
		location.href = "index.php?action=manage_suppliers";
		</script>
		<?php 
		exit();
		}


//Fetching  customer_number from db
$Supplier_Number = $db->autoincrement(PREFIX."suppliers", "Supplier_Number");


// how many records should be displayed on a page?
$res 				= $db->selectSRow(array('rows'),PREFIX."pagination_rows");
$records_per_page 	= $res['rows'];
?>
 
<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#form_supplier").validate({
			rules: {
				supplier_name: "required",
				address: "required",
				city: "required",
				contact_person: "required",
				phone_number: "required",
				mobile_number: "required"
			},
			messages: {
				supplier_name: "Please enter supplier name",
				address: "Please enter supplier address",
				city: "Please enter supplier city",
				contact_person: "Please enter contact person",
				phone_number: "Please enter supplier phone",
				mobile_number: "Please enter supplier mobile",
 
			}
		});

	});
	</script>
 
<!--------------For ajax base searching------------------->
<script language="javascript">

$(document).ready(function(){
$(".collapse").collapse();
$('#accordion').collapse({hide: true})
	//show loading bar

	function showLoader(){

		$('.search-background').fadeIn(200);

	}

	//hide loading bar

	function hideLoader(){

		$('#sub_cont').fadeIn(1500);

		$('.search-background').fadeOut(200);

	};

	$('#search').keyup(function(e) {
		 if(e.keyCode == 13) {

 		showLoader();

		$('#sub_cont').fadeIn(1500);

		$("#content #sub_cont").load("pages/suppliers/manage_search.php?val=" + $("#search").val(), hideLoader());



      }

  

      });

	  

	$(".searchBtn").click(function(){

	

		//show the loading bar

		showLoader();

		$('#sub_cont').fadeIn(1500);

		  

		$("#content #sub_cont").load("pages/suppliers/manage_search.php?val=" + $("#search").val(), hideLoader());



	});

});

 
</script>

 
 
<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>



<h2><img src="resources/images/icons/client.png" alt="icon"  width="40"/>Manage Suppliers</h2>
<?php if(isset($_SESSION["add_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["add_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["add_message"]); } ?>

 <?php if(isset($_SESSION["edit_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["edit_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["edit_message"]);  } ?>

 <?php if(isset($_SESSION["dell_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["dell_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["dell_message"]);  } ?>

                         
<div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form action=""  name="form_supplier" id="form_supplier" method="post" enctype="multipart/form-data">
		<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
		<p>
		<label>Supplier Number  <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" value="<?php echo  $Supplier_Number; ?>" readonly="readonly"  />
      
		</p>
        
        <p>
		<label>Supplier Name  <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="supplier_name" name="supplier_name"  />
      
		</p>
        
         <p>
		<label>Address  <span class="style1" style="color:#F00"> *</span>  </label> 
       <textarea class="text-input medium-input"  id="address" name="address"  ></textarea> 
        </p>    
            	 <p>
		<label>City   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="city" name="city"   />
		</p>
        
              
               <p>
		<label>Contact Person   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="contact_person" name="contact_person"   />
		</p>
        
            <p>
		<label>Phone Number   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="phone_number" name="phone_number"   />
		</p>
          <p>
		<label>Email</label> 
		<input class="text-input medium-input" type="text" id="email" name="email"   />
       
		</p>
            <p>
		<label>Mobile Number   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="mobile_number" name="mobile_number"   />
		</p>	
        
         <p>
		<label>Notes </label> 
		<input class="text-input medium-input" type="text" id="notes" name="notes"  />
      
		</p>
          
       <p>
        <input class="button" type="submit" value="Submit" name="submit" />
        </p>
        </fieldset>
							
		 <div class="clear"></div><!-- End .clear -->
							
		 </form>
						
					</div>
            <div class="content-box"><!-- Start Content Box -->
                            
                           <div class="content-box-header">
                                
                                <h3 style="cursor: s-resize;">Suppliers Listing</h3>
                                
                           </div> <!-- End .content-box-header -->
                    <div style="min-height:480px;" class="content-box-content">
            <div id="tab1" class="tab-content default-tab" style="display: block;"> <!-- This is the target div. id must match the href of this div's tab -->
               
                    <label>Show 
                    <select name="rows"  id="rows" size="1" aria-controls="example" onchange="select_rows(this.value)" >						
                    				<option value="10" <?PHP if($records_per_page == 10) echo"selected";?>>10</option>
                                    <option value="25" <?PHP if($records_per_page == 25) echo"selected";?>>25</option>
                                    <option value="50" <?PHP if($records_per_page == 50) echo"selected";?>>50</option>
                                    <option value="100" <?PHP if($records_per_page == 100) echo"selected";?>>100</option>
                    </select> entries</label>
                     
                     <div class="dataTables_filter" id="example_filter" style="float: right;"><label>Search: <input type="text" aria-controls="example" value="" name="searchBox" id="search"></label></div>
                     
         
                  <div class="searchBtn">
					&nbsp;
       			</div>   

    
        
	
<br clear="all" />
<div id="content">
	<div id="sub_cont" class="search-record">	</div>
</div>               
<div role="grid" class="dataTables_wrapper" id="example_wrapper">
<table id="example" class="dataTable" aria-describedby="example_info">
<thead>
     <tr role="row"  > <th class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending" style=" width: 13%;"></th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"   aria-label="Group Name: activate to sort column ascending">Supplier Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Supplier Name</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Contact Person</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Phone Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Mobile Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Balance</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Stock Available? </th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
</thead>

                
<!--Pagination Code put everywhere u need it-->
<?php
//Fetching all records from db
$results_objects = $db->select_orderby(PREFIX."suppliers","","Supplier_ID DESC");
$Supplier_ID_array = array();
foreach ($results_objects as $key => $value) {
 	$Supplier_ID_array[] = $value->Supplier_ID;
}
 


// the number of total records is the number of records in the array
$pagination->records(count($Supplier_ID_array));
// records per page
$pagination->records_per_page($records_per_page);
// here's the magick: we need to display *only* the records for the current page
$Supplier_ID_array = array_slice(
    $Supplier_ID_array,
    (($pagination->get_page() - 1) * $records_per_page),
    $records_per_page
);

?>  
               <tfoot>
                      </tfoot>  
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php  foreach ($Supplier_ID_array as $index => $Supplier_ID):  ?>
                          
                          <tr class="odd" >
                        	
							<?php $Supplier_Number 	= $db->selectSRow(array("Supplier_Number"),PREFIX."suppliers","Supplier_ID='$Supplier_ID'");
							$Supplier_Number = $Supplier_Number['Supplier_Number'];
                           ?>
                           <td>
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $Supplier_ID; ?>">
                                <img src="resources/images/icons/add.png" style="width: 10px;">
                           </a>
                              <a href="index.php?action=manage_purchases&Supplier_Number=<?php echo $Supplier_Number; ?>" class="button" >Purchase Now </a> 
                            </td>
                            <td style="11%">
                            <?php 
                             echo $Supplier_Number;
							?>
                            
						   </td>
                         
                             <td>
                           <?php $Supplier_Name 	= $db->selectSRow(array("Supplier_Name"),PREFIX."suppliers","Supplier_ID='$Supplier_ID'");
							echo $Supplier_Name = $Supplier_Name['Supplier_Name'];
							?>
                            </td>
                            
                             <td>
                           <?php $Contact_Person 	= $db->selectSRow(array("Contact_Person"),PREFIX."suppliers","Supplier_ID='$Supplier_ID'");
							echo $Contact_Person = $Contact_Person['Contact_Person'];
							?>
                            </td>
                            
                            <td>
                           <?php $Phone_Number 	= $db->selectSRow(array("Phone_Number"),PREFIX."suppliers","Supplier_ID='$Supplier_ID'");
							echo $Phone_Number = $Phone_Number['Phone_Number'];
							?>
                            </td>
                            <td>
                           <?php $Mobile_Number 	= $db->selectSRow(array("Mobile_Number"),PREFIX."suppliers","Supplier_ID='$Supplier_ID'");
							echo $Mobile_Number = $Mobile_Number['Mobile_Number'];
							?>
                            </td>
                            <td>
                           <?php $Balance 	= $db->selectSRow(array("Balance"),PREFIX."suppliers","Supplier_ID='$Supplier_ID'");
							echo $Balance = $Balance['Balance'];
							?>
                            </td>
                              <td>
                           <?php
						  
						    $counter = $db->countfields(array("*"),PREFIX."stock_items","Supplier_Number='$Supplier_Number'");
							if($counter == 0)
								echo"No";
							else
								echo"Yes";
							?>
                            </td>
                            
                           
                           <td class=" sorting_1">
                          
                            
                               <a title="Edit Detail" href="index.php?action=edit_supplier&Supplier_ID=<?php echo $Supplier_ID;  ?>"><img width="20" alt="View" src="resources/images/icons/pencil_48.png" class="Employee" id=""></a>
                               
                              
                            </td>
                    </tr>
                     	
                    <tr>
                          <td colspan="8">
                          <div class="panel panel-default collapsed">
    
                            <div id="collapse<?php echo $Supplier_ID; ?>" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <table id="example" class="dataTable" aria-describedby="example_info">
<thead>
     <tr   style="background: orange none repeat scroll 0 0; border: 1px solid black;" > <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Record Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Purchase Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Purchase Date</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Supplier ID</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Total Amount</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Payment</th> <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Balance </th></tr>
</thead>

						<tbody role="alert" aria-live="polite" aria-relevant="all">
						
						<?php 
								 
								$purchases_array = $db->select_array(array("*"),PREFIX."purchases","Supplier_ID='$Supplier_Number'","","Purchase_ID DESC");
								//echo"<pre>"; print_r($purchases_array); 
								for($loop=1; $loop<=count($purchases_array); $loop++ ){
								?>
								
                          <tr   style="border:none; ">
                        
                            <td style="11%">
                            <?php 
                            echo $purchases_array[$loop]['Purchase_ID'];
							?>
                           </td>
                           <td style="11%">
                            <?php 
                            echo $purchases_array[$loop]['Purchase_Number'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $purchases_array[$loop]['Purchase_Date'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $purchases_array[$loop]['Supplier_ID'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $purchases_array[$loop]['Total_Amount'];
							?>
                           </td>
                           
                          <td style="11%">
                            <?php 
                            echo $purchases_array[$loop]['Total_Payment'];
							?> 
                            </td>
                          
                           <td style="11%">
                            <?php 
                            echo $purchases_array[$loop]['Total_Balance'];
							?> 
                            </td>
                          
                            
                            
                           </tr>
                           </tbody>
                          
                           
						  <?php } ?>
                            </table> 
                              </div>
                            </div>
                          </div>
                          </td>
                   </tr>
                   
                   
                    <?php endforeach?>
                            
                             
                            
                            </tbody></table>
                            
<?php
// render the pagination links
$pagination->render();

?>
                            </div>
</div> <!-- End #tab1 -->
                                
              </div>
            </div>
            
              