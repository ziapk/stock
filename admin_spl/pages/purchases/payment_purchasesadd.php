<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		 extract($_POST);
		 //Fetching  sales_number from db
		 $Ref_ID 			= 	$db->autoincrement(PREFIX."payment_transactions", "Ref_ID");
		 $sub_total 		= 	isset($_POST['sub_total'])?$_POST['sub_total']:"";
		 $payment 			= 	isset($_POST['payment'])?$_POST['payment']:"";
		 $balance			=	$sub_total - $payment;
		 $date_added		= 	$db->date_time('DT');
		 $added_by			= 	$_SESSION['user_name'];
		 
		  if($payment > $sub_total){
			?>
            <script type="text/javascript">
				alert("Sorry, payment can't be greater then total due amount");
				location.href = "index.php?action=manage_purchases";
			</script>
            	 
		 <?php 
		 	exit();
		 }
		else{ 
			 //data array for sales table
			 $data_array = array("Ref_ID"=>$Ref_ID,"Type"=>"purchase","Customer"=>"","Supplier"=>$supplier_number,"Sub_Total"=>$sub_total,"Payment"=>$payment,"Balance"=>$balance,"Due_Date"=>$due_date,"Date_Transaction"=>$transaction_date,"Date_Added"=>$date_added,"Added_By"=>$added_by);
			$res	=	$db->insert($data_array,PREFIX."payment_transactions");
			//echo"<pre>"; print_r($data_array); exit;
			if($res){
				
				$existance_payment	= $db->selectSRow(array("Total_Payment"),PREFIX."purchases","Purchase_Number='$purchase_number'");
				$existance_payment	= $existance_payment['Total_Payment'];
				$remaining_payment	= $payment + $existance_payment;
				
				$res = $upd = $db->updateCondition(array('Total_Payment'=>$remaining_payment,'Total_Balance'=>$balance),PREFIX."purchases","Purchase_Number='$purchase_number'");
			}
			if($res)
			$upd = $db->updateCondition(array('Balance'=>$balance),PREFIX."suppliers","Supplier_Number='$supplier_number'");
			
			
			
			$_SESSION["add_message"] = "Payment detail have been added successfully.";
			 ?>
			<script type="text/javascript">
			location.href = "index.php?action=manage_purchases";
			</script>
			<?php 
			exit();
		}
}

 

//GET sales_number from href
$purchase_number = isset($_GET['Purchase_Number'])?$_GET['Purchase_Number']:"";

//GET sales_number from href
$Supplier_ID = isset($_GET['Supplier_ID'])?$_GET['Supplier_ID']:"";

//CAlculating sub total
$sub_total	= $db->selectSRow(array("Balance"),PREFIX."suppliers","Supplier_Number='$Supplier_ID'");
$sub_total	= $sub_total['Balance'];
							
?>
 
<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#add_purchases_form").validate({
			rules: {
				purchase_number: "required",
 				supplier_number: "required",
				sub_total: {
					    required: true,
     			 		number: true
				},
				payment: {
					    required: true,
     			 		number: true
				},
				due_date: "required",
				transaction_date: "required",
			},
			messages: {
				purchase_number: "Please enter purchase number",
  				supplier_number: "Please enter supplier number",
				sub_total: "Please enter sub total",
				payment: "Please enter payment",
				due_date: "Please enter due date",
				transaction_date: "Please enter transaction date",
				
			}
		});

	});



//Datepicker for add sale
$(function () {
    $('#due_date').datetimepicker();
});
//Datepicker for add sale
$(function () {
    $('#transaction_date').datetimepicker();
});

//Auto select change by selecting supplier
$(document).ready(function()
{
 $(".supplier_number").change(function()
 {
  var id=$(this).val();
  var dataString = 'id='+ id;
 
   $.ajax({
        type: "POST",
        async: false,
        url: "pages/purchases/find_supplier_subtotal.php",
       	data:{id:id},
        error: function(result) {
		  alert(result);
		},
        success: function (result) {
			$('#sub_total').val(result);
			 
        }
    });
	
	
  });
 
 
});

</script>





<h2><img src="resources/images/icons/purchase.png" alt="icon"  width="50"/>Add Payment </h2>

 
 

 



       
<div id="tab1" class="tab-content default-tab" style="display: block;">
		<form action="" enctype="multipart/form-data" name="add_purchases_form" id="add_purchases_form" method="post" >
		<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
		<p>
		<label>Ref ID  <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="purchase_number" name="purchase_number" value="<?php echo $purchase_number; ?>" readonly="readonly"  />
      
		</p>
        
       
        
       
         <p>
		<label>Supplier Name <span class="style1" style="color:#F00"> *</span></label> 
		<select   id="supplier_number"  name="supplier_number"  class="supplier_number form-control"  style="width: 51.4%;" >
             
                <?php  
				if($Supplier_ID)              
         		$suppliers 	= $db->select(array('*'),PREFIX."suppliers","Supplier_Number='$Supplier_ID'");
                else{
					$suppliers 	= $db->select(array('*'),PREFIX."suppliers");
					echo"<option value=''>Select supplier</option>";
				}
                 foreach($suppliers as $index => $supplier):  ?>  
                <option value="<?php echo $supplier->Supplier_Number; ?>"><?php echo $supplier->Supplier_Name; ?></option>
               <?php  endforeach; ?>
        </select>
		</p>
		         
        
        
      
		</p>
        
        <p>
		<label>Sub Total <span class="style1" style="color:#F00"> *</span></label> 
		<input class="sub_total text-input medium-input" type="text" id="sub_total" name="sub_total" value="<?php if(isset($sub_total)) echo $sub_total; ?>"  readonly="readonly"  />
      
		</p> 
        
        <p>
		<label> Payment  <span class="style1" style="color:#F00"> *</span></label> 
		<input class="text-input medium-input" type="text" id="payment" name="payment"  />
      
		</p> 
        
       <p>
		<label>Due Date  <span class="style1" style="color:#F00"> *</span> </label> 
		 
        <div class='input-group date' id='sales_date' style="width: 50%;">
                    <input type='text' class="form-control"  id="due_date" name="due_date"/>
        </div>
       </p>
       
       <p>
		<label>Transaction Date  <span class="style1" style="color:#F00"> *</span> </label> 
		 
        <div class='input-group date' id='sales_date' style="width: 50%;">
                    <input type='text' class="form-control"  id="transaction_date" name="transaction_date"/>
        </div>
       </p>
       
       
         
       
           
		 
              
        <p>
              <input class="button" type="submit" value="Submit" name="submit" />
        </p>
        </fieldset>
							
		 <div class="clear"></div><!-- End .clear -->
							
		 </form>
					
     	</div>
              
            
              