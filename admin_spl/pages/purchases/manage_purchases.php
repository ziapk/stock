<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		
		 extract($_POST);
  		 $date_added		= 	$db->date_time('DT');
		 $added_by			= 	$_SESSION['user_name'];
		 
		 
		 $purchasing_quantity		=	isset($_POST['purchasing_quantity'])?$_POST['purchasing_quantity']:1;
		 $purchasing_price			=	isset($_POST['purchasing_price'])?$_POST['purchasing_price']:"";
		 $purchasing_total_amount	=	$purchasing_quantity*$purchasing_price;

		 $total_payment				=	isset($_POST['total_payment'])?$_POST['total_payment']:0;
		 $total_balance 			=	$purchasing_total_amount - $total_payment;
		
		// how many stock already exist
		$stock_array		= $db->selectSRow(array('Quantity'),PREFIX."stock_items","Stock_Number='$stock_item'");
		$total_stock_quantity		= $stock_array['Quantity'] + $purchasing_quantity;
		 
		 if($total_payment > $purchasing_total_amount){
			?>
            <script type="text/javascript">
				alert("Sorry, payment amount can't be greater then total purchasing amount");
				location.href = "index.php?action=manage_purchases";
			</script>
            	 
		 <?php 
		 	exit();
		 }
		 
		  if($purchasing_total_amount != $total_payment + $total_balance){
			?>
            <script type="text/javascript">
				alert("Sorry, total purchasing amount is not equal to total payment + total balance");
				location.href = "index.php?action=manage_purchases";
			</script>
            	 
		 <?php
         	exit();
          }
		 else{
			
		 //data array for sales table
		 $data_array = array("Purchase_Number"=>$purchase_number,"Purchase_Date"=>$purchase_date,"Supplier_ID"=>$supplier_id,"Notes"=>$notes,"Total_Amount"=>$purchasing_total_amount,"total_payment"=>$total_payment,"total_balance"=>$total_balance,"Date_Added"=>$date_added,"Added_By"=>$added_by);
		 
		 $response = $db->insert($data_array,PREFIX."purchases");
		 if($response){
			 
		 		$total_balance=0;
				$res = $db->select_array(array('Total_Balance'),PREFIX."purchases","Supplier_ID='$supplier_id'");
				for($p=1; $p<=count($res); $p++){
					$total_balance = $total_balance + $res[$p]['Total_Balance'];
				}
				$upd = $db->updateCondition(array('Balance'=>$total_balance),PREFIX."suppliers","Supplier_Number='$supplier_id'");
		 
		 		//Updating stock table
				$upd = $db->updateCondition(array('Quantity'=>$total_stock_quantity),PREFIX."stock_items","Stock_Number='$stock_item'");
		 			
				//data array for sales_detail table
				$data_array = array("Purchase_Number"=>$purchase_number,"Supplier_Number"=>$supplier_id,"Stock_Item"=>$stock_item,"Purchasing_Quantity"=>$purchasing_quantity,"Purchasing_Price"=>$purchasing_price,"Selling_Price"=>$sales_price,"Purchasing_Total_Amount"=>$purchasing_total_amount);
				//echo"<pre>"; print_r($data_array); exit;
				$db->insert($data_array,PREFIX."purchases_detail");
				 
				 
				 
		 }
		 $_SESSION["add_message"] = "Purchases detail have been added successfully.";
		 ?>
		<script type="text/javascript">
		location.href = "index.php?action=manage_purchases";
		</script>
		<?php 
		exit();
		}
}

//Fetching  sales_number from db
$purchase_number = $db->autoincrement(PREFIX."purchases", "Purchase_Number");

//GET sales_number from href
$supplier_number = isset($_GET['Supplier_Number'])?$_GET['Supplier_Number']:"";

// how many records should be displayed on a page?
$res 				= $db->selectSRow(array('rows'),PREFIX."pagination_rows");
$records_per_page 	= $res['rows'];
?>
 
<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#add_purchases_form").validate({
			rules: {
				purchase_number_check: "required",
				purchase_date: "required",
 				supplier_id: "required",
				supplier_number: "required",
				stock_item: "required",
				sales_price: {
					    required: true,
     			 		number: true
				},
				purchasing_price: {
					    required: true,
     			 		number: true
				},
				purchasing_quantity:{
					    required: true,
     			 		number: true
				},
				total_payment:{
					    required: true,
     			 		number: true
				},
			},
			messages: {
				purchase_number_check: "Please enter purchase number",
				purchase_date: "Please enter purchasing date",
  				supplier_id: "Please enter supplier id",
				supplier_number: "Please select supplier name",
				stock_item: "Please enter stock item",
				sales_price: "Please enter sales price",
				purchasing_price: "Please enter purchasing price",
				purchasing_quantity: "Please enter purchasing quantity ",
				total_payment: "Please enter total payment",
			}
		});

	});



//Datepicker for add sale
$(function () {
    $('#purchase_date').datetimepicker();
});


//Auto select change by selecting supplier
$(document).ready(function()
{
 $(".supplier_number").change(function()
 {
  var id=$(this).val();
  var dataString = 'id='+ id;
 
  $.ajax
  ({
   type: "POST",
   url: "pages/purchases/find_stockitem_name.php",
   data: dataString,
   cache: false,
   success: function(data)
   {
	 $("#stock_item").append(data);
   } 
   });
  });
 
 
 $(".stock_item").change(function()
 {
  var id=$(this).val();
  var dataString = 'id='+ id;
  
  $.ajax
  ({
   type: "POST",
   url: "pages/purchases/stockitem_selling_price.php",
   data: dataString,
   cache: false,
   success: function(data)
   {	    
	  $('#sales_price').val(parseInt(data));
   } 
   });
   
    $.ajax
  ({
   type: "POST",
   url: "pages/purchases/stockitem_purchasing_price.php",
   data: dataString,
   cache: false,
   success: function(data)
   {	    
	  $('#purchasing_price').val(parseInt(data));
   } 
   });
   
   
  });
 
});
</script>

<!--------------For ajax base searching------------------->
<script language="javascript">

$(document).ready(function(){
$(".collapse").collapse();
$('#accordion').collapse({hide: true})
	//show loading bar

	function showLoader(){

		$('.search-background').fadeIn(200);

	}

	//hide loading bar

	function hideLoader(){

		$('#sub_cont').fadeIn(1500);

		$('.search-background').fadeOut(200);

	};

	$('#search').keyup(function(e) {
		 if(e.keyCode == 13) {

 		showLoader();

		$('#sub_cont').fadeIn(1500);

		$("#content #sub_cont").load("pages/purchases/manage_search.php?val=" + $("#search").val(), hideLoader());



      }

  

      });

	  

	$(".searchBtn").click(function(){

	

		//show the loading bar

		showLoader();

		$('#sub_cont').fadeIn(1500);

		  

		$("#content #sub_cont").load("pages/purchases/manage_search.php?val=" + $("#search").val(), hideLoader());



	});

});

 
</script>     	
<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>



<h2><img src="resources/images/icons/purchase.png" alt="icon"  width="50"/>Add Purchase </h2>

 <?php if(isset($_SESSION["add_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["add_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["add_message"]); } ?>

 <?php if(isset($_SESSION["edit_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["edit_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["edit_message"]);  } ?>

 <?php if(isset($_SESSION["dell_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["dell_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["dell_message"]);  } ?>
 

 



       
<div id="tab1" class="tab-content default-tab" style="display: block;">
		<form action="" enctype="multipart/form-data" name="add_purchases_form" id="add_purchases_form" method="post" >
		<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
		<p>
		<label>Purchase Number   <span class="style1" style="color:#F00"> *</span> </label> 
		<input class="text-input medium-input" type="text" id="purchase_number_check" name="purchase_number_check" value="<?php echo $purchase_number; ?>" readonly="readonly"  />
      
		</p>
        
        <p>
		<label>Purchase Date  <span class="style1" style="color:#F00"> *</span> </label> 
		 
        <div class='input-group date' id='sales_date' style="width: 50%;">
                    <input type='text' class="form-control"  id="purchase_date" name="purchase_date"/>
        </div>
       </p>
       
       
         <?php if($supplier_number) { ?>              
         <p>
		<label>Supplier ID  <span class="style1" style="color:#F00"> *</span>  </label> 
			<input class="text-input medium-input" type="text"  id="supplier_id"  name="supplier_id" value="<?php echo $supplier_number; ?>"  readonly="readonly"/>
          </p>
         <?php } 
		 else{  
		?>  
       
        <p>
		<label>Supplier ID  <span class="style1" style="color:#F00"> *</span></label> 
		<select   id="supplier_id"  name="supplier_id"  class="form-control"  style="width: 51.4%;" >
			<option value="" selected="">Please select</option>
			<?php  
			 $suppliers 	= $db->selectAll(PREFIX."suppliers");
			 foreach($suppliers as $index => $supplier): ?>  
            <option value="<?php echo $supplier->Supplier_Number; ?>"><?php echo $supplier->Supplier_Name; ?></option>
           <?php  endforeach; ?>
        </select>
		</p>
		<?php 
		} 
		?>         
        
      	
        <p>
		<label>Notes    </label> 
		<input class="text-input medium-input" type="text" id="notes" name="notes"   />
		</p>
        
              
           
		 
        
       
         <p>
		<label>Supplier Name <span class="style1" style="color:#F00"> *</span></label> 
		<select   id="supplier_number"  name="supplier_number"  class="supplier_number form-control"  style="width: 51.4%;" >
            <option value="" selected="">Please select</option>
                <?php  
				if($supplier_number)              
         		$suppliers 	= $db->select(array('*'),PREFIX."suppliers","Supplier_Number='$supplier_number'");
                else
				$suppliers 	= $db->select(array('*'),PREFIX."suppliers");
				
                 foreach($suppliers as $index => $supplier): ?>  
                <option value="<?php echo $supplier->Supplier_Number; ?>"><?php echo $supplier->Supplier_Name; ?></option>
               <?php  endforeach; ?>
        </select>
		</p>
		         
        
        
      
		</p>
        
        <p>
         <label>Stock Item <span class="style1" style="color:#F00"> *</span> </label>  
        <select class="stock_item text-input medium-input" type="text" id="stock_item" name="stock_item"  style="width: 51.6% !important;"  >
        <option value="">Select Stock Item</option>
        
        </select>
       </p>
        
        <p>
		<label>Purchasing Quantity  <span class="style1" style="color:#F00"> *</span></label> 
          <input class="text-input medium-input" type="text" id="purchasing_quantity" name="purchasing_quantity"  />
       
        
		</p> 
        
        
        <p>
		<label>Purchasing Price  <span class="style1" style="color:#F00"> *</span></label> 
		<input class="purchasing_price text-input medium-input" type="text" id="purchasing_price" name="purchasing_price"    />
		</p> 
        
        <p>
		<label>Selling Price  <span class="style1" style="color:#F00"> *</span></label> 
      	<input class="sales_price text-input medium-input" type="text" id="sales_price" name="sales_price"   />
		</p> 
       
         
       
         <p>
		<label>Total Payment  <span class="style1" style="color:#F00"> *</span></label> 
		<input class="text-input medium-input" type="text" id="total_payment" name="total_payment"  />
      
		</p> 
        
              
        <p>
        
        <input name="purchase_number" type="hidden" value="<?php echo $purchase_number; ?>" />
        <input class="button" type="submit" value="Submit" name="submit" />
        </p>
        </fieldset>
							
		 <div class="clear"></div><!-- End .clear -->
							
		 </form>
					
     	</div>
        <div class="content-box"><!-- Start Content Box -->
                            
                           <div class="content-box-header">
                                
                                <h3 style="cursor: s-resize;">Purchases </h3>
                                
                           </div> <!-- End .content-box-header -->
                    <div style="min-height:480px;" class="content-box-content">
            <div id="tab1" class="tab-content default-tab" style="display: block;"> <!-- This is the target div. id must match the href of this div's tab -->
               
                    <label>Show 
                    <select name="rows"  id="rows" size="1" aria-controls="example" onchange="select_rows(this.value)" >						
                    				<option value="10" <?PHP if($records_per_page == 10) echo"selected";?>>10</option>
                                    <option value="25" <?PHP if($records_per_page == 25) echo"selected";?>>25</option>
                                    <option value="50" <?PHP if($records_per_page == 50) echo"selected";?>>50</option>
                                    <option value="100" <?PHP if($records_per_page == 100) echo"selected";?>>100</option>
                    </select> entries</label>
                     
                     <div class="dataTables_filter" id="example_filter" style="float: right;"><label>Search: <input type="text" aria-controls="example" value="" name="searchBox" id="search"></label></div>
                     
         
                  <div class="searchBtn">
					&nbsp;
       			</div>   

    
        
	
<br clear="all" />
<div id="content">
	<div id="sub_cont" class="search-record">	</div>
</div>               
<div role="grid" class="dataTables_wrapper" id="example_wrapper">
<table id="example" class="dataTable" aria-describedby="example_info">
<thead>
     <tr role="row"  > <th class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Group Name: activate to sort column ascending"></th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Group Name: activate to sort column ascending">Purchase Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Purchase Date </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Supplier ID </th>  <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Amount </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Payment </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Balance </th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
</thead>

                
<!--Pagination Code put everywhere u need it-->
<?php
//Fetching all records from db
$results_objects = $db->select_orderby(PREFIX."purchases","","Purchase_ID DESC");
$Purchase_ID_array = array();
foreach ($results_objects as $key => $value) {
 	$Purchase_ID_array[] = $value->Purchase_Number;
}
 


// the number of total records is the number of records in the array
$pagination->records(count($Purchase_ID_array));
// records per page
$pagination->records_per_page($records_per_page);
// here's the magick: we need to display *only* the records for the current page
$Purchase_ID_array = array_slice(
    $Purchase_ID_array,
    (($pagination->get_page() - 1) * $records_per_page),
    $records_per_page
);

?>  
               <tfoot>
                      </tfoot>  
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php  foreach ($Purchase_ID_array as $index => $Purchase_Number):  ?>
                          
                          <tr class="odd" >
                        	
							<?php 
							
							$table_date	= $db->selectSRow(array("*"),PREFIX."purchases","Purchase_Number='$Purchase_Number'");
							//echo"<pre>"; print_r($table_date); exit;
                            $Purchase_ID 	=  $table_date['Purchase_ID'];
							$total_balance	=  $table_date['Total_Balance'];
							$Supplier_ID 	=  $table_date['Supplier_ID'];
							
							
                           ?>
                           <td style="width: 12%">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $Purchase_ID; ?>">
                                <img src="resources/images/icons/add.png" style="width: 10px;">
                           </a>
                           <?php if($total_balance > 0) {?>
                              <a href="index.php?action=payment_purchasesadd&Supplier_ID=<?php echo $Supplier_ID; ?>&Purchase_Number=<?php echo $Purchase_Number; ?>" class="button" >Pay Now </a> 
                            <?php } ?>
                            </td>
                            
                             <td>
                             <?php 
                           		echo $Purchase_Number;
								?>
                            </td>
                            
                            
                           <td>
                             <?php 
                           $Purchase_Date	= $db->selectSRow(array("Purchase_Date"),PREFIX."purchases","Purchase_Number='$Purchase_Number'");
                            echo $Purchase_Date 	=  $Purchase_Date['Purchase_Date'];
							
                            ?>
                            </td>
                            
                             <td>
                             <?php 
                           $Supplier_ID	= $db->selectSRow(array("Supplier_ID"),PREFIX."purchases","Purchase_Number='$Purchase_Number'");
                            echo $Supplier_ID 	=  $Supplier_ID['Supplier_ID'];
							
                            ?>
                            </td>
                            
                            
                         <td>
                             <?php 
                           $Total_Amount	= $db->selectSRow(array("Total_Amount"),PREFIX."purchases","Purchase_Number='$Purchase_Number'");
                            echo $Total_Amount['Total_Amount'];
                            ?>
                            </td>
                            
                          <td>
                             <?php 
                           $Total_Payment	= $db->selectSRow(array("Total_Payment"),PREFIX."purchases","Purchase_Number='$Purchase_Number'");
                            echo $Total_Payment['Total_Payment'];
                            ?>
                            </td>
                            
                            
                           
                             <td>
                             <?php 
                           $Total_Balance	= $db->selectSRow(array("Total_Balance"),PREFIX."purchases","Purchase_Number='$Purchase_Number'");
                            echo $Total_Balance['Total_Balance'];
                            ?>
                            </td>
                            
                           
                          
                            
                           
                           
                           
                           <td class=" sorting_1">
                            <a title="View Sale" href="<?php /*?>index.php?action=view_purchase&Purchase_Number=<?php echo $Purchase_Number;  ?><?php */?>"><img width="20" alt="View" src="resources/images/icons/view_icon.png" class="Employee" id=""></a>
                            
                               <a title="Edit Sale" href="<?php /*?>index.php?action=edit_purchase&Purchase_Number=<?php echo $Purchase_Number;  ?><?php */?>"><img width="20" alt="Edit" src="resources/images/icons/pencil_48.png" class="Employee" id=""></a>
                               
                              
                            </td>
                    </tr>
                     	
                    <tr >
                          <td colspan="8">
                          <div class="panel panel-default collapsed">
    
                            <div id="collapse<?php echo $Purchase_ID; ?>" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <table id="example" class="dataTable" aria-describedby="example_info">
<thead>
     <tr role="row"  style="background: orange none repeat scroll 0 0; border: 1px solid black;" > <th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Record Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Supplier Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Stock Item </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Purchasing Quantity </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Purchasing Price </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Selling Price </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Purchasing Total Amount  </th></tr>
</thead>

						<tbody role="alert" aria-live="polite" aria-relevant="all">
						
						<?php 
							 	 
						$purchase_array = $db->select_array(array("*"),PREFIX."purchases_detail","Purchase_Number='$Purchase_Number'","","Purchase_ID DESC");
						//echo"<pre>"; print_r($sales_array); exit;
						for($loop=1; $loop<=count($purchase_array); $loop++ ){
						?>
								
                          <tr class="odd" style="border:none; ">
                          <td style="11%">
                            <?php 
                            echo $purchase_array[$loop]['Purchase_ID'];
							?>
                           </td>
                           
                          
                           <td style="11%">
                            <?php 
                            echo $purchase_array[$loop]['Supplier_Number'];
							?>
                           </td>
                           
                            <td style="11%">
                            <?php 
                            echo $purchase_array[$loop]['Stock_Item'];
							?>
                           </td>
                           
                            <td style="11%">
                            <?php 
                            echo $purchase_array[$loop]['Purchasing_Quantity'];
							?>
                           </td>
                            <td style="11%">
                            <?php 
                            echo $purchase_array[$loop]['Purchasing_Price'];
							?>
                           </td>
                           
                            <td style="11%">
                            <?php 
                            echo $purchase_array[$loop]['Selling_Price'];
							?>
                           </td>
                           
                            <td style="11%">
                            <?php 
                            echo $purchase_array[$loop]['Purchasing_Total_Amount'];
							?>
                           </td>
                          </tr>
                           </tbody>
                          
                           
						  <?php } ?>
                            </table> 
                              </div>
                            </div>
                          </div>
                          </td>
                   </tr>
                   
                   
                    <?php endforeach?>
                            
                             
                            
                            </tbody></table>
                            
<?php
// render the pagination links
$pagination->render();

?>
                            </div>
</div> <!-- End #tab1 -->
                                
              </div>
            </div>     
            
              