<?php 
	require_once("../../../common/customers.php");
	$customers = new Customers;
	$response = array();
	$response_errors = array();
	$everything_is_ok = true;
	$customer_id = isset($_REQUEST['e_customer_id']) ? trim($_REQUEST['e_customer_id']) : '';
	$customer_number = isset($_REQUEST['e_customer_number']) ? trim($_REQUEST['e_customer_number']) : '';
	$city = isset($_REQUEST['e_city']) ? trim($_REQUEST['e_city']) : '';
	$contact_person = isset($_REQUEST['e_contact_person']) ? trim($_REQUEST['e_contact_person']) : '';
	$phone_number = isset($_REQUEST['e_phone_number']) ? trim($_REQUEST['e_phone_number']) : '';
	$mobile_number = isset($_REQUEST['e_mobile_number']) ? trim($_REQUEST['e_mobile_number']) : '';
	$email = isset($_REQUEST['e_email']) ? trim($_REQUEST['e_email']) : '';
	$notes = isset($_REQUEST['e_notes']) ? trim($_REQUEST['e_notes']) : '';
	$address = isset($_REQUEST['e_address']) ? trim($_REQUEST['e_address']) : '';

	if(!empty($customer_number)){
		$response['data']['customer_id'] = $customer_id;
		$response['data']['added_by'] = $_REQUEST['e_added_by'];
		$response['data']['updated_by'] = $_REQUEST['e_updated_by'];
		$response['data']['customer_number'] = $customer_number;

	}else{
		$everything_is_ok = false;
		$response_errors['errors']['customer_number'] = 'Please check your customer number';
	}

	if($city && !empty($city)) {
		$response['data']['city'] = $city;

	}else{
		$everything_is_ok = false;
		$response_errors['errors']['city'] = 'Please check your city';
	}


	if($contact_person && !empty($contact_person)) {
		$response['data']['customer_name'] = $contact_person;
		$response['data']['contact_person'] = $contact_person;
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['contact_person'] = 'Please select contact person';
	}

	if($phone_number && !empty($phone_number)) {
		$response['data']['phone_number'] = $phone_number;
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['phone_number'] = 'Please check your phone number';
	}
	if($email && !empty($email)) {
		$response['data']['email'] = $email;
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['email'] = 'Please check your email';
	}
	if($mobile_number && !empty($mobile_number)) {
		$response['data']['mobile_number'] = $mobile_number;
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['mobile_number'] = 'Please check your mobile number';
	}
	if($notes && !empty($notes)) {
		$response['data']['notes'] = $notes;
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['notes'] = 'Please check your notes';
	}
	if($address && !empty($address)) {
		$response['data']['address'] = $address;
	}else{
		$everything_is_ok = false;
		$response_errors['errors']['address'] = 'Please check your address';
	}

	if ($everything_is_ok)
	    {
	    	$res = $customers->UpdateCustomer($response['data']);
	    	if(!is_string($res) && $res > 0) {
		        header('Content-Type: application/json');
		        $response['code'] = 201;
				$response['message'] = 'New record updated!';
				$response['status'] = 'success';
	        	print json_encode($response);
	    	}else{
	    		$response_errors['code'] = '406';
				$response_errors['status'] = 'error';
				$response_errors['message'] = 'Not Acceptable';
		        header('HTTP/1.1 '.$response_errors['code'].' '.$response_errors['message']);
		        header('Content-Type: application/json; charset=UTF-8');

		        die(json_encode($response_errors));
	    	}

	    }
	else
    {
        $response_errors['code'] = '400';
		$response_errors['status'] = 'error';
		$response_errors['message'] = 'Bad Request';
        header('HTTP/1.1 '.$response_errors['code'].' '.$response_errors['message']);
        header('Content-Type: application/json; charset=UTF-8');

        die(json_encode($response_errors));
	}
?>