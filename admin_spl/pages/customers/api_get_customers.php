<?php 
require_once("../../../common/customers.php");
$customers = new Customers;
$result = $customers->FetchAll('assoc');
$new = array();
$new['draw'] = 1;
$new['recordsTotal'] = count($result);
$new['data'] = $result;
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($new);
?>
