<?php
include('../../../common/customers.php');
$data = array();
$response = array();
$response_errors = array();
$data['customer_id'] = trim(strip_tags($_REQUEST['customer_id']));
$data['added_by'] = trim(strip_tags($_REQUEST['added_by']));
$customers = new Customers;
$res = $customers->DelCustomer($data);


if(!is_string($res) && $res > 0) {
    header('Content-Type: application/json');
	$response['data'] = $res;
	$response['code'] = 201;
	$response['message'] = $data['customer_id'].' record deleted successfully!';
	$response['status'] = 'success';
	echo json_encode($response);
}else{
	$response_errors['code'] = '406';
	$response_errors['status'] = 'error';
	$response_errors['message'] = 'You are not eligible for this action!';//'Not Acceptable';
    header('HTTP/1.1 '.$response_errors['code'].' '.$response_errors['message']);
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode($response_errors));
}
flush();
?>