<?php
require_once("../../../common/config.php");
function checkValues($value)
{
	 // Use this function on all those values where you want to check for both sql injection and cross site scripting
	 //Trim the value
	 $value = trim($value);
	 
	// Stripslashes
	if (get_magic_quotes_gpc()) {
		$value = stripslashes($value);
	}
	
	 // Convert all &lt;, &gt; etc. to normal html and then strip these
	 $value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
	
	 // Strip HTML Tags
	 $value = strip_tags($value);
	
	// Quote the value
	$value = mysql_real_escape_string($value);
	return $value;
	
}		
 
$rec = checkValues($_REQUEST['val']);

if(!$rec){
	exit;
}
if($rec)
{

	$res = $db->select_array(array('*'), PREFIX."customers","customer_number like '%$rec%' OR customer_name like '%$rec%' OR contact_person like '%$rec%' OR phone_number like '%$rec%' OR mobile_number like '%$rec%' OR email like '%$rec%'");
}

//echo"<pre>"; print_r($res); exit;
$total =  $db->countfields(array('*'), PREFIX."customers","customer_number like '%$rec%' OR customer_name like '%$rec%' OR contact_person like '%$rec%' OR phone_number like '%$rec%' OR mobile_number like '%$rec%' OR email like '%$rec%'");

?>



<table id="example" class="dataTable" aria-describedby="example_info" style="width: 95.4%;">
       <tbody role="alert" aria-live="polite" aria-relevant="all">
        
    <tr role="row"  > <th class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending"></th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Customer Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Customer Name</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Contact Person</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Phone Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Mobile Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Balance</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
<?php
for($row=1; $row<=count($res); $row++)

{?>
	<div class="each_rec">
   			 
             	<tr class="odd" style="background:none; ">
                         
                         <td>
                             <a href="index.php?action=manage_sales&customer_id=<?php echo $res[$row]['customer_number'];?>" class="button" >Sale Now </a> 
                              
                         <td>
                           <?php echo $res[$row]['customer_number'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['customer_name'];?>
                          </td>
                          
                            <td>
                           <?php echo $res[$row]['contact_person'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['phone_number'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['mobile_number'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['balance'];?>
                          </td>
                          
                         
                          
                         
						  <td class=" sorting_1">
                            <a title="View Detail" href="index.php?action=view_customer&customer_id=<?php echo $res[$row]['customer_id'];?>"><img width="20" alt="View" src="resources/images/icons/view_icon.png" class="Employee" id=""></a>
                            
                               <a title="Edit Detail" href="index.php?action=edit_customer&customer_id=<?php echo $res[$row]['customer_id'];?>"><img width="20" alt="View" src="resources/images/icons/pencil_48.png" class="Employee" id=""></a>
                               
                                <a title="Edit Detail" href="index.php?action=del_customer&customer_id=<?php echo $res[$row]['customer_id'];?>"><img width="20" alt="View" src="resources/images/icons/cross.png" class="Employee" onclick="return confirm('Are you sure to delete it ?');" ></a>
                            </td>
                          
                         
                           
    </tr>
    	<tr><td colspan="8">
        <div class="panel panel-default">
    
                            <div class="panel-collapse"    style="">
                              <div class="panel-body">
                                 				<table aria-describedby="example_info" class="dataTable" id="example" style="margin-left: -6px; width: 100.7%;">
<thead>
     <tr style="background: orange none repeat scroll 0 0; border: 1px solid black;" role="row" class="alt-row"> <th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Sales Number</th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Sales Date</th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Customer ID</th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Total Amount</th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Discount Amount</th><th width="223" aria-label="Group Name: activate to sort column ascending" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Tax Amount</th><th width="223" aria-label="Group Name: activate to sort column ascending" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Final Amount</th><th width="223" aria-label="Group Name: activate to sort column ascending" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Total Payment</th><th width="223" aria-label="Group Name: activate to sort column ascending" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Total Balance </th></tr>
</thead>
                              <?php 
								$customer_number = $res[$row]['customer_number'];
								$sales_array = $db->select_array(array("*"),PREFIX."sales","customer_id='$customer_number'");
								for($loop=1; $loop<=count($sales_array); $loop++ ){
								?>
                                


						<tbody aria-relevant="all" aria-live="polite" role="alert">
                         <tr class="odd" style="border:none; ">
                          <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['sales_number'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['sales_date'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['customer_id'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['total_payment'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['discount_amount'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['tax_amount'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['total_amount'];
							?>
                           </td>
                           
                          
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['total_payment'];
							?> 
                            </td>
                            
                            <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['total_balance'];
							?> 
                            </td>
                            
                            
                           </tr>
                           </tbody>
                          
                           
						  <?php } ?>
                            </table> 
                           </div>
                           </div>
                           </div>
                           </td>
                           </tr>
    </div>
<?php
}

if($total==0){
	?> 
    <tr class="odd" colspan="7">
     <td>
      <div class="no-rec">No Record Found !</div>
      </td>
    <?php } ?>
</tbody>
</table>