<?php 
require_once("../common/customers.php");
$customers = new Customers;
$customer_number = $customers->nextRecord('assoc');
$added_by = $_SESSION['user_name'];
?>
 


<h2><img src="resources/images/icons/client.png" alt="icon"  width="40"/>Manage Customers</h2>
<?php if(isset($_SESSION["add_message"])) { ?>
 <div class="notification success png_bg">
     <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
     <div>
     <?php echo $_SESSION["add_message"];?>                    
</div>
</div>
<?php unset($_SESSION["add_message"]); } ?>

<?php if(isset($_SESSION["edit_message"])) { ?>
 <div class="notification success png_bg">
     <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
     <div>
     <?php echo $_SESSION["edit_message"];?>                    
</div>
</div>
<?php unset($_SESSION["edit_message"]);  } ?>

 <?php if(isset($_SESSION["dell_message"])) { ?>
 <div class="notification success png_bg">
     <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
     <div>
     <?php echo $_SESSION["dell_message"];?>                    
</div>
</div>
<?php unset($_SESSION["dell_message"]);  } ?>

                         
<div id="tab1" class="tab-content default-tab" style="display: block;">
          
  <form id="form_customer">
    <fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
    <div class="row">
      <div class="col-sm-6 form-group">
        <label>Customer Number</label> 
        <input class="form-control" type="text" name="customer_number" id="customer_number" value="<?php echo  $customer_number; ?>" readonly="readonly"  />
        <input type="hidden" id="added_by" name="added_by" value="<?php echo $added_by; ?>">
        <input type="hidden" id="updated_by" name="updated_by" value="<?php echo $added_by; ?>">
      </div>
      <div class="col-sm-6 form-group">
        <label>City</label>
        <input class="form-control" type="text" id="city" name="city"   />
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 form-group">
        <label>Contact Person</label>
        <input class="form-control" type="text" id="contact_person" name="contact_person" />
      </div>
      <div class="col-sm-6 form-group">
        <label>Phone Number</label>
        <input class="form-control" type="text" id="phone_number" name="phone_number" />
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6 form-group">
        <label>Email</label>
        <input class="form-control" type="text" id="email" name="email"   />
      </div>
      <div class="col-sm-6 form-group">
        <label>Mobile Number</label>
        <input class="form-control" type="text" id="mobile_number" name="mobile_number" />
      </div>
    </div>


    <div class="row">
      <div class="col-sm-12 form-group">
        <label>Notes </label>
        <input class="form-control" type="text" id="notes" name="notes"  />
      </div>
      <div class="col-sm-12 form-group">
        <label>Address</label>
        <textarea class="form-control" rows="5" id="address" name="address"  ></textarea>
      </div>
    </div>
    <p>
      <input class="btn btn-primary" type="submit" value="Submit" name="submit" />
    </p>
        </fieldset>
							
		 <div class="clear"></div><!-- End .clear -->
							
		 </form>
						
					</div>
            <div class="content-box"><!-- Start Content Box -->
                            
              <div class="content-box-header">
                <h3>Customer Listing</h3>
              </div> <!-- End .content-box-header -->
            <div class="content-box-content">
              <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Customer Name</th>
                        <th>Customer Address</th>
                        <th>City</th>
                        <th>Mobile Number</th>
                        <th width="10">Delete</th>
                        <th width="10">Edit</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Customer Name</th>
                        <th>Customer Address</th>
                        <th>City</th>
                        <th>Mobile Number</th>
                        <th>Delete</th>
                        <th>Edit</th>
                        <th>Created At</th>
                    </tr>
                </tfoot>
            </table>

            </div> <!-- End #tab1 -->
                                
              </div>
            </div>

    <div class="modal fade" id="edit_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <form id="e_form_customer">
          <div class="modal-body">
            <fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
            <div class="row">
              <div class="col-sm-6 form-group">
                <label>Customer Number</label> 
                <input class="form-control" type="text" name="e_customer_number" id="e_customer_number" value="<?php echo  $customer_number; ?>" readonly="readonly"  />
                <input type="hidden" id="e_customer_id" name="e_customer_id">
                <input type="hidden" id="e_added_by" name="e_added_by" value="<?php echo $added_by; ?>">
                <input type="hidden" id="e_updated_by" name="e_updated_by" value="<?php echo $added_by; ?>">
              </div>
              <div class="col-sm-6 form-group">
                <label>City</label>
                <input class="form-control" type="text" id="e_city" name="e_city"   />
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 form-group">
                <label>Contact Person</label>
                <input class="form-control" type="text" id="e_contact_person" name="e_contact_person" />
              </div>
              <div class="col-sm-6 form-group">
                <label>Phone Number</label>
                <input class="form-control" type="text" id="e_phone_number" name="e_phone_number" />
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6 form-group">
                <label>Email</label>
                <input class="form-control" type="text" id="e_email" name="e_email"   />
              </div>
              <div class="col-sm-6 form-group">
                <label>Mobile Number</label>
                <input class="form-control" type="text" id="e_mobile_number" name="e_mobile_number" />
              </div>
            </div>


            <div class="row">
              <div class="col-sm-12 form-group">
                <label>Notes </label>
                <input class="form-control" type="text" id="e_notes" name="e_notes"  />
              </div>
              <div class="col-sm-12">
                <label>Address</label>
                <textarea class="form-control" rows="5" id="e_address" name="e_address"  ></textarea>
              </div>
            </div>
                </fieldset>
                      
             <div class="clear"></div><!-- End .clear -->
                      
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input class="btn btn-primary" type="submit" value="Submit" name="submit" />
          </div>
             </form>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function OnDelSubmit (element) {
      var del=confirm("Are you sure you want to delete this record?");
        if (del==true){
          var url = "pages/customers/api_del_customers.php";
          var raw = $(element).serialize();
          var params = raw+"&added_by="+"<?php echo $added_by; ?>";
            $.ajax ({
              type: "POST",
              url: url,
              data: params,
              processData: false,
              async: false,
              success: function(result) {
                window.location.href=window.location.href;
              },
              error:function (error) {
                alert(error.statusText);
                return false;
              }
            });
          return false;
        }
        return del;
    }
    function OnEditSubmit (element) {
      var url ='pages/customers/api_get_customer.php';
      var raw = $(element).serialize();
      var params = raw+"&added_by="+"<?php echo $added_by; ?>";
      $.ajax({
        type: "POST",
        url: url,
        data: params,
        processData: false,
        async: false,
        success: function(result) {
          $('#edit_modal').modal('show');
          $('#edit_modal').on('shown.bs.modal', function () {
            var customer_id = $(this).find('#e_customer_id');
            var customer_number = $(this).find('#e_customer_number');
            var city = $(this).find('#e_city');
            var contact_person = $(this).find('#e_contact_person');
            var phone_number = $(this).find('#e_phone_number');
            var email = $(this).find('#e_email');
            var mobile_number = $(this).find('#e_mobile_number');
            var notes = $(this).find('#e_notes');
            var address = $(this).find('#e_address');
            var added_by = $(this).find('#e_added_by');
            var updated_by = $(this).find('#e_updated_by');
            customer_id.val(result.data[0].customer_id);
            customer_number.val(result.data[0].customer_number);
            city.val(result.data[0].city);
            contact_person.val(result.data[0].contact_person);
            phone_number.val(result.data[0].phone_number);
            email.val(result.data[0].email);
            mobile_number.val(result.data[0].mobile_number);
            notes.val(result.data[0].notes);
            address.val(result.data[0].address);
            updated_by.val(result.data[0].updated_by);
            added_by.val(result.data[0].added_by);
          });
        },
        error:function (error) {
          return false;
        }

      })
      return false;
    }
    
    function format ( d ) {
        return  '<table><thead><tr><th>Customer ID</th><th>Customer Name</th><th>Customer Address</th><th>City</th><th>Mobile Number</th><th> Added By</th></tr></thead>'+
            '<tbody><tr><td>'+d.customer_number +'</td><td>'+d.customer_name +'</td><td>'+d.address +'</td><td>'+d.city +'</td><td>'+d.mobile_number +'</td><td class="creator">'+d.added_by +'</td></tr></tbody></table>';
    }
   
    $(document).ready(function() {
        
        var dt = $('#example').DataTable( {
            "processing": false,
            "serverSide": false,
            "ajax": "pages/customers/api_get_customers.php",
            "columns": [
                {
                    "class":          "details-control",
                    "orderable":      true,
                    "data":           null,
                    "defaultContent": ""
                },
                { "data": "customer_name" },
                { "data": "address" },
                { "data": "city" },
                { "data": "mobile_number" },
                {
                  "class" : "text-center", 
                  "data": "customer_id",
                  "searchable": false,
                  "orderable": false,
                  render : function(data, type, row) {
                    if(row.added_by == '<?php echo $added_by; ?>'){
                      return '<form id="d_form" onsubmit="return OnDelSubmit(this);"><input type="hidden" name="customer_id" id="d_customer_id" value="'+data+'" /><button type="submit" class="btn btn-danger delete-row">Delete</button></form>';
                    }else{
                      return '<span type="submit" class="btn btn-danger delete-row disabled" >Delete</span>';
                    }
                  } 
                },
                {
                  "class" : "text-center",
                  "data": "customer_id",
                  "searchable": false,
                  "orderable": false,
                  render : function(data, type, row) {
                    if(row.added_by == '<?php echo $added_by; ?>'){
                      return '<form id="e_form" onsubmit="return OnEditSubmit(this);"><input type="hidden" name="customer_id" id="e_customer_id" value="'+data+'" /><button type="submit" class="btn btn-primary edit-row">Edit</button></form>'
                    }else{
                      return '<span type="submit" class="btn btn-primary edit-row disabled" >Edit</span>';
                    }
                  }  
                },
                { "data": "date_added" },
            ],
            "columnDefs": [
                {
                    "targets": [ -1 ],
                    "visible": false,
                    "searchable": false
                },
            ],
            "order": [[7, 'desc']]
        } );
     
        // Array to track the ids of the details displayed rows
        var detailRows = [];

        $('#example tbody').on( 'click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = dt.row( tr );
            var idx = $.inArray( tr.attr('id'), detailRows );
     
            if ( row.child.isShown() ) {
                tr.removeClass( 'details' );
                row.child.hide();
     
                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                tr.addClass( 'details' );
                row.child( format( row.data() ) ).show();
     
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( tr.attr('id') );
                }
            }
        } );

        
     
        // On each draw, loop over the `detailRows` array and show any child rows
        dt.on( 'draw', function () {
            $.each( detailRows, function ( i, id ) {
                $('#'+id+' td.details-control').trigger( 'click' );
            } );

        } );
    $.validator.addMethod("customemail", 
      function(value, element) {
         return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
      }, 
      "Please enter a valid email address."
    );
    // validate signup form on keyup and submit
    $("#form_customer").validate({
      debug: false,
      errorClass: "text-danger",
      errorElement: "span",
      rules: {
        contact_person: {
          required: true
        },
        email: {
          required: true,
          customemail: true
        },
        address: {
          required: true
        },
        notes: {
          required: true
        },
        city: {
          required: true,
        },
        phone_number: {
          required: true,
          number: true
        },
        mobile_number: {
          required: true,
          number: true
        },
      },
      // form on submit 
      submitHandler: function(form) {
        url = 'pages/customers/new_customer.php';
        var params = $(form).serialize();
        $.ajax ({
          type: "POST",
          url: url,
          data: params,
          processData: false,
          async: false,
          success: function(result) {
            form.reset();
            $('#customer_number').val(result.data.next_id);
            dt.ajax.reload();
          },
          error:function (error) {
            return false;
          }
        });
      },
      // default error placement
      errorPlacement: function (error, element) {
        $(element).parent().append(error);
      },
    });
    // validate signup form on keyup and submit
    $("#e_form_customer").validate({
      debug: false,
      errorClass: "text-danger",
      errorElement: "span",
      rules: {
        e_contact_person: {
          required: true
        },
        e_email: {
          required: true,
          customemail: true
        },
        e_address: {
          required: true
        },
        e_notes: {
          required: true
        },
        e_city: {
          required: true,
        },
        e_phone_number: {
          required: true,
        },
        e_mobile_number: {
          required: true,
        },
      },
      // form on submit 
      submitHandler: function(form) {
        url = 'pages/customers/api_update_customers.php';
        var params = $(form).serialize();
        $.ajax ({
          type: "POST",
          url: url,
          data: params,
          processData: false,
          async: false,
          success: function(result) {
            $('#edit_modal').modal('hide');
            $('#edit_modal').on('hidden.bs.modal', function () {
              $('#e_form_customer').reset();
            })
            dt.ajax.reload();
            return false;
          },
          error:function (error) {
            return false;
          }
        });
      },
      // default error placement
      errorPlacement: function (error, element) {
        $(element).parent().append(error);
      },
    });
    
    

  });
  </script>
 
