<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
if($_POST['submit'])
	{
		
	     extract($_POST);
		 $entry_date = $db->date_time('D');
		 $job_no = $db->autoincrement(PREFIX."laptops_repairing", "job_no");
		 
		 $data_array = array("job_no"=>$job_no,"entry_date"=>$entry_date,"customer_name"=>$customer_name,"customer_contact"=>$customer_contact,"equipment_name"=>$equipment_name,"hdd"=>$hdd,"ram"=>$ram,"problem_description"=>$problem_description);
		//echo"<pre>"; print_r($data_array); exit;
		 
		$db->insert($data_array,PREFIX."laptops_repairing");
		$_SESSION["add_message"] = "Laptop repairing detail have been added successfully."; 
		
	?>
    <script type="text/javascript">
				location.href = "index.php?action=manage_laptops";
		 	</script> 
         <?php     
		 exit();	   
	}


//Fetching new job no from DB
$job_no = $db->autoincrement(PREFIX."laptops_repairing", "job_no");

// how many records should be displayed on a page?
$res 				= $db->selectSRow(array('rows'),PREFIX."pagination_rows");
$records_per_page 	= $res['rows'];

?>
 
<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#laptop_entry").validate({
			rules: {
				job_no: "required",
				customer_name: "required",
				customer_contact: "required",
				equipment_name: "required",
				problem_description: "required",
				
			},
			messages: {
				job_no: "Please enter job number",
				customer_name: "Please enter customer name",
				customer_contact: "Please enter customer contact",
				equipment_name: "Please enter equipment name",
				problem_description: "Please enter problem description",
			 
			}
		});

	});
	</script>
    
<script>
function change_status(data){
	  var data_value = data.value;
      var laptopid = $(data).data('laptop-id');
	  
	 $.ajax({
		url: 'pages/laptops_repairing/edit_laptop_status.php',
		data: {status:data_value, laptopid:laptopid},
		cache: false,
		error: function(e){
			alert("Error occured");
		},
		success: function(response){
		   console.info(response);
		   location.reload(true);
		}
	});   
}
</script>

<!--------------For ajax base searching------------------->
<script language="javascript">

$(document).ready(function(){

	//show loading bar

	function showLoader(){

		$('.search-background').fadeIn(200);

	}

	//hide loading bar

	function hideLoader(){

		$('#sub_cont').fadeIn(1500);

		$('.search-background').fadeOut(200);

	};

	$('#search').keyup(function(e) {
		 if(e.keyCode == 13) {

 		showLoader();

		$('#sub_cont').fadeIn(1500);

		$("#content #sub_cont").load("pages/laptops_repairing/manage_search.php?val=" + $("#search").val(), hideLoader());



      }

  

      });

	  

	$(".searchBtn").click(function(){

	

		//show the loading bar

		showLoader();

		$('#sub_cont').fadeIn(1500);

		  

		$("#content #sub_cont").load("pages/laptops_repairing/manage_search.php?val=" + $("#search").val(), hideLoader());



	});

});

</script>
<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>




<h2><img width="24" alt="icon" src="resources/images/icons/image_add_48.png">Manage Laptops</h2>                         
 
 
<?php if(isset($_SESSION["add_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["add_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["add_message"]); } ?>

 <?php if(isset($_SESSION["edit_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["edit_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["edit_message"]);  } ?>

                         
<div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form  enctype="multipart/form-data" method="post" action="" id="laptop_entry" name="laptop_entry">
							
							<fieldset> 
							  <p>
							    <label>Job No <span style="color:#FF0000">*</span> </label>
									<input type="text"  name="job_no" id="job_no" class="text-input medium-input" value="<?php echo $job_no; ?>" readonly>
                                   
                            </p>  
                              <p>
							    <label>Customer Name <span style="color:#FF0000">*</span> </label>
									<input type="text"  name="customer_name" id="customer_name" class="text-input medium-input">
                                   
                            </p>
                             <p>
							    <label>Contact <span style="color:#FF0000">*</span> </label>
									<input type="text"  name="customer_contact" id="customer_contact" class="text-input medium-input">
                                   
                            </p>
                            <p>
							    <label>Equipment Name <span style="color:#FF0000">*</span> </label>
									<input type="text"  name="equipment_name" id="equipment_name" class="text-input medium-input">
                                   
                            </p>
                            <p>
							    <label>HDD </label>
									<input type="text"  name="hdd" id="hdd" class="text-input medium-input">
                                   
                            </p>
                            <p>
							    <label>RAM  </label>
									<input type="text"  name="ram" id="ram" class="text-input medium-input">
                                   
                            </p>
                            
                             <p>
							    <label>Problem Description  <span style="color:#FF0000">*</span> </label>
									<textarea name="problem_description" id="problem_description" rows="5" class="text-input medium-input" style="width: 540px; height: 102px;"></textarea>
                                    
                            </p>
							<p>
									<input type="submit" value="Submit" class="button" name="submit" onclick="window.print()">
							  </p>
								
						  </fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div>
            <div class="content-box"><!-- Start Content Box -->
                            
                            <div class="content-box-header">
                                
                                <h3 style="cursor: s-resize;">Laptop Listing</h3>
                                
                           </div> <!-- End .content-box-header -->
                    <div style="min-height:480px;" class="content-box-content">
            <div id="tab1" class="tab-content default-tab" style="display: block;"> <!-- This is the target div. id must match the href of this div's tab -->
               
                    <label>Show 
                    <select name="rows"  id="rows" size="1" aria-controls="example" onchange="select_rows(this.value)" >						
                    				<option value="10" <?PHP if($records_per_page == 10) echo"selected";?>>10</option>
                                    <option value="25" <?PHP if($records_per_page == 25) echo"selected";?>>25</option>
                                    <option value="50" <?PHP if($records_per_page == 50) echo"selected";?>>50</option>
                                    <option value="100" <?PHP if($records_per_page == 100) echo"selected";?>>100</option>
                    </select> entries</label>
                     
                     <div class="dataTables_filter" id="example_filter" style="float: right;"><label>Search: <input type="text" aria-controls="example" value="" name="searchBox" id="search"></label></div>
                     
         
                  <div class="searchBtn">
					&nbsp;
       			</div>   
    
    
        
	
<br clear="all" />
<div id="content">
	<div id="sub_cont" class="search-record" >	</div>
</div>









                     
<div role="grid" class="dataTables_wrapper" id="example_wrapper">
<table id="example" class="dataTable" aria-describedby="example_info">
<thead>
     <tr role="row"><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Job No</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Date</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Customer Name</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Contact</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Equipment Name</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">HDD</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">RAM</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Status</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
</thead>

                
<!--Pagination Code put everywhere u need it-->
<?php
//Fetching all records from db
$results_objects = $db->select_orderby(PREFIX."laptops_repairing","","laptop_id DESC");
$laptop_id_array = array();
foreach ($results_objects as $key => $value) {
 	$laptop_id_array[] = $value->laptop_id;
}

// the number of total records is the number of records in the array
$pagination->records(count($laptop_id_array));
// records per page
$pagination->records_per_page($records_per_page);
// here's the magick: we need to display *only* the records for the current page
$laptop_id_array = array_slice(
    $laptop_id_array,
    (($pagination->get_page() - 1) * $records_per_page),
    $records_per_page
);
?>  
               <tfoot>
                      </tfoot>  
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php  foreach ($laptop_id_array as $index => $laptop_id):  ?>
                          
                          <tr class="odd">
                            <td>
                            <?php 
                            $job_no 	= $db->selectSRow(array("job_no"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
                            echo $job_no['job_no'];
                            ?>
                            </td>
                         
                         	  <td>
                            <?php 
                            $entry_date 	= $db->selectSRow(array("entry_date"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
                            echo $entry_date['entry_date'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $customer_name 	= $db->selectSRow(array("customer_name"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
                          	echo substr($customer_name['customer_name'],0,17).'...'; 
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $customer_contact 	= $db->selectSRow(array("customer_contact"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
                            echo $customer_contact['customer_contact'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $equipment_name 	= $db->selectSRow(array("equipment_name"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
                            echo $equipment_name['equipment_name'];
                            ?>
                            </td>
                            
                              <td>
                            <?php 
                            $hdd 	= $db->selectSRow(array("hdd"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
                            echo $hdd['hdd'];
                            ?>
                            </td>
                              <td>
                            <?php 
                            $ram 	= $db->selectSRow(array("ram"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
                            echo $ram['ram'];
                            ?>
                            </td>
                            <td>
                           <select class="sel_opr" name="status" data-laptop-id="<?php echo $laptop_id; ?>"  onchange="change_status(this)">
                           <?php 
                            $status 	= $db->selectSRow(array("status"),PREFIX."laptops_repairing","laptop_id='$laptop_id'");
							//echo"<pre>"; print_r( $status); 
							?>
                            <option value="0" <?php if ($status['status'] == '0') echo"selected = 'selected'"; ?>>Open</option>
							<option value="1" <?php if ($status['status'] == '1') echo"selected = 'selected'"; ?>>Close</option>
                            <option value="2" <?php if ($status['status'] == '2') echo"selected = 'selected'"; ?>>Repaired</option>
                            <option value="3" <?php if ($status['status'] == '3') echo"selected = 'selected'"; ?>>N/R</option>
                         </select>
                            </td>
                           
                           <td class=" sorting_1">
                            <a title="View Detail" href="index.php?action=view_laptop&laptop_id=<?php echo $laptop_id;  ?>"><img width="20" alt="View" src="resources/images/icons/view_icon.png" class="Employee" id=""></a>
                            </td>
                    </tr>
                    <?php endforeach?>
                            
                             
                            
                            </tbody></table>
                            
<?php
// render the pagination links
$pagination->render();

?>
                            </div>
                                    
                      </div> <!-- End #tab1 -->
                                
              </div>
            </div>