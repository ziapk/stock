<?php
require_once("../../../common/config.php");
function checkValues($value)
{
	 // Use this function on all those values where you want to check for both sql injection and cross site scripting
	 //Trim the value
	 $value = trim($value);
	 
	// Stripslashes
	if (get_magic_quotes_gpc()) {
		$value = stripslashes($value);
	}
	
	 // Convert all &lt;, &gt; etc. to normal html and then strip these
	 $value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
	
	 // Strip HTML Tags
	 $value = strip_tags($value);
	
	// Quote the value
	$value = mysql_real_escape_string($value);
	return $value;
	
}		
 
$rec = checkValues($_REQUEST['val']);

if(!$rec){
	exit;
}
if($rec)
{

	$res = $db->select_array(array('*'), PREFIX."laptops_repairing","customer_name like '%$rec%' OR job_no like '%$rec%' OR customer_contact like '%$rec%'");
}

//echo"<pre>"; print_r($res); exit;
$total =  $db->countfields(array('*'), PREFIX."laptops_repairing","customer_name like '%$rec%' OR job_no like '%$rec%' OR customer_contact like '%$rec%'");

?>


<table id="example" class="dataTable" aria-describedby="example_info" style="  width: 95.4%;">
       <tbody role="alert" aria-live="polite" aria-relevant="all">
       <tr role="row"><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Job No</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Date</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Customer Name</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Contact</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Equipment Name</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">HDD</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">RAM</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Status</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
       
<?php
for($row=1; $row<=count($res); $row++)

{?>
	<div class="each_rec">
   			 
             	<tr class="odd">
                         
                         <td>
                           <?php echo $res[$row]['job_no'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['entry_date'];?>
                          </td>
                          
                            <td>
                           <?php echo $res[$row]['customer_name'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['customer_contact'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['equipment_name'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['hdd'];?>
                          </td>
                          
                          <td>
                           <?php echo $res[$row]['ram'];?>
                          </td>
                          
                         <td>
                           <select class="sel_opr" name="status" data-laptop-id="<?php echo $res[$row]['laptop_id'];   ?>"  onchange="change_status(this)">
                           <option value="0" <?php if ($res[$row]['status'] == '0') echo"selected = 'selected'"; ?>>Open</option>
							<option value="1" <?php if ($res[$row]['status'] == '1') echo"selected = 'selected'"; ?>>Close</option>
                            <option value="2" <?php if ($res[$row]['status'] == '2') echo"selected = 'selected'"; ?>>Repaired</option>
                            <option value="3" <?php if ($res[$row]['status'] == '3') echo"selected = 'selected'"; ?>>N/R</option>
                           </select>
                            </td>
						   <td class=" sorting_1">
                            <a title="View Detail" href="index.php?action=view_laptop&laptop_id=<?php echo $res[$row]['laptop_id'];?>">
                            <img width="20" alt="View" src="resources/images/icons/view_icon.png" class="Employee" id="">
                            </a>
                            </td>
                          
                         
                           
    </tr>
    
   
    </div>
<?php
}

if($total==0){
	?> 
    <tr class="odd" colspan="7">
     <td>
      <div class="no-rec">No Record Found !</div>
      </td>
    <?php } ?>
</tbody>
</table>