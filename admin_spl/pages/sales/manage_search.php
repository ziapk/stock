<?php
require_once("../../../common/config.php");
function checkValues($value)
{
	 // Use this function on all those values where you want to check for both sql injection and cross site scripting
	 //Trim the value
	 $value = trim($value);
	 
	// Stripslashes
	if (get_magic_quotes_gpc()) {
		$value = stripslashes($value);
	}
	
	 // Convert all &lt;, &gt; etc. to normal html and then strip these
	 $value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
	
	 // Strip HTML Tags
	 $value = strip_tags($value);
	
	// Quote the value
	$value = mysql_real_escape_string($value);
	return $value;
	
}		
 
$rec = checkValues($_REQUEST['val']);

if(!$rec){
	exit;
}
if($rec)
{

	$res = $db->select_array(array('*'), PREFIX."sales","sales_number like '%$rec%' OR customer_id like '%$rec%' OR final_total_amount like '%$rec%' OR total_payment like '%$rec%' OR total_balance like '%$rec%'");
}

//echo"<pre>"; print_r($res); exit;
$total =  $db->countfields(array('*'), PREFIX."sales","sales_number like '%$rec%' OR customer_id like '%$rec%' OR final_total_amount like '%$rec%' OR total_payment like '%$rec%' OR total_balance like '%$rec%'");

?>



<table id="example" class="dataTable" aria-describedby="example_info" style="width: 95.4%;">
       <tbody role="alert" aria-live="polite" aria-relevant="all">
  
  
     <tr role="row"  > <th class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Group Name: activate to sort column ascending"></th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Group Name: activate to sort column ascending">Sales Number</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Customer ID </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Total Amount </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Discount Amount </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Tax Amount</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending"> Final Total Amount </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Payment </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Balance </th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
 
<?php
for($row=1; $row<=count($res); $row++)

{?>
	<div class="each_rec">
   			 
             	<tr class="odd" style="background:none; ">
                         
                         <?php 
						 	$sales_number = $res[$row]['sales_number'];
							
							
							$total_balance	= $db->selectSRow(array("total_balance"),PREFIX."sales","sales_number='$sales_number'");
                            $total_balance	= $total_balance['total_balance'];
							
							$customer_id	= $db->selectSRow(array("customer_id"),PREFIX."sales","sales_number='$sales_number'");
                            $customer_id 	=  $customer_id['customer_id'];
							
							
                           ?>
                           
                        
                         
                         <td style="width: 12%">
                            
                           <?php if($total_balance>0) {?>
                              <a href="index.php?action=payment_salesadd&customer_id=<?php echo $customer_id; ?>&sales_number=<?php echo $sales_number; ?>" class="button" >Pay Now </a> 
                            <?php } ?>
                            </td>
                          
                          <td> 
                            
                           <?php echo $sales_number;?>
                          </td>
                          
                          <td>
                           <?php echo $customer_id;?>
                          </td>
                          
                           <td>
                             <?php 
                           $total_amount	= $db->selectSRow(array("total_amount"),PREFIX."sales","sales_number='$sales_number'");
                            echo $total_amount['total_amount'];
                            ?>
                            </td>
                          <td>
                             <?php 
                           $discount_amount	= $db->selectSRow(array("discount_amount"),PREFIX."sales","sales_number='$sales_number'");
                            echo $discount_amount['discount_amount'];
                            ?>
                            </td>
                            
                              <td>
                             <?php 
                           $tax_amount	= $db->selectSRow(array("tax_amount"),PREFIX."sales","sales_number='$sales_number'");
                            echo $tax_amount['tax_amount'];
                            ?>
                            </td>
                            
                            
                              <td>
                             <?php 
                           $final_total_amount	= $db->selectSRow(array("final_total_amount"),PREFIX."sales","sales_number='$sales_number'");
                            echo $final_total_amount['final_total_amount'];
                            ?>
                            </td>
                            
                              <td>
                             <?php 
                           $total_payment	= $db->selectSRow(array("total_payment"),PREFIX."sales","sales_number='$sales_number'");
                            echo $total_payment['total_payment'];
                            ?>
                            </td>
                            
                            
                              <td>
                             <?php 
                          		echo $total_balance;
                            ?>
                            </td>
                         
                          
                         
						   <td class=" sorting_1">
                            <a title="View Sale" href="index.php?action=view_sale&sales_number=<?php echo $sales_number;  ?>"><img width="20" alt="View" src="resources/images/icons/view_icon.png" class="Employee" id=""></a>
                            
                               <a title="Edit Sale" href="index.php?action=edit_sale&sales_number=<?php echo $sales_number;  ?>"><img width="20" alt="Edit" src="resources/images/icons/pencil_48.png" class="Employee" id=""></a>
                               
                              
                            </td>
                         
                           
    </tr>
    	<tr><td colspan="8">
        <div class="panel panel-default">
    
                            <div class="panel-collapse"    style="">
                              <div class="panel-body">
                                 				<table aria-describedby="example_info" class="dataTable" id="example" style="margin-left: -6px; width: 100.7%;">
<thead>
         <tr role="row"  style="background: orange none repeat scroll 0 0; border: 1px solid black;" > <th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Record Number</th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Sales Date</th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Supplier Number</th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Stock Item </th><th width="223" aria-label="Group Name: activate to sort column ascending" style="width: 708px;" colspan="1" rowspan="1" aria-controls="example" tabindex="0" role="columnheader" class="sorting">Sales Quantity </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Purchasing Price </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Sales Price </th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"  aria-label="Group Name: activate to sort column ascending">Total Amount </th></tr>

</thead>
                            


						<tbody aria-relevant="all" aria-live="polite" role="alert">
						
						<?php 
						 	 
						$sales_array = $db->select_array(array("*"),PREFIX."sales_detail","Sales_Number='$sales_number'","","Sales_ID DESC");
						//echo"<pre>"; print_r($sales_array); exit;
						for($loop=1; $loop<=count($sales_array); $loop++ ){
						?>
								
                          <tr class="odd" style="border:none; ">
                          <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['Sales_ID'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            $sales_date 	= $db->selectSRow(array("sales_date"),PREFIX."sales","sales_number='$sales_number'");
							echo $sales_date = $sales_date['sales_date'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['Supplier_Number'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['Stock_Item'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['Sales_Quantity'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['Purchasing_Price'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['Sales_Price'];
							?>
                           </td>
                           
                           <td style="11%">
                            <?php 
                            echo $sales_array[$loop]['Sales_Total_Amount'];
							?>
                           </td>
                          </tr>
                           </tbody>
                          
                           
						  <?php } ?>
                            </table> 
                           </div>
                           </div>
                           </div>
                           </td>
                           </tr>
    </div>
<?php
}

if($total==0){
	?> 
    <tr class="odd" colspan="7">
     <td>
      <div class="no-rec">No Record Found !</div>
      </td>
    <?php } ?>
</tbody>
</table>