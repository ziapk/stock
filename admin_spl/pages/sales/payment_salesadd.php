<?php 
//---------------------------------------------------- Updation ---------------------------------------------------------------//
/*if(isset($_POST['submit']))
	{
	 extract($_POST);
	 //Fetching  sales_number from db
	 $ref_id 			= $db->autoincrement(PREFIX."payment_transactions", "ref_id");
	 $sub_total 		= isset($_POST['sub_total'])?$_POST['sub_total']:""; 
	 $payment 			= isset($_POST['payment'])?$_POST['payment']:"";
	 $balance 			= $sub_total - $payment;  
	 $date_added		= $db->date_time('DT');
	 $added_by			= $_SESSION['user_name'];
	 
	  if($payment > $sub_total){
		?>
        <script type="text/javascript">
			alert("Sorry, payment can't be greater then total due amount");
			location.href = "index.php?action=manage_sales";
		</script>
        	 
	 <?php 
	 	exit();
	 }
	else{ 
		 //data array for sales table
		 $data_array = array("ref_id"=>$ref_id,"type"=>"sales","customer"=>$customer_id,"supplier"=>"","sub_total"=>$sub_total,"payment"=>$payment,"balance"=>$balance,"due_date"=>$due_date,"date_transaction"=>$transaction_date,"date_added"=>$date_added,"added_by"=>$added_by);
		$res = $db->insert($data_array,PREFIX."payment_transactions");
		if($res){
			
			$existance_payment	= $db->selectSRow(array("total_payment"),PREFIX."sales","sales_number='$sales_number'");
			$existance_payment	= $existance_payment['total_payment'];
			$remaining_payment	= $payment + $existance_payment;
			
			$upd = $db->updateCondition(array('total_payment'=>$remaining_payment,'total_balance'=>$balance),PREFIX."sales","sales_number='$sales_number'");
		}
		if($res)
		$upd = $db->updateCondition(array('balance'=>$balance),PREFIX."customers","customer_number='$customer_id'");
		
		
		
		$_SESSION["add_message"] = "Payment detail have been added successfully.";
		 ?>
		<script type="text/javascript">
		location.href = "index.php?action=manage_sales";
		</script>
		<?php 
		exit();
	}
}*/



//GET sales_number from href
$customer_id = isset($_GET['customer_id'])?$_GET['customer_id']:"";



//GET sales_number from href
$sales_number = isset($_GET['sales_number'])?$_GET['sales_number']:"";

$sub_total=0;
$res = $db->select_array(array('total_balance'),PREFIX."sales","customer_id='$customer_id'");
for($p=1; $p<=count($res); $p++){
	$sub_total = $sub_total + $res[$p]['total_balance'];
}
?>
 
<!-- Javascript Code -->
<script>
	$(document).ready(function(){
		// validate signup form on keyup and submit
		$("#add_payment_form").validate({
			rules: {
				sales_number_check: "required",
				customer_id_check: "required",
				sub_total: {
					    required: true,
     			 		number: true
				},
				payment: {
					    required: true,
     			 		number: true
				},
				due_date: "required"
			},
			messages: {
				sales_number_check: "Please enter sales number",
				customer_id_check: "Please enter customer id",
				sub_total: "Please enter sub total amount",
				payment: "Please enter  payment",
				due_date: "Please enter due date",
				transaction_date: "Please enter transaction date",
				 
			}
		});

	});
	console.log($.now());
</script>

 
<!-- End of Javascript Code -->



<h2><img src="resources/images/icons/sale_now.png" alt="icon"  width="40"/> Add Payment</h2>
       
<div id="tab1" class="tab-content default-tab" style="display: block;">
		<form action="" enctype="multipart/form-data" name="add_payment_form" id="add_payment_form" method="post" >
		<!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
		<table class="table normal-table">
			<tr>
				<th width="150">Ref ID :</th>
				<td><?php echo $sales_number; ?></td>
			</tr>
			<tr>
				<th>Customer ID :</th>
				<td><?php echo $customer_id; ?></td>
			</tr>
			<tr>
				<th>Sub Total :</th>
				<td><?php echo $sub_total; ?></td>
			</tr>
			<tr>
				<th>Transation Date :</th>
				<td>
					<?php
						$transaction_date = time();
						echo date('g:iA \o\n l jS F Y', $transaction_date);
					?>
				</td>
			</tr>
		</table>
		<p>
		<label>Payment  <span class="style1" style="color:#F00"> *</span></label> 
		<input class="form-control" type="text" id="payment" name="payment"  />
		      
		</p> 
		        
		          <p>
		<label>Due Date  <span class="style1" style="color:#F00"> *</span></label> 
		<input class="form-control" type="text" id="due_date" name="due_date"  />
		      
		</p>
		<p>
	        <input name="sales_number" type="hidden" value="<?php echo $sales_number; ?>" />
	        <input name="customer_id" type="hidden" value="<?php echo $customer_id; ?>" />
	        <input class="button" type="submit" value="Submit" name="submit" />
        </p>
							
		 <div class="clear"></div><!-- End .clear -->
							
		 </form>
					
     	</div>
              
            
              