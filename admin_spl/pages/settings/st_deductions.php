<?php 

if($_POST['deduction_form'])
	{
	  extract($_POST);
	

	$deduction_array = array();
	for($rec=1; $rec<count($_POST)-3; $rec ++ )
	{
		$deduction_array["d".$rec] = $_POST['d'.$rec];
	}
	$deduction_array['group_name'] = $group_name;
	$deduction_array['month'] = $month;
	$deduction_array['year'] = $year;
	 
	//Check record already exist OR NOT
	$count_array = array('*');	
	$counter = $db->countfields($count_array,PREFIX."deduction_report","group_name='$group_name' && month='$month' && year='$year'");
	
	if($counter>0){
		$upd = $db->updateCondition($deduction_array,PREFIX."deduction_report","group_name='$group_name' && month='$month' && year='$year'");
			if($upd)
					 $_SESSION["upd_message"] = "Deduction report have been updated successfully."; 
	}
	else{
		$ins = $db->insert($deduction_array,PREFIX."deduction_report");
		if($ins)
				 $_SESSION["add_message"] = "Deduction report have been added successfully."; 
		}
	}
 
 
	//Fetching all active groups from db 
	$all_rec=array("*");
	$rs = $db->select($all_rec,PREFIX."project","status=1");
	/*echo"<pre>"; print_r($result); exit;*/
?>

<!-------------------------------------------JS for show only month in datepicker --------------------------------->
<style>
.error{
	background-color: rgb(143,193,233);;
}
</style>
<script type="text/javascript">
 
$(function() {
 $('#selectmonth').datepicker({
     changeMonth: true,
     changeYear: true,
     dateFormat: 'MM',
       
     onClose: function() {
        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
     },
       
     beforeShow: function() {
       if ((selDate = $(this).val()).length > 0) 
       {
          iYear = selDate.substring(selDate.length - 4, selDate.length);
          iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
           $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
       }
    }
  });
  
 $('#selectyear').datepicker({
     changeMonth: true,
     changeYear: true,
     dateFormat: 'yy',
       
     onClose: function() {
        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
     },
       
     beforeShow: function() {
       if ((selDate = $(this).val()).length > 0) 
       {
          iYear = selDate.substring(selDate.length - 4, selDate.length);
          iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
           $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
       }
    }
  });
  
  });
 
 
 $(document).ready(function(){
		// validate signup form on keyup and submit
		$("#search_deduction").validate({
			rules: {
				group_id: "required",
				month: "required",
				year: "required",
			},
			messages: {
				group_id: "Please select group name",
				month: "Please select month",
				year: "Please select year",
			}
		});

	});
  	
</script>

<!-------------------------------------------//////---------------------------------------------------------------->
<!-- --------------------------------------Style for show only month in datepicker ------------------------------->

<!----------------------------------------------------------////-------------------------------------------------------->



<h2><img src="resources/images/icons/setting.png" alt="icon" /> Settings</h2>
<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					 
					<h3>Group Daily Deduction Settings</h3>
				
					
					
					
				</div> <!-- End .content-box-header -->
                
				<div class="content-box-content">
                
                <div id="tab1" class="tab-content default-tab" style="display: block;">
                <?php if(isset($_SESSION["upd_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["upd_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["upd_message"]); } ?>

<?php if(isset($_SESSION["add_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["add_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["add_message"]); } ?>   



      <form  enctype="multipart/form-data" method="Post" action="" id="search_deduction" >

        <fieldset>

        
          <table width="100%" border="1">
			<tbody>
            	<tr class="alt-row">
 					<td><p>
    					<label>Group</label>
							<select style="width:200px !important;"  class="text-input medium-input" id="group_id" name="group_id">
								<option value="">Choose Group</option>
									<?php foreach ($rs as $key => $result) {?>
											<option value="<?php echo $result->id; ?>"><?php echo $result->group_name; ?></option>
									<?php } ?>
							</select>
						</p>
					</td>

              		<td><p>
						<label>Month</label>
								<input type="text" style="width:200px !important;"  id="selectmonth" name="month" class="text-input medium-input"> 
        						 
 						</p>
                    </td>
              		<td>
                    <p>
						<label>Year</label>
							 <input type="text" style="width:200px !important;" class="text-input medium-input"  id="selectyear" name="year">
           			</p>
                    </td>
				 </tr>
 			 </tbody>
            </table>
		   <p>

            <input type="submit" id="search" name="search" value="Search" class="button">
 </p>

        </fieldset>

        <div class="clear"></div>

        <!-- End .clear -->

        

      </form>

    </div>
				<!-------------------------------------Table executed---------------------------------------->
                
                


               
                <?php if(isset($_POST['month']) && isset($_POST['year']) && isset($_POST['group_id'])) 
				{  
						$group_id 	= $_POST['group_id'];
						$nmonth 	= date('m',strtotime($_POST['month']));
						$year 		= $_POST['year'];
						
						//Returning group_name against posted group_id
						$field = array('group_name'=>'group_name');
						$rs = $db->selectfeilds($field, PREFIX."project","id=$group_id");
						$group_name = $rs['group_name'];
						
						//Returning calender against posted group_id, month, year
						echo"<form  enctype='multipart/form-data' method='post' action=''>";
						echo $db->draw_calendar($nmonth,$year,$group_name);
						
						if(strtotime($_POST['year']."/".$nmonth."/31") >= strtotime(date('Y/m/d'))){
							echo"<input type='submit' value='Update' class='button' name='deduction_form'>";
                       	}
						echo"</form>";
					
						 
				?>
                <div id="tab1" class="tab-content default-tab" style="display: block;">
					 
					</div> 
                 <?php } ?>
               					
   </div>
</div>