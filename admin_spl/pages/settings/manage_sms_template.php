<?php 

if($_POST['sms_template_form'])
	{
	  	 extract($_POST);
		 $status = isset($_POST['status']) ? $_POST['status'] : 0 ;
		 
		 //Pick SMS Type name from its corresponding set_id
		 $sms_title = $db->selectSRow(array('sms_title'),PREFIX."sms_settings","set_id ='".$temp_id."'");
		 $selected_temp = $sms_title['sms_title'];
		 
		 $data_array = array("selected_temp"=>$selected_temp,"temp_format"=>$temp_format,"status"=>$status);
		
		 $fields = array("*");
		 $alrdy = $db->selectSRow($fields,PREFIX."sms_templates","selected_temp ='".$selected_temp."' && temp_format ='".$temp_format."' && status ='".$status."'");
		 
		if($alrdy){//check if already exist 
			$_SESSION["already_message"] = "SMS template already exists.";   
		}
		else{
			$db->insert($data_array,PREFIX."sms_templates");
			$_SESSION["add_message"] = "SMS template have been added successfully."; 
			
		}
	}
 
 
	//Fetching all active SMS Templates from db 
 	$all_rec=array("*");
	$rs = $db->select($all_rec,PREFIX."sms_settings","status='1'");
	 
?>


<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<style>
.error{
	background-color:inherit;
}
</style>
<script>
$(document).ready(function(){
		// validate sms seting form on submit
		$("#sms_template").validate({
			rules: {
				temp_id: "required",
				temp_format: "required",
			},
			messages: {
				temp_id: "Please select SMS template",
				temp_format: "Please enter SMS template format",
			}
		});

	});
</script>



<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>




<h2><img src="resources/images/icons/setting.png" alt="icon" /> Settings</h2>

<?php if(isset($_SESSION["add_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["add_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["add_message"]); } ?>

<?php if(isset($_SESSION["already_message"])) { ?>
                                         <div class="notification error png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["already_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["already_message"]); } ?> 

<?php if(isset($_SESSION["dell_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["dell_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["dell_message"]); } ?>    

<?php if(isset($_SESSION["edit_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["edit_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["edit_message"]); } ?> 


<div class="content-box"><!-- Start Content Box -->

				<div class="content-box-header">
					<h3>SMS Type</h3>
				</div> <!-- End .content-box-header -->

				<div class="content-box-content">
                <div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form  enctype="multipart/form-data" method="post" action="" name="sms_template" id="sms_template">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
							
                          <p>
    					<label>SMS Template</label>
							<select style="width:200px !important;"  class="text-input medium-input" id="temp_id" name="temp_id">
								<option value="">Choose SMS Type</option>
									<?php foreach ($rs as $key => $result) {?>
											<option value="<?php echo $result->set_id; ?>"><?php echo $result->sms_title; ?></option>
									<?php } ?>
							</select>
						</p>
							  <p>
								<label>SMS Format</label>
                                  
                                  <textarea name="temp_format" id="temp_format" rows="5" class="text-input medium-input" placeholder="Write down SMS template format here"></textarea>
<span style="color:#FF0000">*</span> 
                            
								</p>
								<p>
							    <label>Active </label>
							    <input type="checkbox" checked="checked" value="1" id="status" name="status">
							  </p>
							<p>
									<input type="submit" value="Submit" class="button" name="sms_template_form">
								</p>
                                								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div>
 				 
				 
                 
                    <div id="tab1" class="tab-content default-tab" style="display: block;"> <!-- This is the target div. id must match the href of this div's tab -->
                    
                     <div id="example_length" class="dataTables_length"><label>Show <select name="rows"  id="rows" size="1" aria-controls="example" onchange="select_rows(this.value)" ><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div>
                    
                    
<div role="grid" class="dataTables_wrapper" id="example_wrapper">
<table id="example" class="dataTable" aria-describedby="example_info">
<thead>
     <tr role="row"><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">Selected SMS Type</th><th width="223" class="sorting" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 708px;" aria-label="Group Name: activate to sort column ascending">SMS Format</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Status</th><th width="98" class="sorting_disabled " role="columnheader" rowspan="1" colspan="1" style="width: 333px;" aria-label="Actions">Actions</th></tr>
</thead>

 

<?php
 
//Fetching all records from db
$results_objects = $db->selectAll(PREFIX."sms_templates");
$temp_id_array = array();
foreach ($results_objects as $key => $value) {
 	$temp_id_array[] = $value->temp_id;
}
// how many records should be displayed on a page?
$res 				= $db->selectSRow(array('rows'),PREFIX."pagination_rows");
$records_per_page 	= $res['rows'];

// the number of total records is the number of records in the array
$pagination->records(count($set_id_array));
// records per page
$pagination->records_per_page($records_per_page);
// here's the magick: we need to display *only* the records for the current page
$temp_id_array = array_slice(
    $temp_id_array,
    (($pagination->get_page() - 1) * $records_per_page),
    $records_per_page
);

?>
 	<tbody role="alert" aria-live="polite" aria-relevant="all">
    <?php foreach ($temp_id_array as $index => $temp_id):  ?>
   
     <tr class="<?php if($temp_id % 2 == 0)echo"even"; else echo"odd"; ?>">
        <td>
        <?php 
		$selected_temp 	= $db->selectSRow(array("selected_temp"),PREFIX."sms_templates","temp_id=$temp_id");
		echo $selected_temp['selected_temp'];
		?>
        </td>
     
        <td>
		<?php 
		$temp_format 	= $db->selectSRow(array("temp_format"),PREFIX."sms_templates","temp_id=$temp_id");
		echo $temp_format['temp_format'];
		?>
        </td>
     
        <td>
		<?php 
		$status 	= $db->selectSRow(array("status"),PREFIX."sms_templates","temp_id=$temp_id");
		if($status['status'] == 1) echo "Active"; else echo"InActive";
		?>
        </td>
     
        <td>
		 <a title="Edit" href="index.php?action=edit_sms_template&temp_id=<?php echo $temp_id; ?>"><img width="20" alt="Edit" src="resources/images/icons/pencil.png" class="Doctor" id=""></a>
                                    <a title="Delete" href="index.php?action=del_sms_template&temp_id=<?php echo $temp_id; ?>"  onCLick="return confirm('Are you sure to delete it ?');" ><img width="20" alt="Delete" src="resources/images/icons/cross.png" class="Employee" id=""></a>
         </td>
    </tr>
	
    <?php endforeach?>
	</tbody>
</table>

<?php

// render the pagination links
$pagination->render();

?>


                                  </div>
                          </div> <!-- End #tab1 -->
				</div>
                

</div>