<?php
	if(isset($_POST["submit"]))
	{
	
		extract($_POST);
		$status = isset($_POST['status']) ? $_POST['status'] : 0 ;
		$upd_array = array("sms_title"=>$sms_title,"sms_text"=>$sms_text,"status"=>$status);
		$upd = $db->updateCondition($upd_array,PREFIX."sms_settings","set_id=$set_id");
        $_SESSION["edit_message"] = "SMS settings have been updated successfully."; 
			 
		
		?>
		<script type="text/javascript">
            location.href = "index.php?action=manage_sms_setting";
        </script>	
                
		<?php 
		exit();	
}	
?>

<?php
	
	$set_id=$_GET['set_id'];
	 
	$all_rec=array("*");
	$res_sel 			= $db->selectSRow($all_rec,PREFIX."sms_settings","set_id=$set_id");
	$set_id 			= $res_sel['set_id'];
	$sms_title 			= $res_sel['sms_title'];
	$sms_text			= $res_sel['sms_text'];
	$status 			= $res_sel['status'];
?>
 
<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
$(document).ready(function(){
		// validate sms seting form on submit
		$("#sms_setting").validate({
			rules: {
				sms_title: "required",
				sms_text: "required",
			},
			messages: {
				sms_title: "Please enter sms title",
				sms_text: "Please enter sms text",
			}
		});

	});
</script>




<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------> 

<h2><img src="resources/images/icons/setting.png" alt="icon" /> Settings</h2>

<div class="content-box"><!-- Start Content Box -->

				<div class="content-box-header">
					<h3>Edit SMS Settings</h3>
				</div> <!-- End .content-box-header -->

				<div class="content-box-content">
                <div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form  enctype="multipart/form-data" method="post" action="" name="sms_setting" id="sms_setting">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
							
                            <p>
							    <label>SMS Title </label>
							    <input type="text"   name="sms_title" id="sms_title" class="text-input medium-input" value="<?php echo stripslashes($sms_title); ?>">
                                <span style="color:#FF0000">*</span> 
							  </p>	
							  <p>
								<label>SMS Text</label>
                                  
                                  <textarea name="sms_text" id="sms_text" rows="5" class="text-input medium-input" placeholder="Write down your sms text here"><?php echo stripslashes($sms_text); ?></textarea>
<span style="color:#FF0000">*</span> 
                            
								</p>
								<p>
							    <label>Active </label>
							    <input type="checkbox" value="1" id="status" name="status" <?php if($status == 1) echo "checked"; ?>>
							  </p>
							<p>
	                            <input type="hidden" value="<?php echo $set_id; ?>" name="set_id">
								<input type="submit" value="Submit" class="button" name="submit">
								</p>
                                								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div>
 				 
				</div>
                

</div>		 
 