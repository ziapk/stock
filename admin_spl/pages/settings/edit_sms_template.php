<?php 

if($_POST['sms_template_form'])
	{
		
	  	extract($_POST);
		
		 
		$status = isset($_POST['status']) ? $_POST['status'] : 0 ;
		$upd_array = array("selected_temp"=>$sms_title,"temp_format"=>$temp_format,"status"=>$status);
		//echo"<pre>"; print_r($upd_array); exit;
		$upd = $db->updateCondition($upd_array,PREFIX."sms_templates","temp_id=$temp_id");
        $_SESSION["edit_message"] = "SMS template have been updated successfully."; 
			 
		
		?>
		<script type="text/javascript">
            location.href = "index.php?action=manage_sms_template";
        </script>	
                
		<?php 
		exit();	
	}
	 
 
	$temp_id = $_GET['temp_id'];
	 
	$all_rec=array("*");
	$res_sel 			= $db->selectSRow($all_rec,PREFIX."sms_templates","temp_id=$temp_id");
	$temp_id 			= $res_sel['temp_id'];
	$selected_temp 		= $res_sel['selected_temp'];
	$temp_format		= $res_sel['temp_format'];
	$status 			= $res_sel['status'];
	
	//For SMS Titles
	$rs = $db->selectAll(PREFIX."sms_settings","");
	//echo "<pre>"; print_r($rs); exit;
	
?>


<!------------------------------------------------- Javascript Code ----------------------------------------------------------->
<script>
$(document).ready(function(){
		// validate sms seting form on submit
		$("#sms_template").validate({
			rules: {
				sms_title: "required",
				temp_format: "required",
			},
			messages: {
				sms_title: "Please select SMS template",
				temp_format: "Please enter SMS template format",
			}
		});

	});
</script>




<!-------------------------------------------------- End of Javascript Code ------------------------------------------------------>




<h2><img src="resources/images/icons/setting.png" alt="icon" /> Settings</h2>

<div class="content-box"><!-- Start Content Box -->

				<div class="content-box-header">
					<h3>Edit SMS Template</h3>
				</div> <!-- End .content-box-header -->

				<div class="content-box-content">
                <div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form  enctype="multipart/form-data" method="post" action="" name="sms_template" id="sms_template">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
							
                          <p>
    					<label>SMS Template</label>
							<select style="width:200px !important;"  class="text-input medium-input" id="sms_title" name="sms_title">
								<option value="">Choose SMS Type</option>
								<?php foreach($rs as $value) : ?>
								<option value="<?php echo $value->sms_title; ?>" <?php  if($value->sms_title == $selected_temp) echo"selected";  ?>><?php echo $value->sms_title; ?> </option>
									<?php endforeach ?>
							</select>
						</p>
							  <p>
								<label>SMS Format</label>
                                  
                                  <textarea name="temp_format" id="temp_format" rows="5" class="text-input medium-input" placeholder="Write down SMS template format here"><?php echo stripslashes($temp_format); ?></textarea>
<span style="color:#FF0000">*</span> 
                            
								</p>
								<p>
							    <label>Active </label>
							    <input type="checkbox"  value="1" id="status" name="status" <?php if($status == 1) echo "checked"; ?>>
							  </p>
							<p>
                             		
                                    <input type="hidden" value="<?php echo $temp_id; ?>" name="temp_id">
									<input type="submit" value="Submit" class="button" name="sms_template_form">
								</p>
                                								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div>
 				 
				 
                 
                      <!-- End #tab1 -->
				</div>
                

</div>