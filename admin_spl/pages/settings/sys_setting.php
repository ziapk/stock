<?php $id_ch = $_SESSION["admin_id"];
if($_POST['settings'])
	{
		extract($_POST);
		$turn_sys_off 	= isset($_POST['turn_sys_off']) ? $_POST['turn_sys_off'] : 1 ;
		$upd_array 		= array("est_date"=>$est_date,"est_time"=>$est_time,"turn_sys_off"=>$turn_sys_off);
		//echo"<pre>"; print_r($upd_array); exit;
		$upd = $db->update(PREFIX."sys_settings",$upd_array);
        $_SESSION["edit_message"] = "System settings have been updated successfully."; 
			 
		
		?>
		<script type="text/javascript">
            location.href = "index.php?action=sys_setting";
        </script>	
                
		<?php 
		exit();	
		
	}
	
	$all_rec = array("*");
	$rs = $db->selectSRow($all_rec,PREFIX."sys_settings","turn_sys_off='0'");
	$est_date		=	$rs["est_date"];
	$est_time		=	$rs["est_time"];
	$turn_sys_off	=	$rs["turn_sys_off"];
?>


<!--////////////////////////////////////////validation///////////////////////////////////-->
<style>
.error{
	background-color:inherit;
}
</style>
<script type="text/javascript">
/*$(function() {
 $('#est_date').datepicker('yy-mm-dd');
 });*/
	
  	 $(document).ready(function(){
		// validate signup form on keyup and submit
		$("#settings").validate({
			rules: {
				est_date: "required",
				est_time: "required",
			},
			messages: {
				est_date: "Please enter estimated date",
				est_time: "Please enter estimated time"
			}
		});

	});
</script>
 


<h2><img src="resources/images/icons/setting.png" alt="icon" /> Settings</h2>
<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>System Settings</h3>
				
					
					
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                <?php if(isset($_SESSION["edit_message"])) { ?>
                                         <div class="notification success png_bg">
                                             <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                             <div>
                                             <?php echo $_SESSION["edit_message"];?>                    
                    						</div>
										</div>
<?php unset($_SESSION["edit_message"]); } ?> 
 <!-- End #tab1 -->
											<div id="tab1" class="tab-content default-tab" style="display: block;">
					
						<form action="" method="post" enctype="multipart/form-data"  id="settings">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p>
									<label>Turn System Off</label>
								  <input type="checkbox" value="0" id="turn_sys_off" name="turn_sys_off" <?php  if($turn_sys_off == '0') echo "checked"; ?>>
								</p>
								<p>
							    <label>Estimate Date</label>
									<input type="text" value="<?php  echo $est_date; ?>"  id="est_date"  name="est_date" class="text-input medium-input">
                                    <span style="color:#FF0000">*</span> 
                            
                            
								</p>
    <div id="est_time" class="ui-widget-content" style="padding: .5em;">

								<p>
								  <label>Estimate Time</label>
								  <input type="text" value="<?php echo $est_time; ?>"  id="est_time" name="est_time"  class="text-input medium-input">
                                    <span style="color:#FF0000">*</span> 
                                     
								</p></div>
								
							
								
							
								
							
								
							
								<p>
                               	
									<input type="submit" value="Update" class="button" name="settings">
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div> <!-- End #tab2 -->    
					
					
</div>
</div>
