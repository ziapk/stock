<?php
	require_once("../common/config.php");
	if(!isset($_SESSION["admin_login"])) {
		header('Location: index.php');
	}    
	$all_rec = array("*");
	$ad_id=$_SESSION["admin_id"] ;
	$res_name = $db->selectSRow($all_rec,PREFIX."admin","admin_id='$ad_id'");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Admin Area</title>
		<link rel="stylesheet" href="resources/css/reset.css" />
		<link rel="stylesheet" href="resources/css/invalid.css" />	
		<link rel="stylesheet" href="resources/css/style.css" />
		<link rel="stylesheet" href="resources/css/colorbox.css" />
		<link rel="stylesheet" href="resources/css/zebra_pagination.css"  />
        <link rel="stylesheet" href="resources/css/accordian.css"  />
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/jquery-ui.min.css">
        <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.validate.min.js"></script>
        <script src="assets/js/additional-methods.min.js"></script>
        <script src="assets/js/jquery.dataTables.min.js"></script>
		<script src="resources/scripts/moment-with-locales.js"></script>
		<script src="resources/scripts/zebra_pagination.js"></script>
		
        
	<style>
	.page-header{
		margin: 0;
	}
	</style>
         
         
        <!-- AJAX function for updating pagination table, put here for accessing everywhere -->
        
          
        <script>
		function select_rows(rows){
             $.ajax({
                url: '../common/pagination_handler.php',
                data: {rows:rows},
                cache: false,
                error: function(e){
                    alert("Error occured");
                },
                success: function(response){
                   console.info(response);
                   location.reload(true);
                }
            });   
        }
	</script>
		<?php require_once("files/check_selected.php"); ?>
		<!-- Internet Explorer .png-fix -->
		<script src="resources/scripts/jquery.colorbox.js" type="text/javascript"></script>
        <style type="text/css" title="currentStyle">
			tr.selected {
				background-color: red !important;
			}
		</style>
	</head>
	<body>
	<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
		<div id="sidebar"><?php require_once("files/sidebar.php");?></div>
		<div class="header_top" style="">Inventory Management System </div>
		<div id="main-content"> <!-- Main Content Section with everything -->
			<div style="min-height:650px;">
			<noscript> <!-- Show a notification if the user has disabled javascript -->
				<div class="notification error png_bg">
					<div>
						Javascript is disabled or is not supported by your browser. Please <a href="../../../browsehappy.com/index.htm" title="Upgrade to a better browser">upgrade</a> your browser or <a href="../../../www.google.com/support/bin/answer_2EEFB1D5.py" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
					</div>
				</div>
			</noscript>
			<!-- Page Head -->
			<div class="welcome">
				<h4>Welcome  <?php echo $res_name["user_name"]; ?></h4></div>	
				<?php require_once("files/handler.php"); ?>
			</div>
			<?php  require_once("files/footer.php"); ?>
		</div> 
	</div>
</body>
</html>